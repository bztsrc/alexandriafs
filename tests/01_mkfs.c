/*
 * tests/01_mkfs.c
 * https://gitlab.com/bztsrc/alexandriafs
 *
 * Copyright (C) 2023 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Make file system test boundle
 */

#define NOMOUNT
#include  "common.h"

/* test definitions */
tests_t tests[] = {
    { test01, "bs 512, is 128" },
    { test02, "bs 512, is 256" },
    { test03, "bs 512, is 512" },
    { test04, "bs 4096, is 256" },
    { test05, "bs 4096, is 512" },
    { test06, "bs 4096, is 4096" },
    { test07, "encrypted bs 512, is 256" },
    { test08, "encrypted bs 512, is 512" },
    { test09, "encrypted bs 4096, is 512" },
    { 0 }
};

int test01(void)
{
    /* better to be safe than sorry */
    if(axfs_crc(NULL, 0) != 0 || axfs_crc32(0, NULL, 0) != 0 || axfs_crc((uint8_t*)"a", 1) != axfs_crc32(0, (uint8_t*)"a", 1) ||
      axfs_crc((uint8_t*)"123456789", 9) != axfs_crc32(0, (uint8_t*)"123456789", 9))
        return report("axfs_crc and axfs_crc32 mismatch?");

    if(axfs_mkfs(NULL, 0, 2, DISK_SIZE/512, 0, uuid, JBLKS, WRPROT, 0, NULL, NULL))
        return report("axfs_mkfs returned not 0?");
    return 1;
}

int test02(void)
{
    axfs_inode_t *ino = (axfs_inode_t*)(disk + 256);
    axfs_extent_t ext = { 0 };
    int nblk = 2;

    if(axfs_mkfs(NULL, 0, 1, DISK_SIZE/512, 0, uuid, JBLKS, WRPROT, 0, NULL, NULL) != nblk)
        return report("axfs_mkfs returned not %u?", nblk);
    if(memcmp(disk + 256, AXFS_IN_MAGIC, 4)) return report("no inode after superblock");
    if(memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) || ino->i_type != AXFS_FT_INT_VOL)
        return report("missing volume inode");
    if(*((uint8_t*)ino + 64)) return report("should have no partial inode blocks");
    ino = (axfs_inode_t*)(disk + 768);
    if(memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) || ino->i_type != AXFS_FT_INT_FREE)
        return report("missing free blocks inode");
    axfs_unpack((uint8_t*)ino + 32, &ext);
    if(ext.e_blk != (uint64_t)nblk) return report("first free block isn't %u?", nblk);
    return 1;
}

int test03(void)
{
    axfs_inode_t *ino = (axfs_inode_t*)(disk + 512);
    axfs_extent_t ext = { 0 };
    int nblk = 4;

    if(axfs_mkfs(NULL, 0, 0, DISK_SIZE/512, 0, uuid, JBLKS, WRPROT, 0, NULL, NULL) != nblk)
        return report("axfs_mkfs returned not %u?", nblk);
    if(!memcmp(disk + 256, AXFS_IN_MAGIC, 4)) return report("should be no inode after superblock");
    if(memcmp(disk + 512, AXFS_IN_MAGIC, 4)) return report("no inode on first block");
    if(memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) || ino->i_type != AXFS_FT_INT_VOL)
        return report("missing volume inode");
    if(*((uint8_t*)ino + 64)) return report("should have no partial inode blocks");
    ino = (axfs_inode_t*)(disk + 1536);
    if(memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) || ino->i_type != AXFS_FT_INT_FREE)
        return report("missing free blocks inode");
    axfs_unpack((uint8_t*)ino + 32, &ext);
    if(ext.e_blk != (uint64_t)nblk) return report("first free block isn't %u?", nblk);
    return 1;
}

int test04(void)
{
    axfs_inode_t *ino = (axfs_inode_t*)(disk + 256);
    axfs_extent_t ext = { 0 };
    int nblk = 1;

    if(axfs_mkfs(NULL, 3, 4, DISK_SIZE/4096, 0, uuid, JBLKS, WRPROT, 0, NULL, NULL) != nblk)
        return report("axfs_mkfs returned not %u?", nblk);
    if(memcmp(disk + 256, AXFS_IN_MAGIC, 4)) return report("no inode after superblock");
    if(memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) || ino->i_type != AXFS_FT_INT_VOL)
        return report("missing volume inode");
    if(!*((uint8_t*)ino + 64)) return report("should have partial inode blocks");
    ino = (axfs_inode_t*)(disk + 768);
    if(memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) || ino->i_type != AXFS_FT_INT_FREE)
        return report("missing free blocks inode");
    axfs_unpack((uint8_t*)ino + 32, &ext);
    if(ext.e_blk != (uint64_t)nblk) return report("first free block isn't %u?", nblk);
    return 1;
}

int test05(void)
{
    axfs_inode_t *ino = (axfs_inode_t*)(disk + 512);
    axfs_extent_t ext = { 0 };
    int nblk = 1;

    if(axfs_mkfs(NULL, 3, 3, DISK_SIZE/4096, 0, uuid, JBLKS, WRPROT, 0, NULL, NULL) != nblk)
        return report("axfs_mkfs returned not %u?", nblk);
    if(!memcmp(disk + 256, AXFS_IN_MAGIC, 4)) return report("should be no inode after superblock");
    if(memcmp(disk + 512, AXFS_IN_MAGIC, 4)) return report("no inode on first block");
    if(memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) || ino->i_type != AXFS_FT_INT_VOL)
        return report("missing volume inode");
    if(!*((uint8_t*)ino + 64)) return report("should have partial inode blocks");
    ino = (axfs_inode_t*)(disk + 1536);
    if(memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) || ino->i_type != AXFS_FT_INT_FREE)
        return report("missing free blocks inode");
    axfs_unpack((uint8_t*)ino + 32, &ext);
    if(ext.e_blk != (uint64_t)nblk) return report("first free block isn't %u?", nblk);
    return 1;
}

int test06(void)
{
    axfs_inode_t *ino = (axfs_inode_t*)(disk + 4096);
    axfs_extent_t ext = { 0 };
    int nblk = 4;

    if(axfs_mkfs(NULL, 3, 0, DISK_SIZE/4096, 0, uuid, JBLKS, WRPROT, 0, NULL, NULL) != nblk)
        return report("axfs_mkfs returned not %u?", nblk);
    if(!memcmp(disk + 256, AXFS_IN_MAGIC, 4)) return report("should be no inode after superblock");
    if(!memcmp(disk + 512, AXFS_IN_MAGIC, 4)) return report("should be no inode on first sector");
    if(memcmp(disk + 4096, AXFS_IN_MAGIC, 4)) return report("no inode on first block");
    if(memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) || ino->i_type != AXFS_FT_INT_VOL)
        return report("missing volume inode");
    if(*((uint8_t*)ino + 64)) return report("should have no partial inode blocks");
    ino = (axfs_inode_t*)(disk + 12288);
    if(memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) || ino->i_type != AXFS_FT_INT_FREE)
        return report("missing free blocks inode");
    axfs_unpack((uint8_t*)ino + 32, &ext);
    if(ext.e_blk != (uint64_t)nblk) return report("first free block isn't %u?", nblk);
    return 1;
}

int test07(void)
{
    if(axfs_mkfs(NULL, 0, 1, DISK_SIZE/512, 0, uuid, JBLKS, WRPROT, 0, NULL, "canismerga") != 2)
        return report("axfs_mkfs returned not 2?");
    if(*((uint32_t*)(disk + 256)) != 0x3fa6d1c8) return report("no encrypted inode after superblock");
    return 1;
}

int test08(void)
{
    if(axfs_mkfs(NULL, 0, 0, DISK_SIZE/512, 0, uuid, JBLKS, WRPROT, 0, NULL, "canismerga") != 4)
        return report("axfs_mkfs returned not 4?");
    if(*((uint32_t*)(disk + 256))) return report("rest of the superblock should be left unencrypted");
    if(*((uint32_t*)(disk + 512)) != 0x6cbaa51e) return report("no encrypted inode on first block");
    return 1;
}

int test09(void)
{
    if(axfs_mkfs(NULL, 3, 3, DISK_SIZE/4096, 0, uuid, JBLKS, WRPROT, 0, NULL, "canismerga") != 1)
        return report("axfs_mkfs returned not 1?");
    if(!*((uint32_t*)(disk + 256))) return report("rest of the superblock should be encrypted");
    if(*((uint32_t*)(disk + 512)) != 0x37a369ed) return report("no encrypted inode on first block");
    return 1;
}
