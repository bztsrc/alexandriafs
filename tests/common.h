/*
 * tests/common.h
 * https://gitlab.com/bztsrc/alexandriafs
 *
 * Copyright (C) 2023 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Common test handling functions
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <sys/stat.h>
#define AXFS_IMPLEMENTATION
#include <libaxfs.h>

#ifndef DISK_SIZE
#define DISK_SIZE 1024*1024
#endif

#ifndef JBLKS
#define JBLKS 0
#endif

#ifndef WRPROT
#define WRPROT 0
#endif

/* list of tests in this boundle */
typedef struct { int (*test)(void); char *name; } tests_t;
extern tests_t tests[];
extern int test01(void); extern int test02(void); extern int test03(void); extern int test04(void); extern int test05(void);
extern int test06(void); extern int test07(void); extern int test08(void); extern int test09(void); extern int test10(void);
extern int test11(void); extern int test12(void); extern int test13(void); extern int test14(void); extern int test15(void);
extern int test16(void); extern int test17(void); extern int test18(void); extern int test19(void); extern int test20(void);
extern int test21(void); extern int test22(void); extern int test23(void); extern int test24(void); extern int test25(void);
extern int test26(void); extern int test27(void); extern int test28(void); extern int test29(void); extern int test30(void);
char err[128];
int report(char *fmt, ...) { __builtin_va_list args; __builtin_va_start(args, fmt); vsprintf(err, fmt, args); __builtin_va_end(args); return 0; }

/* interface for a minimalist ramdisk */
uint64_t dev_now_msec = 1;
uint8_t *disk, uuid[16] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
axfs_ctx_t ctx;
int dev_read(void *dev, uint64_t off, void *buf, uint32_t size)
{ (void)dev; if(off + size > DISK_SIZE || off & (ctx.blksiz - 1)) return 0; memcpy(buf, disk + off, size); return 1; }
int dev_write(void *dev, uint64_t off, void *buf, uint32_t size)
{ (void)dev; if(off + size > DISK_SIZE || off & (ctx.blksiz - 1)) return 0; memcpy(disk + off, buf, size); return 1; }
void hexdump(void *buf, uint32_t n)
{
    uint8_t *ptr = (uint8_t*)buf;
    uint64_t offs = (uintptr_t)ptr - (uintptr_t)disk;
    uint32_t i, j, k;
    if(offs >= DISK_SIZE) { printf("\nout of range\n"); }
    else if(offs + n > DISK_SIZE) n = DISK_SIZE - offs;
    for(j = 0; j < n; j += 16, ptr += 16) {
        printf("\r%08lx: ", offs + j);
        k = j + 16 < n ? 16 : n - j;
        for(i = 0; i < 16; i++) if(i < k) printf("%02x ", ptr[i]); else printf("   ");
        printf(" ");
        for(i = 0; i < k && i < 16; i++) printf("%c", ptr[i] >= 32 && ptr[i] < 127 ? ptr[i] : '.');
        printf("\n");
    }
}

/**
 * The common test boundle runner
 */
int main(int argc, char **argv)
{
    char *boundle = strrchr(argv[0], '/') + 1;
    int i, n, ret = 0;

    (void)argc; (void)argv;
    for(n = 0; tests[n].test; n++);

    disk = malloc(DISK_SIZE);
    if(!disk) { fprintf(stderr, "unable to allocate memory\n"); return 1; }
    memset(&ctx, 0, sizeof(ctx));
#ifndef NOMOUNT
    memset(disk, 0, DISK_SIZE);
    if(!axfs_mkfs(NULL, 3, 3, DISK_SIZE/512, 0, uuid, JBLKS, WRPROT, 0, NULL, NULL) || !axfs_mount(&ctx, NULL, NULL))
        { free(disk); fprintf(stderr, "unable to mount? should never happen\n"); return 1; }
#endif

    for(i = 0; i < n; i++) {
#ifdef NOMOUNT
        memset(disk, 0, DISK_SIZE);
#endif
        printf("\r%2d/%2d %s\x1b[K", i + 1, n, tests[i].name); fflush(stdout);
        if(!(*tests[i].test)()) {
            printf("\r---------------- %s/test%02d() failed! %s ----------------\x1b[K\n", boundle, i + 1, err);
            ret++;
#ifndef NOMOUNT
            break;
#endif
        }
    }

#ifndef NOMOUNT
    if(!axfs_umount(&ctx)) { free(disk); fprintf(stderr, "\nunable to umount? should never happen\n"); return 1; }
#endif
    if(!ret) printf("\r%-32sok\x1b[K\n", boundle);
    else printf("\r%-32sfailed %d, skipped %d tests\x1b[K\n", boundle, ret, i < n ? n - i - 1 : 0);
    free(disk);
    return ret;
}
