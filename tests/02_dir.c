/*
 * tests/02_dir.c
 * https://gitlab.com/bztsrc/alexandriafs
 *
 * Copyright (C) 2023 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Directory test boundle
 */

#include  "common.h"

/* test definitions */
tests_t tests[] = {
    { test01, "empty root" },
    { test02, "mkdir" },
    { test03, "mkdir again" },
    { test04, "mkdir with existing filename" },
    { 0 }
};

int test01(void)
{
    axfs_super_t *sb = (axfs_super_t*)disk;
    axfs_inode_t *ino = (axfs_inode_t*)(disk + 1024);
    axfs_dirhdr_t *hdr = (axfs_dirhdr_t*)ino->i_d;

    if(sb->s_root_fid != 2 || memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) || ino->i_type != AXFS_FT_DIR ||
      ino->i_mime != AXFS_MT_ROOT || ino->i_flags != 1 || ino->i_btime != dev_now_msec ||
      ino->i_ctime || ino->i_mtime) return report("missing or bad root dir inode");
    if(ino->i_size != hdr->d_size + 1 || hdr->d_size != sizeof(axfs_dirhdr_t) || memcmp(hdr->d_magic, AXFS_DR_MAGIC, 4) ||
      hdr->d_self_fid != sb->s_root_fid) return report("bad root dir header");
    return 1;
}

int test02(void)
{
    axfs_inode_t *ino = (axfs_inode_t*)(disk + 1024);
    axfs_dirhdr_t *hdr = (axfs_dirhdr_t*)ino->i_d;
    axfs_extent_t ext = { 0 };

    if(!axfs_mkdir(&ctx, "/bin")) return report("mkdir failed");
    if(memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) || ino->i_type != AXFS_FT_DIR ||
      ino->i_mime != AXFS_MT_ROOT || ino->i_flags != 1 || ino->i_ctime != dev_now_msec || ino->i_mtime != dev_now_msec ||
      ino->i_size != 42) return report("missing or bad root dir inode");
    if(memcmp(hdr->d_magic, AXFS_DR_MAGIC, 4) || hdr->d_size != 38 || hdr->d_nument != 1) return report("bad directory header");
    axfs_unpack((uint8_t*)hdr + sizeof(axfs_dirhdr_t), &ext);
    if(ext.e_blk != 4) return report("first entry's d_fid isn't 4");
    return 1;
}

int test03(void)
{
    axfs_inode_t *ino = (axfs_inode_t*)(disk + 1024);
    axfs_dirhdr_t *hdr = (axfs_dirhdr_t*)ino->i_d;
    axfs_extent_t ext = { 0 };

    if(!axfs_mkdir(&ctx, "/etc")) return report("mkdir failed");
    if(ino->i_size != 52 || ino->i_flags != 1) return report("bad root dir inode");
    if(memcmp(hdr->d_magic, AXFS_DR_MAGIC, 4) || hdr->d_size != 44 || hdr->d_nument != 2) return report("bad directory header");
    axfs_unpack((uint8_t*)hdr + 38, &ext);
    if(ext.e_blk != 5) return report("second entry's d_fid isn't 5");
    return 1;
}

int test04(void)
{
    axfs_inode_t *ino = (axfs_inode_t*)(disk + 1024);
    axfs_dirhdr_t *hdr = (axfs_dirhdr_t*)ino->i_d;

    if(axfs_mkdir(&ctx, "/etc")) return report("mkdir did not fail");
    if(ino->i_size != 52 || ino->i_flags != 1) return report("bad root dir inode");
    if(memcmp(hdr->d_magic, AXFS_DR_MAGIC, 4) || hdr->d_size != 44 || hdr->d_nument != 2) return report("bad directory header");
    return 1;
}
