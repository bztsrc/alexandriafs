The AleXandria File System
==========================

The [AleXandriaFS](https://gitlab.com/bztsrc/alexandriafs) is designed for modern storage devices without any moving parts (SSDs,
SD cards, USB sticks, etc.), provides all the features you'd expect from an enterprise-grade file system (write cycle protection,
access control lists, extended attributes, journaling, encryption etc.) and it is very efficient, flexible, and very performant.
Furthermore **AleXandriaFS** also made it its goal to use as small portion of the storage as possible for its meta data.

The low-level on-disk format can be found in the [AleXandriaFS Specification](docs/README.md).

The [linux](linux) directory contains a Linux kernel module providing `mount -t axfs`. More information can be found
[here](linux/Documentation/filesystems/axfs.rst) on how to use it. (WIP, read-only for now)

The [src](src) directory on the other hand contains dependency-free and stand alone user space tools like *mkfs.axfs*, *fsck.axfs*
etc., these are implemented both for the POSIX and for the WIN32 API. These work on device and image files as well. The *tool.axfs*
is a no root privilege required swiss army knife tool (much like mtools), but you can also use the *fuse.axfs* FUSE driver.

The entire file system is implemented in two ANSI C header files: [axfs.h](axfs.h) contains all the on-disk structures, and
[libaxfs.h](libaxfs.h) is an stb-style single header library with all the algorithms and a [high level user API](docs/API.md).

Limitations
-----------

Not really a good phrase, because using this file system feels like if it were truly unlimited.

| Property                      | Biggest allowed                                                                                |
|-------------------------------|------------------------------------------------------------------------------------------------|
| logical block size            | 2^264 (but that's not practical. The default is 4k, 64k being a more likely limit)             |
| one volume size               | 2^64 blocks (with 4k blocks it's 2^76 bytes, 64 Zetta, or ten thousands of million Terrabytes) |
| one file                      | 2^80 bytes (also depends on block size and the actual volume capacity)                         |
| longest file name             | 246 bytes (less in characters, because some UTF-8 characters are multi-byte)                   |
| O(log2) access directories    | 2^32 bytes (ca. 16 million entries, if all file names are 246 bytes, more if they are shorter) |
| entries in a directory        | 2^32 (less than 4 billion, because unique names force file names to grow)                      |
| blocks in one extent          | 2^64 - 1                                                                                       |
| number of attributes          | 2^32 in total, one compressed block (with 4k blocks about 500 attributes) per inode            |

Most important aspects are all handled in a flexible way, for example there's absolutely no limit on number of inodes, and you
can resize the volume or just the journal buffer on-the-fly. The biggest advantage of AXFS is scalability, that the size of the
required meta data does not grow as the volume size increases like with most other file systems.

License
-------

**AleXandriaFS** as well as its supporting library is Free and Open Source Software, licensed under the terms of GPL version 3 or
(at your opinion) any later version. See the [LICENSE](LICENSE) file for details.
