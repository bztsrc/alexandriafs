/*
 * axfs.h - The AleXandria File System
 * https://gitlab.com/bztsrc/alexandriafs
 *
 * Copyright (C) 2023 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief On-disk structures of the AleXandria File System
 */

#ifndef AXFS_H
#define AXFS_H

#ifndef _STDINT_H
#include <stdint.h>
#endif

/*
 * Usual layout:
 *   LBN 0: superblock (at byte offset 0x000 - 0x0FF)
 *   LBN 1: first inode block (with superblock, root dir, and free blocks)
 *   LBN 2: root directory
 *   ...data blocks...
 */

#define AXFS_FILENAME_MAX 246           /* longest file name allowed */

/******** super block ********/

#define AXFS_SB_MAGIC    "AXFS"
#define AXFS_SB_MAJOR    1
#define AXFS_SB_MINOR    0

#define AXFS_SB_F_NOCRC  (1<<0)         /* feature flag, file system meta data has no or invalid CRC checksums */
#define AXFS_SB_E_SHA    0              /* encryption method SHA-CBC */
#define AXFS_SB_E_AES    1              /* encryption method AES-CBC */

typedef struct {
    uint8_t         s_loader[4];        /* 0x000 reserved for boot loader */
    uint8_t         s_magic[4];         /* 0x004 magic 'A','X','F','S' */
    uint8_t         s_major;            /* 0x008 1 */
    uint8_t         s_minor;            /* 0x009 0 */
    uint8_t         s_blksize;          /* 0x00A block size (in power of two minus 9) */
    uint8_t         s_inoshift;         /* 0x00B inode shift bits (between 0 and 12) */
    uint16_t        s_flags;            /* 0x00C feature flags */
    uint8_t         s_nresblks;         /* 0x00E number of reserved blocks for boot loader */
    uint8_t         s_enctype;          /* 0x00F encryption method */
    uint8_t         s_encrypt[28];      /* 0x010 encryption mask */
    uint32_t        s_enchash;          /* 0x02C encryption password CRC */
    uint8_t         s_uuid[16];         /* 0x030 file system UUID (must match GPT.PartitionUniqueUUID) */
    uint64_t        s_nblks;            /* 0x040 total number of blocks on volume */
    uint64_t        s_volume_fid;       /* 0x048 volume's inode */
    uint64_t        s_root_fid;         /* 0x050 root directory's inode */
    uint64_t        s_freeblk_fid;      /* 0x058 free block's inode */
    uint64_t        s_journal_fid;      /* 0x060 journal's inode (0 if none) */
    uint64_t        s_attridx_fid;      /* 0x068 attribute index's inode (0 if none) */
    uint64_t        s_uquota_fid;       /* 0x070 user quota's inode (0 if none) */
    uint8_t         s_reserved[120];    /* 0x078 must be zero, reserved for future use */
    uint64_t        s_badblk_fid;       /* 0x0F0 bad block's inode (0 if none) */
    uint32_t        s_chksum;           /* 0x0F8 CRC32 0x004 - 0x0F8 */
    uint8_t         s_magic2[4];        /* 0x0FC magic 'A','X','F','S' */
} __attribute__((packed)) axfs_super_t;
/*
 * the super block is always at offset 0 and 256 bytes in size, no matter the block size.
 * block size is (1 << (s_blksize + 9)), from 512 bytes.
 * inode size is (block size >> s_inoshift), but at least 256 bytes.
 * if s_enchash is non-zero, then all blocks except the first one are encrpyted.
 * fid: file id, (fid >> s_inoshift) is an LBN pointing to a block with inodes, (fid & ((1 << s_inoshift)-1)) index into block.
 */

/******** access control entry ********/

#define AXFS_READ    (1<<0)
#define AXFS_WRITE   (1<<1)
#define AXFS_EXEC    (1<<2)
#define AXFS_APPEND  (1<<3)             /* no write, but able to append */
#define AXFS_DELETE  (1<<4)             /* allow deletion of file without write permission on the parent directory */
#define AXFS_NOT     (1<<5)             /* negates the access right, for example no read, no write etc. */
#define AXFS_SUID    (1<<6)             /* inherit owner */
#define AXFS_SGID    (1<<7)             /* inherit ACL */

typedef struct {                        /* an UUID, with access bits in the most significant byte */
    uint32_t        Data1;
    uint16_t        Data2;
    uint16_t        Data3;
    uint8_t         Data4[7];
    uint8_t         access;
} __attribute__((packed)) axfs_access_t;

/******** extent ********/

typedef struct {
    uint64_t        e_blk;              /* LBN block address pointing to data blocks or further axfs_extent_t records */
    uint64_t        e_num;              /* total number of blocks coverred by the pointed extents list */
    uint32_t        e_len;              /* only for indirect extents, number of blocks allocated for next level */
} __attribute__((packed)) axfs_extent_t;
/* extents are stored on disk as compressed block runs. Use axfs_pack() and axfs_unpack(), see below. */

/******** i-node ********/

#define AXFS_IN_MAGIC    "AXIN"
/* special file types */
#define AXFS_FT_DIR      0              /* directory, normal file just with structured datastream */
#define AXFS_FT_SDR      1              /* live directory, search results */
#define AXFS_FT_UNI      2              /* directory union, target in inode */
#define AXFS_FT_LNK      3              /* symbolic link, target in inode */
#define AXFS_FT_DEV      4              /* device file, inlined datastream contains OS-specific details */
#define AXFS_FT_PIPE     5              /* named pipe, fifo, inlined datastream contains OS-specific details */
#define AXFS_FT_SOCK     6              /* network socket, inlined datastream contains OS-specific details */
#define AXFS_FT_BOOT     7              /* same as AXFS_FT_APPL, except must be defragmented and cannot be relocated */
/* regular file types (main media or mime type) */
#define AXFS_FT_APPL     8              /* application */
#define AXFS_FT_AUDIO    9
#define AXFS_FT_FONT    10
#define AXFS_FT_IMAGE   11              /* 2D picture */
#define AXFS_FT_MESSAGE 12
#define AXFS_FT_MODEL   13              /* 3D model */
#define AXFS_FT_MULTI   14              /* multipart, this is also used for all archive files instead of their IANA type */
#define AXFS_FT_TEXT    15
#define AXFS_FT_VIDEO   16
#define AXFS_FT_DBASE   17              /* database */
#define AXFS_FT_DOC     18              /* document, book */
/* media or mime sub-types for normal inodes (byte offset to s_attridx_fid's data stream) */
/* see the full list at https://www.iana.org/assignments/media-types/media-types.txt */
#define AXFS_MT_UNKN   0                /* used when s_attridx_fid is zero or mime type not known, application/octet-stream */
#define AXFS_MT_ROOT   0xffffffff       /* in conjunction with AXFS_FT_DIR this means root directory */

/* file system internal types */
#define AXFS_FT_INT_FREE  0xff
#define AXFS_FT_INT_BAD   0xfe
#define AXFS_FT_INT_VOL   0xfd
#define AXFS_FT_INT_JRNL  0xfc
#define AXFS_FT_INT_AIDX  0xfb
#define AXFS_FT_INT_UQTA  0xfa

#define AXFS_ISDIR(mode)  ((mode) <= AXFS_FT_UNI)
#define AXFS_ISLNK(mode)  ((mode) == AXFS_FT_LNK)
#define AXFS_ISDEV(mode)  ((mode) == AXFS_FT_DEV)
#define AXFS_ISFIFO(mode) ((mode) == AXFS_FT_PIPE)
#define AXFS_ISSOCK(mode) ((mode) == AXFS_FT_SOCK)
#define AXFS_ISREG(mode)  (!((mode) & 0x80) && (mode) >= AXFS_FT_BOOT)

/* having a character or block device file is OS specific, so needs the datastream to decide, file type just says device file */
#define AXFS_ISCHR(mode)  AXFS_ISDEV(mode)
#define AXFS_ISBLK(mode)  AXFS_ISDEV(mode)

/* I could have used a lot of union structs, but spelling out like this saves us a lot of typing later */

/* normal inode, not AXFS_FT_INT_x and inode size > 256 */
typedef struct {
    uint8_t         i_magic[4];         /* 0x000 magic 'A','X','I','N' */
    uint32_t        i_chksum;           /* 0x004 CRC32 for bytes 0x008 to inode size */
    uint64_t        i_nblks;            /* 0x008 number of allocated blocks for this inode */
    uint64_t        i_size;             /* 0x010 file size low bits */
    uint16_t        i_size_hi;          /* 0x018 file size high bits */
    uint8_t         i_flags;            /* 0x01A inode flags (inlined) */
    uint8_t         i_type;             /* 0x01B file type, AXFS_FT_x */
    uint32_t        i_mime;             /* 0x01C mime type (byte offset into s_attridx_fid's data stream) */
    /* i_type dependent part */
    uint64_t        i_nlinks;           /* 0x020 number of hard links to this inode */
    uint64_t        i_btime;            /* 0x028 inode birth (creation) time */
    uint64_t        i_ctime;            /* 0x030 inode change time */
    uint64_t        i_mtime;            /* 0x038 file data modification time */
    uint8_t         i_attr[16];         /* 0x040 extended attributes extent */
    axfs_access_t   i_owner;            /* 0x050 file owner and access (control ace) */
    axfs_access_t   i_acl[10];          /* 0x060 ACL */
    uint8_t         i_d[1];             /* 0x100 inlined data or packet-encoded axfs_extent_t[] */
} __attribute__((packed)) axfs_inode_t;
/* File Offset mappings
 * only when i_size <= sizeof(i_d) and i_flags = 1, data is stored in i_d inlined, otherwise i_d stores a block run list.
 * for extents with e_len = 0, e_blk pointing to data of e_num blocks
 * for indirect extents with e_len > 0, they're pointing to blocks with more extents
 */

/* small inode, not AXFS_FT_INT_x and inode size == 256 */
typedef struct {
    uint8_t         i_magic[4];         /* 0x000 magic 'A','X','I','N' */
    uint32_t        i_chksum;           /* 0x004 CRC32 for bytes 0x008 to inode size */
    uint64_t        i_nblks;            /* 0x008 number of allocated blocks for this inode */
    uint64_t        i_size;             /* 0x010 file size low bits */
    uint16_t        i_size_hi;          /* 0x018 file size high bits */
    uint8_t         i_flags;            /* 0x01A inode flags (inlined) */
    uint8_t         i_type;             /* 0x01B file type, AXFS_FT_x */
    uint32_t        i_mime;             /* 0x01C mime type (byte offset into s_attridx_fid's data stream) */
    /* i_type dependent part */
    uint64_t        i_nlinks;           /* 0x020 number of hard links to this inode */
    uint64_t        i_btime;            /* 0x028 inode birth (creation) time */
    uint64_t        i_ctime;            /* 0x030 inode change time */
    uint64_t        i_mtime;            /* 0x038 file data modification time */
    uint8_t         i_attr[16];         /* 0x040 extended attributes extent */
    axfs_access_t   i_owner;            /* 0x050 file owner and access (control ace) */
    axfs_access_t   i_acl[2];           /* 0x060 ACL */
    uint8_t         i_d[1];             /* 0x080 inlined data or packet-encoded axfs_extent_t[] */
} __attribute__((packed)) axfs_inode_s_t;
/* same as normal inode, but has smaller i_acl[], just enough for UNIX permission simulation:
 *   i_owner: owner rwx, Data1 = uid, Data2, Data3, Data4 = 0
 *   i_acl[0]: group rwx, Data1 = gid, Data2, Data3, Data4 = 0
 *   i_acl[1]: other rwx, Data1, Data2, Data3, Data4 = 0xff
 */

/* descriptor only inode, AXFS_FT_INT_FREE, AXFS_FT_INT_BAD, AXFS_FT_INT_AIDX, AXFS_FT_INT_UQTA */
typedef struct {
    uint8_t         i_magic[4];         /* 0x000 magic 'A','X','I','N' */
    uint32_t        i_chksum;           /* 0x004 CRC32 for bytes 0x008 to inode size */
    uint64_t        i_nblks;            /* 0x008 number of allocated blocks for this inode */
    uint64_t        i_size;             /* 0x010 file size low bits */
    uint16_t        i_size_hi;          /* 0x018 file size high bits */
    uint8_t         i_flags;            /* 0x01A inode flags */
    uint8_t         i_type;             /* 0x01B file type */
    uint32_t        i_mime;             /* 0x01C mime type */
    /* i_type dependent part */
    uint8_t         i_d[1];             /* 0x020 packet-encoded axfs_extent_t[] */
} __attribute__((packed)) axfs_inode_d_t;

/* volume information, AXFS_FT_INT_VOL */
typedef struct {
    uint8_t         i_magic[4];         /* 0x000 magic 'A','X','I','N' */
    uint32_t        i_chksum;           /* 0x004 CRC32 for bytes 0x008 to inode size */
    uint64_t        i_nblks;            /* 0x008 number of allocated blocks for this inode */
    uint64_t        i_size;             /* 0x010 file size low bits */
    uint16_t        i_size_hi;          /* 0x018 file size high bits */
    uint8_t         i_flags;            /* 0x01A inode flags */
    uint8_t         i_type;             /* 0x01B file type, AXFS_FT_INT_VOL */
    uint32_t        i_state;            /* 0x01C file system state flags (like locked etc. 0 = everything is ok, 1 = needs fsck) */
    /* i_type dependent part */
    uint64_t        i_ninode;           /* 0x020 number of used inodes */
    uint64_t        i_lmtime;           /* 0x028 last mount time */
    uint64_t        i_lutime;           /* 0x030 last umount time */
    uint32_t        i_nwrite;           /* 0x038 number of maximum writes before inode relocation (0 if feature not used) */
    uint32_t        i_cwrite;           /* 0x03C number of remaining writes (counts down) */
    uint8_t         i_d[1];             /* 0x040 packet-encoded axfs_extent_t[] */
    /* the data stream maps the partially used and partially free inode blocks */
} __attribute__((packed)) axfs_inode_v_t;

/* journal inode, AXFS_FT_INT_JRNL */
typedef struct {
    uint8_t         i_magic[4];         /* 0x000 magic 'A','X','I','N' */
    uint32_t        i_chksum;           /* 0x004 CRC32 for bytes 0x008 to inode size */
    uint64_t        i_nblks;            /* 0x008 number of allocated blocks for this inode */
    uint64_t        i_size;             /* 0x010 file size low bits */
    uint16_t        i_size_hi;          /* 0x018 file size high bits */
    uint8_t         i_flags;            /* 0x01A inode flags */
    uint8_t         i_type;             /* 0x01B file type, AXFS_FT_INT_JRNL */
    uint32_t        i_mime;             /* 0x01C mime type */
    /* i_type dependent part */
    uint64_t        i_blk;              /* 0x020 journal buffer start LBN */
    uint64_t        i_len;              /* 0x028 journal buffer length in blocks */
    uint64_t        i_head;             /* 0x030 journal head block */
    uint64_t        i_tail;             /* 0x038 journal tail block */
    uint8_t         i_d[1];             /* 0x040 packet-encoded axfs_extent_t[] */
} __attribute__((packed)) axfs_inode_j_t;

/******** directory ********/

#define AXFS_DR_MAGIC  "AXDR"

typedef struct {
    uint8_t         d_magic[4];         /* 0x000 magic 'A','X','D','R' */
    uint32_t        d_chksum;           /* 0x004 indextable CRC32 (or 0 if directory isn't indexed) */
    uint32_t        d_size;             /* 0x008 size of the entries in bytes */
    uint32_t        d_nument;           /* 0x00C number of entries */
    uint64_t        d_self_fid;         /* 0x010 back reference to inode */
    uint8_t         d_gui_attr[8];      /* 0x018 extended attributes for GUI */
                                        /* 0x020 tightly packed directory entries */
} __attribute__((packed)) axfs_dirhdr_t;

typedef struct {
    uint64_t        d_fid;              /* 0x000 the pointed inode's fid */
    uint8_t         d_nlen;             /* 0x008 the length of d_name (not including terminator zero) */
    char            d_name[AXFS_FILENAME_MAX+1]; /* +1 because of terminating zero */
} __attribute__((packed)) axfs_dirent_t;
/* directory entries stored on disk packed: one entry consist of an axfs_extent_t, in which e_blk is d_fid, e_num and e_len has 0
 * length, followed by d_name, a zero terminated UTF-8 filename. Deleted entry is indicated by an axfs_entent_t in which e_blk has
 * zero length, and instead e_num contains the original fid. The list is terminated by a zero byte (block run list terminator),
 * which isn't included in d_size. If i_size >= d_size + 1, then an indextable follows d_nument times uint16_t or uint32_t. */

/******** extended attributes ********/

/* pointed by i_attr extent in inodes */
#define AXFS_EA_MAGIC  "AXEA"
/* pointed by s_attridx_fid's first data stream, index keys */
#define AXFS_IK_MAGIC  "AXIK"
/* pointed by s_attridx_fid's other data streams, index values */
#define AXFS_IV_MAGIC  "AXIV"

typedef struct {
    uint8_t         a_magic[4];         /* 0x000 magic 'A','X','E','A' / 'A','X','I','K' / 'A','X','I','V'*/
    uint32_t        a_chksum;           /* 0x004 CRC32 (AXEA: from 0x008 to a_size, AXIK, AXIV: indextable's) */
    uint32_t        a_size;             /* 0x008 size of entries in bytes */
    uint32_t        a_nument;           /* 0x00C number of entries */
    uint64_t        a_self_fid;         /* 0x010 back reference to inode (AXEA), s_attridx_fid (AXIK), attribute key offset (AXIV) */
                                        /* 0x018 list of packed axfs_entext_t[] (AXEA) or directory entries (AXIK, AXIV) */
} __attribute__((packed)) axfs_eahdr_t;
/* attribute key value pairs are stored in axfs_extent_t format, e_blk is the attribute's key (offset into s_attridx_fid's
 * data stream), and e_num is the attribute's value (also a byte offset into s_attridx_fid's data stream). For AXIK and AXIV
 * the values are packed directory entries, but AXIV stores an AXIK byte offset in e_num instead of a d_name field. */

/******** user quotas ********/

typedef struct {
    axfs_access_t   q_uuid;             /* 0x000 file owner (i_owner in inodes), access byte must be zero */
    uint64_t        q_nblks;            /* 0x010 total number of blocks currently used by this user */
    uint64_t        q_limit;            /* 0x018 total number of blocks this user is allowed to use */
} __attribute__((packed)) axfs_quotaent_t;

/******** Low-level data stream functions ********/

uint8_t *axfs_ds(axfs_inode_t *ino, uint32_t inosiz);
uint8_t *axfs_pack(axfs_extent_t *ext, uint8_t *ptr, uint8_t *top);
uint8_t *axfs_unpack(uint8_t *ptr, axfs_extent_t *ext);
uint8_t *axfs_skip(uint8_t *ptr);
uint32_t axfs_crc(uint8_t *buf, uint64_t len);

#ifdef AXFS_IMPLEMENTATION

/**
 * Locate the data stream descriptor in the inode
 */
uint8_t *axfs_ds(axfs_inode_t *ino, uint32_t inosiz)
{
    return !ino ? NULL : (ino->i_type & 0x80 ?
        /* special inodes, either 32 or 64, depends on type */
        (ino->i_type == AXFS_FT_INT_VOL || ino->i_type == AXFS_FT_INT_JRNL ?
            ((axfs_inode_v_t*)ino)->i_d : ((axfs_inode_d_t*)ino)->i_d) :
        /* normal inode, either 128 or 256, depends on inode size */
        (inosiz == 256 ? ((axfs_inode_s_t*)ino)->i_d : ino->i_d)
    );
}

/**
 * Compress an extent into a packed block run
 */
uint8_t *axfs_pack(axfs_extent_t *ext, uint8_t *ptr, uint8_t *top)
{
    register uint8_t bl, nl, ll, pk;

    if(!ext->e_blk && ext->e_num == 1) { if(ptr + 2 > top) { return NULL; } *ptr++ = 0x08; *ptr++ = 0x01; return ptr; }
    bl = ext->e_blk >> 32 ? 4 : (ext->e_blk >> 16 ? 3 : (ext->e_blk >> 8 ? 2 : (ext->e_blk ? 1 : 0)));
    nl = ext->e_num >> 32 ? 4 : (ext->e_num >> 16 ? 3 : (ext->e_num >> 8 ? 2 : (ext->e_num > 1 ? 1 : 0)));
    ll = ext->e_len >> 16 ? 3 : (ext->e_len >> 8 ? 2 : (ext->e_len ? 1 : 0));
    pk = bl | (nl << 3) | (ll << 6);
    if(bl) bl = 1 << (bl - 1);
    if(nl) nl = 1 << (nl - 1);
    if(ll) ll = 1 << (ll - 1);
    if(ptr + 1 + bl + nl + ll > top) return NULL;
    *ptr++ = pk;
    if(bl) { memcpy(ptr, &ext->e_blk, bl); ptr += bl; }
    if(nl) { memcpy(ptr, &ext->e_num, nl); ptr += nl; }
    if(ll) { memcpy(ptr, &ext->e_len, ll); ptr += ll; }
    return ptr;
}

/**
 * Decompress a packed block run into an extent
 */
uint8_t *axfs_unpack(uint8_t *ptr, axfs_extent_t *ext)
{
    register uint8_t bl, nl, ll;

    memset(ext, 0, sizeof(axfs_extent_t));
    if(*ptr) {
        bl = (*ptr) & 7;
        nl = (*ptr >> 3) & 7;
        ll = (*ptr >> 6);
        ptr++;
        if(bl) { bl = 1 << (bl - 1); memcpy(&ext->e_blk, ptr, bl); ptr += bl; }
        if(nl) { nl = 1 << (nl - 1); memcpy(&ext->e_num, ptr, nl); ptr += nl; } else ext->e_num++;
        if(ll) { ll = 1 << (ll - 1); memcpy(&ext->e_len, ptr, ll); ptr += ll; }
    }
    return ptr;
}

/**
 * Skip a packed block run
 */
uint8_t *axfs_skip(uint8_t *ptr)
{
    register uint8_t bl, nl, ll;

    if(*ptr) {
        bl = (*ptr) & 7;
        nl = (*ptr >> 3) & 7;
        ll = (*ptr >> 6);
        ptr += 1 +
            (bl ? 1 << (bl - 1) : 0) +
            (nl ? 1 << (nl - 1) : 0) +
            (nl ? 1 << (ll - 1) : 0);
    }
    return ptr;
}

/**
 * Calculate the CRC value (correct and compact, but non-optimal implementation)
 */
uint32_t axfs_crc(uint8_t *buf, uint64_t len)
{
    uint32_t crc, c, i;
    for(crc = 0xffffffff; len; len--) {
        c = (crc & 0xff) ^ *buf++;
        for(i = 0; i < 8; i++)
            if(c & 1) c = 0xedb88320L ^ (c >> 1);
            else c = c >> 1;
        crc = (crc >> 8) ^ c;
    }
    return crc ^ 0xffffffff;
}

#endif /* AXFS_IMPLEMENTATION */

#endif /* AXFS_H */
