/*
 * libaxfs.h - The AleXandria File System supporting library
 * https://gitlab.com/bztsrc/alexandriafs
 *
 * Copyright (C) 2023 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Single header library for the AleXandria File System
 *
 * WARNING little-endian machines only
 */

#include <stdlib.h>
#include <string.h>
#include <axfs.h>

/**
 * ---------------- Must be provided by host starts ----------------
 * both off and size are in bytes, but they always going to be the multiple of block size; return 1 on success 0 or error
 * dev_now_msec is the current timestamp in microseconds, the caller is responsible to set it before each axfs function call
 */
int dev_read(void *dev, uint64_t off, void *buf, uint32_t size);
int dev_write(void *dev, uint64_t off, void *buf, uint32_t size);
extern uint64_t dev_now_msec;
/**---------------- Must be provided by host ends ----------------**/

/******** Public API for the AleXandria File System ********/

#ifndef AXFS_DIRSEP
#define AXFS_DIRSEP '/'
#endif

#ifndef SEEK_SET
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2
#endif

/**
 * Directory index context
 */
#define AXFS_IDXENT(x) ((x) < 65536 ? 2 : 4)
typedef struct {
    axfs_dirhdr_t hdr;
    uint64_t fid, lastfid, offs;
    uint32_t idxent, idxlen, lastlen, lastidx;
    uint8_t *data, lastsrch[AXFS_FILENAME_MAX+1];
    void *idx;
} axfs_diridx_t;

/**
 * Common context used by the routines (a very minimal block cache), one per volume
 */
#define axfs_ino_v ((axfs_inode_v_t*)ctx->ino[0])
#define axfs_ino_r ((axfs_inode_t*)ctx->ino[1])
#define axfs_ino_f ((axfs_inode_d_t*)ctx->ino[2])
#define axfs_ino_j ((axfs_inode_j_t*)ctx->ino[3])
#define axfs_ino_a ((axfs_inode_d_t*)ctx->ino[4])
#define axfs_ino_q ((axfs_inode_d_t*)ctx->ino[5])
#define axfs_ino   ((axfs_inode_t*)ctx->ino[6])
typedef struct {
    void *dev;
    axfs_super_t sb;
    uint32_t blksiz, inosiz, inomsk;
    uint8_t key[32];
    uint64_t lbn[4], fid[2];
#ifdef AXFS_NOMALLOC
    uint8_t blk[8][4096], ino[8][4096];
#else
    uint8_t *blk[8], *ino[8], *jbuf;
    uint32_t jlen;
#endif
    axfs_diridx_t dir, ea;
} axfs_ctx_t;

/**
 * Opened file context, as many as you want
 */
typedef struct {
    axfs_ctx_t *ctx;
    axfs_diridx_t dir;
    uint64_t fid;
    uint64_t offs;
#ifdef AXFS_NOMALLOC
    uint8_t ino[4096];
#else
    uint8_t *ino;
#endif
} axfs_file_t;

/**
 * Returned by axfs_getattr
 */
typedef struct {
    uint64_t        st_ino;     /* fid, inode number */
    uint64_t        st_size;    /* apparent file size on 80 bits */
    uint16_t        st_size_hi;
    uint16_t        st_type;    /* st_mode most significant bits, sort of AXFS_FT_x */
    uint32_t        st_blksize; /* block size in bytes */
    uint64_t        st_blocks;  /* allocated blocks for this file, in st_blksize */
    uint64_t        st_nlink;   /* number of hard links */
    uint64_t        st_btime;   /* birth (creation) time, microseconds since EPOCH */
    uint64_t        st_ctime;   /* change time */
    uint64_t        st_mtime;   /* modification time */
    uint8_t         st_gui[8];  /* directory GUI attributes */
    char            *st_mime;   /* points right after the struct, might be NULL */
    char            *st_icon;   /* points right after the struct, might be NULL */
    char            *st_path;   /* points right after the struct, might be NULL */
    axfs_access_t   st_owner;   /* st_mode + st_uid */
    axfs_access_t   st_acl[10]; /* st_mode + multiple st_gids */
} axfs_stat_t;

/**
 * Public API functions
 */
int axfs_mkfs(void *dev, uint32_t blksize, uint32_t inoshift, uint64_t numblks, uint32_t nresblk, uint8_t *uuid, uint32_t jblks,
    uint32_t wrprot, uint32_t nbad, uint64_t *bad, const char *passphrase);
int axfs_mount(axfs_ctx_t *ctx, void *dev, const char *passphrase);
int axfs_umount(axfs_ctx_t *ctx);
int axfs_rekey(axfs_ctx_t *ctx, const char *oldpass, const char *newpass);
uint64_t axfs_lookupat_fid(axfs_ctx_t *ctx, uint64_t at_fid, const char *d_name, uint32_t len);
uint64_t axfs_lookup_fid(axfs_ctx_t *ctx, const char *path);
int axfs_link(axfs_ctx_t *ctx, uint64_t at_fid, uint64_t d_fid, const char *d_name);
int axfs_unlink(axfs_ctx_t *ctx, uint64_t at_fid, const char *d_name);
int axfs_remove(axfs_ctx_t *ctx, const char *path);
int axfs_mknod(axfs_ctx_t *ctx, const char *path, uint64_t mode, uint64_t dev_major, uint64_t dev_minor);
uint64_t axfs_mkdirat(axfs_ctx_t *ctx, uint64_t at_fid, const char *d_name);
int axfs_mkdir(axfs_ctx_t *ctx, const char *path);
int axfs_mkuni(axfs_ctx_t *ctx, const char *path, const char **targets);
int axfs_symlink(axfs_ctx_t *ctx, const char *path, const char *target);
int axfs_readlink(axfs_ctx_t *ctx, const char *path, char *outbuf, uint32_t outlen);
uint64_t axfs_openat(axfs_file_t *file, axfs_ctx_t *ctx, uint64_t at_fid, const char *d_name, int create);
int axfs_open(axfs_file_t *file, axfs_ctx_t *ctx, const char *path, int create);
uint64_t axfs_seek(axfs_file_t *file, int64_t offset, int whence);
uint64_t axfs_read(axfs_file_t *file, void *buffer, uint64_t size);
uint64_t axfs_write(axfs_file_t *file, void *buffer, uint64_t size);
int axfs_close(axfs_file_t *file);
int axfs_readdir(axfs_file_t *file, axfs_dirent_t *ent);
int axfs_truncate(axfs_file_t *file, uint64_t offs);
int axfs_chowner(axfs_file_t *file, axfs_access_t *acl);
int axfs_chgroup(axfs_file_t *file, axfs_access_t *acl);
int axfs_chother(axfs_file_t *file, axfs_access_t *acl);
int axfs_setattr(axfs_file_t *file, uint8_t type, const char *mime, const char *icon);
int axfs_getattr(axfs_file_t *file, axfs_stat_t *st, uint32_t outlen);
int axfs_setxattr(axfs_file_t *file, const char *key, const char *value);
int axfs_getxattr(axfs_file_t *file, const char *key, char *outval, uint32_t outlen);
int axfs_listxattr(axfs_file_t *file, char *outval, uint32_t outlen);

#ifdef AXFS_IMPLEMENTATION

/******** Low-level algoritms to handle the AleXandria File System ********/

/* blocks */
int axfs_read_blk(axfs_ctx_t *ctx, uint64_t lbn, uint8_t *blk);
int axfs_write_blk(axfs_ctx_t *ctx, uint64_t lbn, uint8_t *blk);
int axfs_write_meta(axfs_ctx_t *ctx, uint64_t lbn, uint8_t *blk);
uint64_t axfs_alloc_blk(axfs_ctx_t *ctx, uint64_t num);
int axfs_free_blk(axfs_ctx_t *ctx, uint64_t lbn, uint64_t num);
/* inodes */
int axfs_ext_ds(axfs_ctx_t *ctx, axfs_inode_t *ino, axfs_extent_t *ext, uint8_t *ds, uint8_t *next, uint8_t *top, int ind);
int axfs_ind_ds(axfs_ctx_t *ctx, axfs_inode_t *ino, uint8_t *ds, uint8_t *top, uint32_t ind);
int axfs_add_ds(axfs_ctx_t *ctx, axfs_inode_t *ino, uint64_t lbn, uint64_t num);
int axfs_del_ds(axfs_ctx_t *ctx, axfs_inode_t *ino, uint64_t lbn, uint64_t num);
uint64_t axfs_read_inode(axfs_ctx_t *ctx, axfs_inode_t *ino, uint64_t offs, uint64_t size, void *buf);
uint64_t axfs_write_inode(axfs_ctx_t *ctx, axfs_inode_t *ino, uint64_t offs, uint64_t size, void *buf);
uint64_t axfs_trunc_inode(axfs_ctx_t *ctx, axfs_inode_t *ino, uint64_t offs);
uint64_t axfs_alloc_inode(axfs_ctx_t *ctx, uint8_t filetype, uint32_t mimetype, uint64_t parent);
int axfs_free_inode(axfs_ctx_t *ctx, uint64_t fid);
axfs_inode_t *axfs_get_inode(axfs_ctx_t *ctx, uint64_t fid, int slot);
int axfs_put_inode(axfs_ctx_t *ctx, axfs_inode_t *ino, uint64_t fid, int slot);
int axfs_relocate(axfs_ctx_t *ctx);

/**
 * Mime types of file extensions database
 */
typedef struct {
    char *extention;
    char *mimetype;
    uint8_t len;
    uint8_t filetype;
} axfs_mimetype_t;

const axfs_mimetype_t axfs_mimetypes[] = {
    { ".csv",   "text/csv",          4, AXFS_FT_DBASE },
    { ".sqlite","application/sqlite",7, AXFS_FT_DBASE },
    { ".pdf",   "application/pdf",   4, AXFS_FT_DOC },
    { ".doc",   "application/msword",4, AXFS_FT_DOC },
    { ".docx",  "application/vnd.openxmlformats-officedocument.wordprocessingml.document",5, AXFS_FT_DOC },
    { ".odt",   "application/vnd.oasis.opendocument.text",4, AXFS_FT_DOC },
    { ".cbz",   "application/cbz",   4, AXFS_FT_DOC },
    { ".epub",  "application/epub+zip",5, AXFS_FT_DOC },
    { ".htm",   "text/html",        4, AXFS_FT_DOC },
    { ".html",  "text/html",        5, AXFS_FT_DOC },
    { ".hlp",   "application/manual", 4, AXFS_FT_DOC },
    { ".aac",   "audio/aac",        4, AXFS_FT_AUDIO },
    { ".wav",   "audio/wave",       4, AXFS_FT_AUDIO },
    { ".ogg",   "audio/vorbis",     4, AXFS_FT_AUDIO },
    { ".mp3",   "audio/mp3",        4, AXFS_FT_AUDIO },
    { ".mid",   "audio/midi",       4, AXFS_FT_AUDIO },
    { ".midi",  "audio/midi",       5, AXFS_FT_AUDIO },
    { ".sfn",   "font/ssfont",      4, AXFS_FT_FONT },
    { ".ttf",   "font/ttf",         4, AXFS_FT_FONT },
    { ".otf",   "font/otf",         4, AXFS_FT_FONT },
    { ".avif",  "image/gif",        5, AXFS_FT_IMAGE },
    { ".bmp",   "image/bmp",        4, AXFS_FT_IMAGE },
    { ".gif",   "image/gif",        4, AXFS_FT_IMAGE },
    { ".png",   "image/png",        4, AXFS_FT_IMAGE },
    { ".tga",   "image/targa",      4, AXFS_FT_IMAGE },
    { ".jpg",   "image/jpeg",       4, AXFS_FT_IMAGE },
    { ".jpeg",  "image/jpeg",       5, AXFS_FT_IMAGE },
    { ".webp",  "image/webp",       5, AXFS_FT_IMAGE },
    { ".eml",   "message/email",    4, AXFS_FT_MESSAGE },
    { ".msg",   "message/email",    4, AXFS_FT_MESSAGE },
    { ".ical",  "text/icalendar",   5, AXFS_FT_MESSAGE },
    { ".m3d",   "model/3d-model",   4, AXFS_FT_MODEL },
    { ".zip",   "application/zip",  4, AXFS_FT_MULTI },
    { ".zzz",   "application/zzz",  4, AXFS_FT_MULTI },
    { ".tar",   "application/tar",  4, AXFS_FT_MULTI },
    { ".tgz",   "application/tar",  4, AXFS_FT_MULTI },
    { ".tar.gz","application/tar",  7, AXFS_FT_MULTI },
    { ".tz",    "application/tar",  3, AXFS_FT_MULTI },
    { ".tar.z", "application/tar",  6, AXFS_FT_MULTI },
    { ".rar",   "application/vnd.rar",4, AXFS_FT_MULTI },
    { ".txt",   "text/plain",       4, AXFS_FT_TEXT },
    { ".md",    "text/markdown",    4, AXFS_FT_TEXT },
    { ".css",   "text/stylesheet",  4, AXFS_FT_TEXT },
    { ".json",  "application/json", 5, AXFS_FT_TEXT },
    { ".xml",   "application/xml",  4, AXFS_FT_TEXT },
    { ".avi",   "video/avi",        4, AXFS_FT_VIDEO },
    { ".mpg",   "video/mpeg",       4, AXFS_FT_VIDEO },
    { ".mpeg",  "video/mpeg",       5, AXFS_FT_VIDEO },
    { ".mp4",   "video/mp4",        4, AXFS_FT_VIDEO },
    { ".mkv",   "video/matroska",   4, AXFS_FT_VIDEO },
    { ".webm",  "video/webm",       5, AXFS_FT_VIDEO },
    { NULL, NULL, 0, 0 }
};

/**
 * Convert UTF-8 to UNICODE
 */
uint32_t axfs_mbtowc(uint8_t **s)
{
    uint32_t c = **s;

    if((**s & 128) != 0) {
        if(!(**s & 32)) { c = ((**s & 0x1F)<<6)|(*(*s+1) & 0x3F); *s += 1; } else
        if(!(**s & 16)) { c = ((**s & 0xF)<<12)|((*(*s+1) & 0x3F)<<6)|(*(*s+2) & 0x3F); *s += 2; } else
        if(!(**s & 8)) { c = ((**s & 0x7)<<18)|((*(*s+1) & 0x3F)<<12)|((*(*s+2) & 0x3F)<<6)|(*(*s+3) & 0x3F); *s += 3; }
        else c = 0;
    }
    (*s)++;
    return c;
}

/**
 * CRC calculation with precalculated table
 */
uint32_t axfs_crc32_tbl[256]={
    0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f, 0xe963a535, 0x9e6495a3, 0x0edb8832,
    0x79dcb8a4, 0xe0d5e91e, 0x97d2d988, 0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2,
    0xf3b97148, 0x84be41de, 0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7, 0x136c9856, 0x646ba8c0, 0xfd62f97a,
    0x8a65c9ec, 0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
    0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b, 0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940, 0x32d86ce3,
    0x45df5c75, 0xdcd60dcf, 0xabd13d59, 0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423,
    0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924, 0x2f6f7c87, 0x58684c11, 0xc1611dab,
    0xb6662d3d, 0x76dc4190, 0x01db7106, 0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
    0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01, 0x6b6b51f4,
    0x1c6c6162, 0x856530d8, 0xf262004e, 0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
    0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65, 0x4db26158, 0x3ab551ce, 0xa3bc0074,
    0xd4bb30e2, 0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
    0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086, 0x5768b525,
    0x206f85b3, 0xb966d409, 0xce61e49f, 0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81,
    0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a, 0xead54739, 0x9dd277af, 0x04db2615,
    0x73dc1683, 0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
    0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7, 0xfed41b76,
    0x89d32be0, 0x10da7a5a, 0x67dd4acc, 0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e,
    0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b, 0xd80d2bda, 0xaf0a1b4c, 0x36034af6,
    0x41047a60, 0xdf60efc3, 0xa867df55, 0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
    0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7,
    0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d, 0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
    0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38, 0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7,
    0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242, 0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
    0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45, 0xa00ae278,
    0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc,
    0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9, 0xbdbdf21c, 0xcabac28a, 0x53b39330,
    0x24b4a3a6, 0xbad03605, 0xcdd70693, 0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
    0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d};
uint32_t axfs_crc32(uint32_t crc, uint8_t *buf, uint64_t len)
{
    for(crc^=0xffffffff; len; len--) crc=(crc>>8)^axfs_crc32_tbl[(crc&0xff)^*buf++];
    return crc^0xffffffff;
}

/**
 * SHA-256
 */
typedef struct {
   uint8_t d[64];
   uint32_t l, b[2], s[8];
} axfs_sha256_ctx_t;
#define SHA_ADD(a,b,c) if(a>0xffffffff-(c))b++;a+=c;
#define SHA_ROTL(a,b) (((a)<<(b))|((a)>>(32-(b))))
#define SHA_ROTR(a,b) (((a)>>(b))|((a)<<(32-(b))))
#define SHA_CH(x,y,z) (((x)&(y))^(~(x)&(z)))
#define SHA_MAJ(x,y,z) (((x)&(y))^((x)&(z))^((y)&(z)))
#define SHA_EP0(x) (SHA_ROTR(x,2)^SHA_ROTR(x,13)^SHA_ROTR(x,22))
#define SHA_EP1(x) (SHA_ROTR(x,6)^SHA_ROTR(x,11)^SHA_ROTR(x,25))
#define SHA_SIG0(x) (SHA_ROTR(x,7)^SHA_ROTR(x,18)^((x)>>3))
#define SHA_SIG1(x) (SHA_ROTR(x,17)^SHA_ROTR(x,19)^((x)>>10))
static uint32_t axfs_sha256_k[64]={
   0x428a2f98,0x71374491,0xb5c0fbcf,0xe9b5dba5,0x3956c25b,0x59f111f1,0x923f82a4,0xab1c5ed5,
   0xd807aa98,0x12835b01,0x243185be,0x550c7dc3,0x72be5d74,0x80deb1fe,0x9bdc06a7,0xc19bf174,
   0xe49b69c1,0xefbe4786,0x0fc19dc6,0x240ca1cc,0x2de92c6f,0x4a7484aa,0x5cb0a9dc,0x76f988da,
   0x983e5152,0xa831c66d,0xb00327c8,0xbf597fc7,0xc6e00bf3,0xd5a79147,0x06ca6351,0x14292967,
   0x27b70a85,0x2e1b2138,0x4d2c6dfc,0x53380d13,0x650a7354,0x766a0abb,0x81c2c92e,0x92722c85,
   0xa2bfe8a1,0xa81a664b,0xc24b8b70,0xc76c51a3,0xd192e819,0xd6990624,0xf40e3585,0x106aa070,
   0x19a4c116,0x1e376c08,0x2748774c,0x34b0bcb5,0x391c0cb3,0x4ed8aa4a,0x5b9cca4f,0x682e6ff3,
   0x748f82ee,0x78a5636f,0x84c87814,0x8cc70208,0x90befffa,0xa4506ceb,0xbef9a3f7,0xc67178f2
};
void axfs_sha256_t(axfs_sha256_ctx_t *ctx)
{
   uint32_t a,b,c,d,e,f,g,h,i,j,t1,t2,m[64];
   for(i=0,j=0;i<16;i++,j+=4) m[i]=(ctx->d[j]<<24)|(ctx->d[j+1]<<16)|(ctx->d[j+2]<<8)|(ctx->d[j+3]);
   for(;i<64;i++) m[i]=SHA_SIG1(m[i-2])+m[i-7]+SHA_SIG0(m[i-15])+m[i-16];
   a=ctx->s[0];b=ctx->s[1];c=ctx->s[2];d=ctx->s[3];
   e=ctx->s[4];f=ctx->s[5];g=ctx->s[6];h=ctx->s[7];
   for(i=0;i<64;i++) {
       t1=h+SHA_EP1(e)+SHA_CH(e,f,g)+axfs_sha256_k[i]+m[i];
       t2=SHA_EP0(a)+SHA_MAJ(a,b,c);h=g;g=f;f=e;e=d+t1;d=c;c=b;b=a;a=t1+t2;
    }
   ctx->s[0]+=a;ctx->s[1]+=b;ctx->s[2]+=c;ctx->s[3]+=d;
   ctx->s[4]+=e;ctx->s[5]+=f;ctx->s[6]+=g;ctx->s[7]+=h;
}
void axfs_sha256_i(axfs_sha256_ctx_t *ctx)
{
    ctx->l=0;ctx->b[0]=ctx->b[1]=0;
    ctx->s[0]=0x6a09e667;ctx->s[1]=0xbb67ae85;ctx->s[2]=0x3c6ef372;ctx->s[3]=0xa54ff53a;
    ctx->s[4]=0x510e527f;ctx->s[5]=0x9b05688c;ctx->s[6]=0x1f83d9ab;ctx->s[7]=0x5be0cd19;
}
void axfs_sha256_u(axfs_sha256_ctx_t *ctx, const void *data, int len)
{
    uint8_t *d=(uint8_t *)data;
    for(;len--;d++) {
        ctx->d[ctx->l++]=*d;
        if(ctx->l==64) {axfs_sha256_t(ctx);SHA_ADD(ctx->b[0],ctx->b[1],512);ctx->l=0;}
    }
}
void axfs_sha256_f(axfs_sha256_ctx_t *ctx, uint8_t *h)
{
    uint32_t i=ctx->l;
    ctx->d[i++]=0x80;
    if(ctx->l<56) {while(i<56) ctx->d[i++]=0x00;}
    else {while(i<64) ctx->d[i++]=0x00;axfs_sha256_t(ctx);memset(ctx->d,0,56);}
    SHA_ADD(ctx->b[0],ctx->b[1],ctx->l*8);
    ctx->d[63]=ctx->b[0];ctx->d[62]=ctx->b[0]>>8;ctx->d[61]=ctx->b[0]>>16;ctx->d[60]=ctx->b[0]>>24;
    ctx->d[59]=ctx->b[1];ctx->d[58]=ctx->b[1]>>8;ctx->d[57]=ctx->b[1]>>16;ctx->d[56]=ctx->b[1]>>24;
    axfs_sha256_t(ctx);
    for(i=0;i<4;i++) {
        h[i]   =(ctx->s[0]>>(24-i*8)); h[i+4] =(ctx->s[1]>>(24-i*8));
        h[i+8] =(ctx->s[2]>>(24-i*8)); h[i+12]=(ctx->s[3]>>(24-i*8));
        h[i+16]=(ctx->s[4]>>(24-i*8)); h[i+20]=(ctx->s[5]>>(24-i*8));
        h[i+24]=(ctx->s[6]>>(24-i*8)); h[i+28]=(ctx->s[7]>>(24-i*8));
    }
}

/**
 * Calculate the encryption key from a passphrase
 */
static void axfs_key(axfs_ctx_t *ctx, uint8_t *passphrase, uint32_t passlen)
{
    int i;
    axfs_sha256_ctx_t sha;
    uint8_t tmp[32];

    if(ctx) memset(ctx->key, 0, 32);
    if(!ctx || !ctx->sb.s_enchash || !passphrase || !*passphrase || !passlen) return;
    axfs_sha256_i(&sha);
    axfs_sha256_u(&sha, passphrase, passlen);
    axfs_sha256_f(&sha, tmp);
    axfs_sha256_i(&sha);
    axfs_sha256_u(&sha, tmp, 32);
    axfs_sha256_u(&sha, &ctx->sb.s_enchash, 4);
    axfs_sha256_f(&sha, tmp);
    for(i = 0; i < 28; i++) tmp[i] ^= ctx->sb.s_encrypt[i] ^ (uint8_t)(passphrase[0] + i);
    axfs_sha256_i(&sha);
    axfs_sha256_u(&sha, tmp, 28);
    axfs_sha256_f(&sha, ctx->key);
}

/**
 * Encrypt one block
 */
static void axfs_encrypt(axfs_ctx_t *ctx, uint64_t lbn, uint8_t *src, uint8_t *dst)
{
    uint32_t i, j;
    axfs_sha256_ctx_t sha;
    uint8_t tmp[32];

    if(!ctx || !ctx->sb.s_enchash || !src || !dst) return;
    switch(ctx->sb.s_enctype) {
        case AXFS_SB_E_SHA:
            axfs_sha256_i(&sha);
            axfs_sha256_u(&sha, ctx->key, 32);
            axfs_sha256_u(&sha, &lbn, 8);
            axfs_sha256_f(&sha, tmp);
            for(i = (lbn ? 0 : 256), src += i, dst += i; i < ctx->blksiz; i += 32) {
                axfs_sha256_i(&sha);
                axfs_sha256_u(&sha, tmp, 32);
                axfs_sha256_f(&sha, tmp);
                for(j = 0; j < 32; j++) *dst++ = *src++ ^ tmp[j];
            }
        break;
        case AXFS_SB_E_AES: break;
    }
}

/**
 * Decrypt one block
 */
static void axfs_decrypt(axfs_ctx_t *ctx, uint64_t lbn, uint8_t *src, uint8_t *dst)
{
    if(!ctx || !ctx->sb.s_enchash || !src || !dst) return;
    switch(ctx->sb.s_enctype) {
        case AXFS_SB_E_SHA: axfs_encrypt(ctx, lbn, src, dst); break;
        case AXFS_SB_E_AES: break;
    }
}

/**
 * Read in one block
 */
int axfs_read_blk(axfs_ctx_t *ctx, uint64_t lbn, uint8_t *blk)
{
    if(!ctx || !dev_read(ctx->dev, lbn << (ctx->sb.s_blksize + 9), blk, ctx->blksiz)) return 0;
    if(ctx->sb.s_enchash) axfs_decrypt(ctx, lbn, blk, blk);
    return 1;
}

/**
 * Load a block in one of the block cache slots
 */
int axfs_load_blk(axfs_ctx_t *ctx, uint64_t lbn, uint32_t slot)
{
    if(!ctx || slot >= sizeof(ctx->blk)/sizeof(ctx->blk[0])) return 0;
    if(ctx->lbn[slot] == lbn) return 1;
    if(!axfs_read_blk(ctx, lbn, ctx->blk[slot])) return 0;
    ctx->lbn[slot] = lbn;
    return 1;
}

/**
 * Write out one block
 */
int axfs_write_blk(axfs_ctx_t *ctx, uint64_t lbn, uint8_t *blk)
{
    int ret;
    if(!ctx) return 0;
    if(ctx->sb.s_enchash) {
        axfs_encrypt(ctx, lbn, blk, ctx->blk[7]);
        ret = dev_write(ctx->dev, lbn << (ctx->sb.s_blksize + 9), ctx->blk[7], ctx->blksiz);
    } else
        ret = dev_write(ctx->dev, lbn << (ctx->sb.s_blksize + 9), blk, ctx->blksiz);
    return ret;
}

/**
 * Write out one meta block (goes through journal if enabled)
 */
int axfs_write_meta(axfs_ctx_t *ctx, uint64_t lbn, uint8_t *blk)
{
    axfs_extent_t ext;
    uint64_t next;
    uint8_t *d, *l;

    if(!ctx) return 0;
    /* without journal, business as usual */
    if(!ctx->sb.s_journal_fid) return axfs_write_blk(ctx, lbn, blk);
    /* write the block to the journal buffer */
    next = (axfs_ino_j->i_tail + 1) % axfs_ino_j->i_len;
    if(next == axfs_ino_j->i_head || !axfs_write_blk(ctx, axfs_ino_j->i_blk + axfs_ino_j->i_tail, blk))
        return 0;
    axfs_ino_j->i_tail = next;
#ifndef AXFS_NOMALLOC
    /* keep a copy in memory for fast replay */
    ctx->jbuf = (uint8_t*)realloc(ctx->jbuf, ctx->jlen + ctx->blksiz);
    if(ctx->jbuf) { memcpy(ctx->jbuf + ctx->jlen, blk, ctx->blksiz); ctx->jlen += ctx->blksiz; }
    else ctx->jlen = 0;
#endif
    /* append the original LBN as an extent to the data stream descriptor */
    for(d = l = axfs_ino_j->i_d; *d; l = d, d = axfs_unpack(d, &ext));
    /* if it comes right after the last extent, then update that, otherwise add a new one */
    if(ext.e_num && ext.e_blk + ext.e_num == lbn) { ext.e_num++; d = l; }
    else { ext.e_blk = lbn; ext.e_num = 1; }
    /* we don't handle indirect here, maybe we should */
    axfs_pack(&ext, d, (uint8_t*)axfs_ino_j + ctx->inosiz);
    return 1;
}

/**
 * Replay journal
 */
int axfs_replay_journal(axfs_ctx_t *ctx)
{
    axfs_extent_t ext;
    uint8_t *ptr;
#ifndef AXFS_NOMALLOC
    uint8_t *blk;
#endif

    if(ctx && ctx->sb.s_journal_fid && axfs_ino_j->i_head != axfs_ino_j->i_tail) {
        /* go over blocks in buffer */
#ifndef AXFS_NOMALLOC
        blk = ctx->jbuf;
#endif
        for(ptr = axfs_ino_j->i_d; *ptr && axfs_ino_j->i_head != axfs_ino_j->i_tail; ) {
            /* get the next extent with the original LBN. We don't handle indirect here, maybe we should */
            ptr = axfs_unpack(ptr, &ext);
            if(ext.e_len) return 0;
            /* iterate on extent's length: read from journal buffer and write to original position */
            for( ; ext.e_num && axfs_ino_j->i_head != axfs_ino_j->i_tail; ext.e_num--, ext.e_blk++) {
#ifndef AXFS_NOMALLOC
                if(blk) {
                    /* we have the block in cache, no need to read it again */
                    if(!axfs_write_blk(ctx, ext.e_blk, blk)) return 0;
                    blk += ctx->blksiz;
                } else
#endif
                if(!axfs_read_blk(ctx, axfs_ino_j->i_blk + axfs_ino_j->i_head, ctx->blk[1]) ||
                   !axfs_write_blk(ctx, ext.e_blk, ctx->blk[1])) return 0;
                axfs_ino_j->i_head = (axfs_ino_j->i_head + 1) % axfs_ino_j->i_len;
            }
        }
        ctx->lbn[1] = -1UL;
        /* commit transaction, save back empty journal inode */
        memset(axfs_ino_j->i_d, 0, ctx->inosiz - sizeof(axfs_inode_j_t));
        axfs_ino_j->i_head = axfs_ino_j->i_tail;
        if(axfs_put_inode(ctx, (axfs_inode_t*)axfs_ino_j, ctx->sb.s_journal_fid, 0)) return 0;
    }
    return 1;
}

/**
 * Commit journal transaction
 */
int axfs_commit(axfs_ctx_t *ctx)
{
    int ret = 1;

    if(!ctx || !ctx->sb.s_journal_fid) return 1;
    if(axfs_ino_j->i_head != axfs_ino_j->i_tail) {
        /* record transaction */
        if(axfs_put_inode(ctx, (axfs_inode_t*)axfs_ino_j, ctx->sb.s_journal_fid, 0)) {
            /* commit transaction */
            ret = axfs_replay_journal(ctx);
        } else ret = 0;
        if(axfs_ino_v->i_nwrite) {
            if(axfs_ino_v->i_cwrite) axfs_ino_v->i_cwrite--;
            if(!axfs_ino_v->i_cwrite) axfs_relocate(ctx);
        }
    }
#ifndef AXFS_NOMALLOC
    if(ctx->jbuf) { free(ctx->jbuf); ctx->jbuf = NULL; ctx->jlen = 0; }
#endif
    return ret;
}

/**
 * Allocate one or more block(s)
 */
uint64_t axfs_alloc_blk(axfs_ctx_t *ctx, uint64_t num)
{
    axfs_extent_t ext, ext1;
    uint64_t lbn = 0;
    uint32_t i;
    uint8_t *o, *l, *d = axfs_ino_f->i_d, *t = axfs_ino_f->i_d, *top;

    if(!ctx || !num || !*axfs_ino_f->i_d || axfs_ino_f->i_size < num * ctx->blksiz) return 0;
    top = (uint8_t*)axfs_ino_f + ctx->inosiz;

    /* first, look for a direct extent in the inode */
    while(*d) {
        l = d; d = axfs_unpack(d, &ext);
        if(ext.e_num >= num && !ext.e_len) {
            lbn = ext.e_blk;
            ext.e_blk += num; ext.e_num -= num;
            axfs_ext_ds(ctx, (axfs_inode_t*)axfs_ino_f, &ext, l, d, top, 1);
            axfs_ino_f->i_size -= num * ctx->blksiz;
            return axfs_put_inode(ctx, (axfs_inode_t*)axfs_ino_f, ctx->sb.s_freeblk_fid, 0) ? lbn : 0;
        }
    }
    /* no luck, we must load indirect extents */
    while(*t) {
        o = t; t = axfs_unpack(t, &ext1);
        if(ext1.e_len) {
            for(i = 0; i < ext1.e_len; i++) {
                if(!axfs_load_blk(ctx, ext1.e_blk + i, 1)) break;
                for(d = ctx->blk[1]; *d;) {
                    l = d; d = axfs_unpack(d, &ext);
                    if(ext.e_num >= num) {
                        if(!ext.e_len) {
                            /* single indirect */
                            lbn = ext.e_blk;
                            ext.e_blk += num; ext.e_num -= num; ext1.e_num -= num;
                            axfs_ext_ds(ctx, (axfs_inode_t*)axfs_ino_f, &ext, l, d, ctx->blk[1] + ctx->blksiz, 1);
                            /* if this was the last extent, then we need to free the indirect block */
                            if(!ext1.e_num) {
                                ext1.e_num = ext1.e_len; ext1.e_len = 0;
                                axfs_ino_f->i_size += ext1.e_num * ctx->blksiz;
                                axfs_ino_f->i_nblks -= ext1.e_num;
                            } else
                                axfs_write_meta(ctx, ctx->lbn[1], ctx->blk[1]);
                            axfs_ext_ds(ctx, (axfs_inode_t*)axfs_ino_f, &ext1, o, t, top, 1);
                            axfs_ino_f->i_size -= num * ctx->blksiz;
                            return axfs_put_inode(ctx, (axfs_inode_t*)axfs_ino_f, ctx->sb.s_freeblk_fid, 0) ? lbn : 0;
                        } else {
                            /* double indirect */
                        }
                    }
                }
            }
        }
    }
    return 0;
}

/**
 * Free one or more block(s)
 */
int axfs_free_blk(axfs_ctx_t *ctx, uint64_t lbn, uint64_t num)
{
    if(!ctx || !lbn || !num) return 0;
    axfs_add_ds(ctx, (axfs_inode_t*)axfs_ino_f, lbn, num);
    return axfs_put_inode(ctx, (axfs_inode_t*)axfs_ino_f, ctx->sb.s_freeblk_fid, 0);
}

/**
 * Insert an extent into a data stream descriptor
 */
int axfs_ext_ds(axfs_ctx_t *ctx, axfs_inode_t *ino, axfs_extent_t *ext, uint8_t *ds, uint8_t *next, uint8_t *top, int ind)
{
    axfs_extent_t ext1, ext2;
    uint32_t siz, s1, s2;
    uint8_t *d, tmp[32];

    if(!ctx || !ino || !ext || !ds || !next || !top || ds > next || next > top) return 0;

    for(d = next; *d; d = axfs_skip(d));
    s1 = (uint32_t)((uintptr_t)d - (uintptr_t)next);
    if(ext->e_num) {
        d = axfs_pack(ext, tmp, tmp + sizeof(tmp));
        siz = (uint32_t)((uintptr_t)d - (uintptr_t)tmp);
        s2 = (uint32_t)((uintptr_t)next - (uintptr_t)ds);
        if(siz <= s2) {
            /* new run fits in the list */
            memcpy(ds, tmp, siz);
            if(siz < s2) {
                memcpy(ds + siz, next, s1);
                memset(ds + siz + s1, 0, top - ds - siz - s1);
            }
        } else {
            /* new run is bigger than the old one */
            s2 = siz - s2;
            if(ds + siz + s1 <= top) {
                if(next > ds) memmove(next + s2, next, s1);
                memcpy(ds, tmp, siz);
            } else {
                /* not enough space, we must make it indirect */
                if(!ind || !(ctx->lbn[2] = axfs_alloc_blk(ctx, 1))) return 0;
                memcpy(ctx->blk[2], tmp, siz);
                memcpy(ctx->blk[2] + siz, next, s1);
                memset(ctx->blk[2] + siz + s1, 0, ctx->blksiz - siz - s1);
                if(!axfs_write_meta(ctx, ctx->lbn[2], ctx->blk[2])) return 0;
                ino->i_nblks++;
                ext1.e_num = 0;
                for(d = ctx->blk[2]; d < ctx->blk[2] + ctx->blksiz && *d;) {
                    d = axfs_unpack(d, &ext2);
                    ext1.e_num += ext2.e_num;
                }
                ext1.e_blk = ctx->lbn[2]; ext1.e_len = 1;
                ds = axfs_pack(&ext1, ds, top);
                memset(ds, 0, top - ds);
            }
        }
    } else {
        /* new extent is empty, just exclude from ds */
        memcpy(ds, next, s1);
        memset(ds + s1, 0, top - ds - s1);
    }
    return 1;
}

/**
 * Make a data stream descriptor indirect
 */
int axfs_ind_ds(axfs_ctx_t *ctx, axfs_inode_t *ino, uint8_t *ds, uint8_t *top, uint32_t ind)
{
    axfs_extent_t ext;
    uint32_t siz;

    if(!ctx || !ino || !ds || !top || top < ds + 16 || !(ctx->lbn[0] = axfs_alloc_blk(ctx, 1))) return 0;
    siz = (uint32_t)((uintptr_t)top - (uintptr_t)ds);
    if(siz > ctx->blksiz) siz = ctx->blksiz;
    memcpy(ctx->blk[0], ds, siz);
    memset(ctx->blk[0] + siz, 0, ctx->blksiz - siz);
    if(ind) { if(!axfs_write_meta(ctx, ctx->lbn[0], ctx->blk[0])) return 0; }
    else {    if(!axfs_write_blk(ctx, ctx->lbn[0], ctx->blk[0])) return 0; }
    ext.e_blk = ctx->lbn[0]; ext.e_num = 1; ext.e_len = ind;
    ds = axfs_pack(&ext, ds, top);
    memset(ds, 0, top - ds);
    ino->i_nblks++;
    return 1;
}

/**
 * Add a block range to a data stream descriptor
 */
int axfs_add_ds(axfs_ctx_t *ctx, axfs_inode_t *ino, uint64_t lbn, uint64_t num)
{
    axfs_extent_t ext, ext1;
    uint8_t *ds, *o, *l, *d, *e, *t, *top;
    uint32_t i;

    if(!ctx || !ino || !lbn || !num || !(ds = axfs_ds(ino, ctx->inosiz))) return 0;
    top = (uint8_t*)ino + ctx->inosiz;

    /* check direct extents first for performance */
    for(e = ds; *e;) {
        l = e; e = axfs_unpack(e, &ext);
        if(!ext.e_len && (lbn + num == ext.e_blk || ext.e_blk + ext.e_num == lbn)) {
            if(lbn + num == ext.e_blk) ext.e_blk -= num;
            ext.e_num += num;
            return axfs_ext_ds(ctx, ino, &ext, l, e, top, 1);
        }
    }
    /* no luck, we must load indirect extents */
    for(t = ds; *t;) {
        o = t; t = axfs_unpack(t, &ext1);
        if(ext1.e_len) {
            for(i = 0; i < ext1.e_len; i++) {
                if(!axfs_load_blk(ctx, ext1.e_blk + i, 1)) break;
                for(d = ctx->blk[1]; *d;) {
                    l = d; d = axfs_unpack(d, &ext);
                    if(lbn + num == ext.e_blk || ext.e_blk + ext.e_num == lbn) {
                        if(!ext.e_len) {
                            /* single indirect */
                            if(lbn + num == ext.e_blk) ext.e_blk -= num;
                            ext.e_num += num; ext1.e_num += num;
                            axfs_ext_ds(ctx, ino, &ext, l, d, ctx->blk[1] + ctx->blksiz, 1);
                            axfs_write_meta(ctx, ctx->lbn[1], ctx->blk[1]);
                            return axfs_ext_ds(ctx, ino, &ext1, o, t, top, 1);
                        } else {
                            /* double indirect */
                        }
                    }
                }
            }
        }
    }
    /* append to the end of the list */
    ext.e_blk = lbn; ext.e_num = num; ext.e_len = 0;
    return axfs_ext_ds(ctx, ino, &ext, e, e, top, 1);
}

/**
 * Remove a block range from a data stream descriptor
 */
int axfs_del_ds(axfs_ctx_t *ctx, axfs_inode_t *ino, uint64_t lbn, uint64_t num)
{
    axfs_extent_t ext, ext1;
    uint8_t *ds, *o, *l, *d, *t, *top;
    uint32_t i;

    if(!ctx || !ino || !lbn || !num || !(ds = axfs_ds(ino, ctx->inosiz))) return 0;
    top = (uint8_t*)ino + ctx->inosiz;

/* FIXME: split extent ext.e_blk < lbn && ext.e_blk + ext.e_num > lbn + num */

    /* check direct extents first for performance */
    for(d = ds; *d;) {
        l = d; d = axfs_unpack(d, &ext);
        if(!ext.e_len && (lbn == ext.e_blk || ext.e_blk + ext.e_num == lbn + num)) {
            if(lbn == ext.e_blk) ext.e_blk += num;
            ext.e_num -= num;
            return axfs_ext_ds(ctx, ino, &ext, l, d, top, 1);
        }
    }
    /* no luck, we must load indirect extents */
    for(t = ds; *t;) {
        o = t; t = axfs_unpack(t, &ext1);
        if(ext1.e_len) {
            for(i = 0; i < ext1.e_len; i++) {
                if(!axfs_load_blk(ctx, ext1.e_blk + i, 1)) break;
                for(d = ctx->blk[1]; *d;) {
                    l = d; d = axfs_unpack(d, &ext);
                    if(lbn == ext.e_blk || ext.e_blk + ext.e_num == lbn + num) {
                        if(!ext.e_len) {
                            /* single indirect */
                            ext1.e_num -= num;
                            if(!ext1.e_num)
                                axfs_free_blk(ctx, ext1.e_blk, ext1.e_len);
                            else {
                                if(lbn == ext.e_blk) ext.e_blk += num;
                                ext.e_num -= num;
                                axfs_ext_ds(ctx, ino, &ext, l, d, ctx->blk[1] + ctx->blksiz, 1);
                                axfs_write_meta(ctx, ctx->lbn[1], ctx->blk[1]);
                            }
                            return axfs_ext_ds(ctx, ino, &ext1, o, t, top, 1);
                        } else {
                            /* double indirect */
                        }
                    }
                }
            }
        }
    }
    return 0;
}

/**
 * Read from an inode's data stream
 */
uint64_t axfs_read_inode(axfs_ctx_t *ctx, axfs_inode_t *ino, uint64_t offs, uint64_t size, void *buf)
{
    uint64_t rem, o = 0, e;
    uint32_t lvl, blk, os = 0, rs, bs;
    uint8_t *d, *i_d, *b = buf;
    axfs_extent_t ext;

    if(!ctx || !ino || !size || !buf || !ctx->blksiz || offs >= ino->i_size ||
      /* no read from free nor from bad blocks */
      (ino->i_type == AXFS_FT_INT_FREE || ino->i_type == AXFS_FT_INT_BAD) ||
      !(i_d = axfs_ds(ino, ctx->inosiz))) return 0;

    /* bound check and calculate frequently used values and starting offsets */
    if(offs + size > ino->i_size) size = ino->i_size - offs;
    bs = ctx->sb.s_blksize + 9; rem = size; os = offs & (ctx->blksiz - 1); offs >>= bs; rs = ctx->blksiz - os;

    /* while we have remaining bytes to read */
    while(rem) {
        /* convert file offset block number to file system block number */
        blk = lvl = 0; d = i_d;
        if(rs > rem) rs = rem;
        if(ino->i_flags & 1) {
            /* inlined data */
            blk = (uint8_t*)ino + ctx->inosiz - i_d;
            if(rs > blk) rs = blk;
            memcpy(b, d + os, rs);
            blk = 1;
        } else {
            while(*d) {
                d = axfs_unpack(d, &ext);
                e = o + (ext.e_num << bs);
                if(o == e) break;
                if(offs >= o && offs < e) {
                    if(ext.e_len) {
                        /* indirect */
                        if(!axfs_load_blk(ctx, ext.e_blk, lvl)) break;
                        d = ctx->blk[lvl];
                        if(lvl++ > 3) break;
                    } else {
                        /* direct */
                        if(ext.e_blk) {
                            if(!axfs_load_blk(ctx, ext.e_blk + o - offs, 0)) break;
                            memcpy(b, ctx->blk[0] + os, rs);
                        } else {
                            /* sparse file */
                            memset(b, 0, rs);
                        }
                        blk = 1;
                        break;
                    }
                } else o = e;
            }
        }
        if(!blk) break;
        os = 0; b += rs; rem -= rs; rs = ctx->blksiz;
        offs++;
    }
    return (size - rem);
}

/**
 * Write into an inode's data stream
 */
uint64_t axfs_write_inode(axfs_ctx_t *ctx, axfs_inode_t *ino, uint64_t offs, uint64_t size, void *buf)
{
    uint8_t *i_d;
    uint64_t inlsiz, oldsiz;

    if(!ctx || !ino || !size || !buf || !ctx->blksiz ||
      /* no writing to free nor to bad blocks */
      (ino->i_type == AXFS_FT_INT_FREE || ino->i_type == AXFS_FT_INT_BAD) ||
      !(i_d = axfs_ds(ino, ctx->inosiz))) return 0;
    ino->i_mtime = dev_now_msec;

    oldsiz = ino->i_size;
    inlsiz = (uint8_t*)ino + ctx->inosiz - i_d;
    if(ino->i_size < offs + size) ino->i_size = offs + size;

    /* inlined data? */
    if(ino->i_size < inlsiz) { memcpy(i_d + offs, buf, size); ino->i_flags |= 1; return size; }

    /* move to a direct block if it was inlined */
    if(ino->i_flags & 1) {
        if(!axfs_ind_ds(ctx, ino, i_d, i_d + inlsiz, 0)) { ino->i_size = oldsiz; return 0; }
        ino->i_flags &= ~1;
    }
    return 0;
}

/**
 * Truncate an inode's data stream
 */
uint64_t axfs_trunc_inode(axfs_ctx_t *ctx, axfs_inode_t *ino, uint64_t offs)
{
    if(!ctx || !ino) return 0;
    /* must not use blk[3] */
    ino->i_size = offs;
    return 0;
}

/**
 * Allocate an inode
 */
uint64_t axfs_alloc_inode(axfs_ctx_t *ctx, uint8_t filetype, uint32_t mimetype, uint64_t parent)
{
    axfs_extent_t ext;
    axfs_dirhdr_t *hdr;
    axfs_inode_t *ino = NULL;
    uint64_t fid = 0;
    uint32_t i = 0;
    uint8_t *d;

    if(!ctx) return 0;
    if(*axfs_ino_v->i_d) {
        /* we have partial inode blocks */
        d = axfs_unpack(axfs_ino_v->i_d, &ext);
        if(!axfs_load_blk(ctx, ext.e_blk, 3)) return 0;
        /* skip superblock on LBN 0 */
        if(!ext.e_blk) { fid++; i += ctx->inosiz; }
        /* find first free inode slot on block */
        for(; i < ctx->blksiz && ctx->blk[3][i]; fid++, i += ctx->inosiz);
        if(i < ctx->blksiz) {
            ino = (axfs_inode_t*)(ctx->blk[3] + i);
            fid |= (ext.e_blk << ctx->sb.s_inoshift);
        }
        /* are all the inode slots used now on this block? */
        for(i += ctx->inosiz; i < ctx->blksiz && ctx->blk[3][i]; i += ctx->inosiz);
        if(i == ctx->blksiz) {
            /* remove from partially used inode blocks list */
            ext.e_blk++; ext.e_num--;
            axfs_ext_ds(ctx, (axfs_inode_t*)axfs_ino_v, &ext, axfs_ino_v->i_d, d, (uint8_t*)axfs_ino_v->i_d + ctx->inosiz, 1);
        }
    }
    if(!ino) {
        /* no partial blocks, start a new inode block */
        ctx->lbn[3] = axfs_alloc_blk(ctx, 1);
        memset(ctx->blk[3], 0, ctx->blksiz);
        ino = (axfs_inode_t *)ctx->blk[3];
        fid = (ctx->lbn[3] << ctx->sb.s_inoshift);
        /* if multiple inodes fit on a block, create a partial inode block record too */
        if(ctx->sb.s_inoshift) {
            ext.e_blk = ctx->lbn[3]; ext.e_num = 1; ext.e_len = 0;
            axfs_pack(&ext, axfs_ino_v->i_d, axfs_ino_v->i_d + 32);
        }
    }
    memset(ino, 0, ctx->inosiz);
    memcpy(&ino->i_magic, AXFS_IN_MAGIC, 4);
    ino->i_type = filetype;
    ino->i_mime = mimetype;
    ino->i_btime = dev_now_msec;
    if(parent) {
        ext.e_blk = 0; ext.e_len = 0; ext.e_num = parent;
        axfs_pack(&ext, ino->i_attr, ino->i_attr + 16);
    }
    if(filetype == AXFS_FT_DIR || filetype == AXFS_FT_INT_AIDX) {
        hdr = (axfs_dirhdr_t*)((uint8_t*)ino + (ctx->inosiz == 256 ? 128 : 256));
        if(filetype == AXFS_FT_INT_AIDX) {
            memcpy(hdr->d_magic, AXFS_IK_MAGIC, 4);
            hdr->d_size = sizeof(axfs_eahdr_t);
        } else {
            memcpy(hdr->d_magic, AXFS_DR_MAGIC, 4);
            hdr->d_size = sizeof(axfs_dirhdr_t);
        }
        hdr->d_self_fid = fid;
        ino->i_size = hdr->d_size + 1;
        ino->i_flags = 1;
    }
    ctx->fid[0] = fid;
    memcpy(ctx->ino[6], ino, ctx->inosiz);
    axfs_ino_v->i_ninode++;
    axfs_put_inode(ctx, (axfs_inode_t*)axfs_ino_v, ctx->sb.s_volume_fid, 1);
    return fid;
}

/**
 * Allocate a special inode
 */
uint64_t axfs_alloc_spec(axfs_ctx_t *ctx, uint8_t filetype)
{
    uint64_t fid;

    if(!ctx || !(filetype & 0x80) || filetype > AXFS_FT_INT_AIDX) return 0;

    fid = axfs_alloc_inode(ctx, filetype, 0, 0);
    switch(filetype) {
        case AXFS_FT_INT_AIDX:
            ctx->sb.s_attridx_fid = fid;
            memcpy(axfs_ino_a, ctx->ino[6], ctx->inosiz);
        break;
        case AXFS_FT_INT_UQTA:
            ctx->sb.s_uquota_fid = fid;
            memcpy(axfs_ino_q, ctx->ino[6], ctx->inosiz);
        break;
        default: fid = 0; break;
    }
    if(fid) {
        /* update superblock, we must read / write multiple of block size */
        ctx->sb.s_chksum = axfs_crc32(0, ctx->sb.s_magic, 0xF4);
        ctx->lbn[1] = -1UL;
        if(!dev_read(ctx->dev, 0, &ctx->blk[1], ctx->blksiz)) return 0;
        memcpy(ctx->blk[1], &ctx->sb, sizeof(axfs_super_t));
        if(!dev_write(ctx->dev, 0, &ctx->blk[1], ctx->blksiz)) return 0;
    }
    return fid;
}

/**
 * Free an inode
 */
int axfs_free_inode(axfs_ctx_t *ctx, uint64_t fid)
{
    axfs_inode_t *ino;
    uint32_t i;

    /* take advantage of the fact that get inode loads to blk[3] */
    if(!ctx || !fid || !axfs_get_inode(ctx, fid, 0)) return 0;
    ino = (axfs_inode_t*)(ctx->blk[3] + (fid & ctx->inomsk) * ctx->inosiz);
    axfs_trunc_inode(ctx, ino, 0);
    memset(ino->i_magic, 0, 4);
    for(i = 0; i < ctx->blksiz && !ctx->blk[3][i]; i += ctx->inosiz);
    if(i < ctx->blksiz) {
        /* there are still used inodes on this block */
        axfs_write_meta(ctx, ctx->lbn[3], ctx->blk[3]);
    } else {
        /* free the block and remove it from partially used inode blocks list */
        axfs_free_blk(ctx, ctx->lbn[3], 1);
        axfs_del_ds(ctx, (axfs_inode_t*)axfs_ino_v, ctx->lbn[3], 1);
    }
    axfs_ino_v->i_ninode--;
    return axfs_put_inode(ctx, (axfs_inode_t*)axfs_ino_v, ctx->sb.s_volume_fid, 0);
}

/**
 * Get inode
 */
axfs_inode_t *axfs_get_inode(axfs_ctx_t *ctx, uint64_t fid, int slot)
{
    axfs_inode_t *ino;
    uint64_t lbn;

    if(!ctx || !fid || slot < 0 || slot > 1) return NULL;
    if(ctx->fid[slot] != fid) {
        ino = (axfs_inode_t*)(ctx->blk[3 - slot] + (fid & ctx->inomsk) * ctx->inosiz);
        lbn = fid >> ctx->sb.s_inoshift;
        if(!axfs_load_blk(ctx, lbn, 3 - slot) || memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) ||
          (!(ctx->sb.s_flags & AXFS_SB_F_NOCRC) && ino->i_chksum != axfs_crc32(0, (uint8_t*)ino + 8, ctx->inosiz - 8)))
            return NULL;
        memcpy(ctx->ino[6 + slot], ino, ctx->inosiz);
        ctx->fid[slot] = fid;
    }
    return (axfs_inode_t*)ctx->ino[6 + slot];
}

/**
 * Store inode to disk
 */
int axfs_put_inode(axfs_ctx_t *ctx, axfs_inode_t *ino, uint64_t fid, int slot)
{
    uint64_t lbn;
    uint8_t *ptr;

    if(!ctx || !ino || !fid || slot < 0 || slot > 1) return 0;
    if(ino->i_chksum == axfs_crc32(0, (uint8_t*)ino + 8, ctx->inosiz - 8)) return 1;
    ino->i_ctime = dev_now_msec;
    ino->i_chksum = axfs_crc32(0, (uint8_t*)ino + 8, ctx->inosiz - 8);
    lbn = fid >> ctx->sb.s_inoshift;
    if(ctx->inosiz < ctx->blksiz) {
        if(!axfs_load_blk(ctx, lbn, 3 - slot)) return 0;
        memcpy(ctx->blk[3 - slot] + (fid & ctx->inomsk) * ctx->inosiz, ino, ctx->inosiz);
        ptr = ctx->blk[3 - slot];
    } else
        ptr = (uint8_t*)ino;
    return axfs_write_meta(ctx, lbn, ptr);
}

/**
 * Relocate special inodes
 */
int axfs_relocate(axfs_ctx_t *ctx)
{
    uint64_t fid;
    if(ctx && axfs_ino_v->i_nwrite) {
        axfs_ino_v->i_cwrite = axfs_ino_v->i_nwrite;
        axfs_ino_v->i_chksum = axfs_ino_f->i_chksum = axfs_ino_j->i_chksum = axfs_ino_a->i_chksum = 0;
        if((fid = axfs_alloc_inode(ctx, AXFS_FT_INT_VOL, 0, 0)) &&
          axfs_put_inode(ctx, (axfs_inode_t*)axfs_ino_v, fid, 0)) {
            axfs_free_inode(ctx, ctx->sb.s_volume_fid);
            ctx->sb.s_volume_fid = fid;
        }
        if((fid = axfs_alloc_inode(ctx, AXFS_FT_INT_FREE, 0, 0)) &&
          axfs_put_inode(ctx, (axfs_inode_t*)axfs_ino_f, fid, 0)) {
            axfs_free_inode(ctx, ctx->sb.s_freeblk_fid);
            ctx->sb.s_freeblk_fid = fid;
        }
        if(ctx->sb.s_attridx_fid && (fid = axfs_alloc_inode(ctx, AXFS_FT_INT_AIDX, 0, 0)) &&
          axfs_put_inode(ctx, (axfs_inode_t*)axfs_ino_a, fid, 0)) {
            axfs_free_inode(ctx, ctx->sb.s_attridx_fid);
            ctx->sb.s_attridx_fid = fid;
        }
        if(ctx->sb.s_uquota_fid && (fid = axfs_alloc_inode(ctx, AXFS_FT_INT_UQTA, 0, 0)) &&
          axfs_put_inode(ctx, (axfs_inode_t*)axfs_ino_q, fid, 0)) {
            axfs_free_inode(ctx, ctx->sb.s_uquota_fid);
            ctx->sb.s_uquota_fid = fid;
        }
        /* journal must be the last */
        if(ctx->sb.s_journal_fid) {
            if((fid = axfs_alloc_inode(ctx, AXFS_FT_INT_JRNL, 0, 0)) &&
              axfs_put_inode(ctx, (axfs_inode_t*)axfs_ino_j, fid, 0)) {
                axfs_free_inode(ctx, ctx->sb.s_journal_fid);
                ctx->sb.s_journal_fid = fid;
            }
            axfs_replay_journal(ctx);
        }
        /* superblock */
        ctx->sb.s_chksum = axfs_crc32(0, ctx->sb.s_magic, 0xF4);
        /* we must read / write multiple of block size */
        ctx->lbn[1] = -1UL;
        if(!dev_read(ctx->dev, 0, &ctx->blk[1], ctx->blksiz)) return 0;
        memcpy(ctx->blk[1], &ctx->sb, sizeof(axfs_super_t));
        return dev_write(ctx->dev, 0, &ctx->blk[1], ctx->blksiz);
    }
    return 1;
}

/**
 * Free a directory indextable
 */
void axfs_free_index(axfs_diridx_t *dir)
{
    if(dir) {
#ifndef AXFS_NOMALLOC
        if(dir->data) { free(dir->data); dir->data = NULL; }
        if(dir->idx) { free(dir->idx); dir->idx = NULL; }
#endif
        memset(dir, 0, sizeof(axfs_diridx_t));
    }
}

/**
 * Read in a directory indextable
 */
int axfs_read_index(axfs_ctx_t *ctx, axfs_inode_t *ino, uint64_t fid, axfs_diridx_t *dir)
{
    if(!ctx || !ino || !fid || !dir || (ino->i_type != AXFS_FT_DIR && ino->i_type != AXFS_FT_INT_AIDX)) return 0;

    /* use cached result whenever possible */
    if(dir->fid == fid) return 1;

    /* we have to read it in from disk */
    axfs_free_index(dir);
    if(!axfs_read_inode(ctx, ino, 0, sizeof(axfs_dirhdr_t), &dir->hdr) || memcmp(dir->hdr.d_magic,
        ino->i_type == AXFS_FT_DIR ? AXFS_DR_MAGIC : AXFS_IK_MAGIC, 4)) return 0;
    dir->fid = fid;
    dir->idxent = AXFS_IDXENT(dir->hdr.d_size);
#ifndef AXFS_NOMALLOC
    dir->idxlen = dir->hdr.d_nument * dir->idxent;
    if((dir->idx = malloc(dir->idxlen))) {
        if(axfs_read_inode(ctx, ino, (dir->hdr.d_size + 4) & ~3, dir->idxlen, dir->idx) != dir->idxlen ||
          (!(ctx->sb.s_flags & AXFS_SB_F_NOCRC) && dir->hdr.d_chksum != axfs_crc32(0, (uint8_t*)dir->idx, dir->idxlen))) {
            free(ctx->dir.idx); ctx->dir.idx = NULL;
            dir->idxlen = 0;
        }
    } else dir->idxlen = 0;
#endif
    return 1;
}

/**
 * Do binary search in indextable, returns
 * -1: read error (IO failure or inode isn't a directory)
 *  0: position in idx, but no match
 *  1: position in idx, match, directory entry's fid also returned
 */
int axfs_bsearch_index(axfs_ctx_t *ctx, axfs_inode_t *ino, axfs_diridx_t *dir, const char *d_name, uint32_t *idx, uint64_t *fid)
{
    axfs_extent_t ext;
    uint8_t *d, *n, *t, *s1, *s2;
    uint32_t l, s, e, h = 0, a, b, o;
    int i = 0;

    if(!ctx || !ino || !dir || !d_name || !*d_name || !idx || !dir->idx || !dir->idxent) return -1;
    for(l = 0; d_name[l]; l++);
    if(fid && dir->lastlen == dir->idxlen && l < AXFS_FILENAME_MAX && !memcmp(d_name, dir->lastsrch, l + 1)) {
        *idx = dir->lastidx;
        *fid = dir->lastfid;
        return 1;
    }
    dir->lastlen = dir->idxlen;
    if(l < AXFS_FILENAME_MAX) { memcpy(dir->lastsrch, d_name, l); dir->lastsrch[l] = 0; } else dir->lastsrch[0] = 0;

    *idx = 0;
    if(!dir->idxlen) return 0;
    s = 0; e = dir->idxlen / dir->idxent - 1; t = ctx->blk[2];
    if(!dir->data) ctx->lbn[2] = -1U;
    while(s <= e) {
        h = (e + s) >> 1;
        o = dir->idxent == 2 ? ((uint16_t*)dir->idx)[h] : ((uint32_t*)dir->idx)[h];
        /* get entry. We either have a memory buffer, or we must read it from the given offset */
        if(dir->data) { t = dir->data + o; l = ctx->blksiz; if(o + l > dir->hdr.d_size) l = dir->hdr.d_size - o; }
        else if(!(l = axfs_read_inode(ctx, ino, o, ctx->blksiz, t))) return -1;
        /* decompress entry */
        d = n = *t ? axfs_unpack(t, &ext) : t + 1; for(; d < t + l && *d; d++);
        /* compare strings (technically strncmp(mbtowcs()), but we must not use libc) */
        for(s1 = n, s2 = (uint8_t*)d_name, i = *s1 - *s2; s1 < t + ctx->blksiz; ) {
            /* we must decode UTF-8, because it has multiple representations for the same UNICODE, see "overlong encodings" */
            a = axfs_mbtowc(&s1); b = axfs_mbtowc(&s2); i = a - b;
            if(!a || !b || i) break;
        }
        *idx = dir->lastidx = h; dir->lastfid = ext.e_blk;
        if(!i) { if(fid) { *fid = ext.e_blk; } dir->offs = o + (uintptr_t)n - (uintptr_t)t; return 1; } else
        if(i > 0) e = h - 1;
        else s = h + 1;
    }
    if(i < 0) *idx = dir->lastidx = h + 1;
    return 0;
}

/**
 * Insert val into indextable at idx
 */
int axfs_insert_index(axfs_diridx_t *dir, uint32_t idx, uint32_t val)
{
    uint16_t *i16;
    uint32_t i, n, *i32;

    if(!dir || !dir->idx || !dir->idxent) return 0;
    if(dir->idxent == 2) {
        i16 = (uint16_t*)dir->idx;
        n = dir->idxlen >> 1;
        if(n) for(i = n - 1; i > idx; i--) i16[i] = i16[i - 1];
        i16[idx] = val;
    } else {
        i32 = (uint32_t*)dir->idx;
        n = dir->idxlen >> 2;
        if(n) for(i = n - 1; i > idx; i--) i32[i] = i32[i - 1];
        i32[idx] = val;
    }
    dir->idxlen += dir->idxent;
    return 1;
}

/**
 * Get attribute key's or value's offset, add it if it doesn't exists yet
 */
uint32_t axfs_attr_offs(axfs_ctx_t *ctx, const char *key, int type)
{
    uint32_t offs;
    uint64_t fid = 0;

    if(!ctx || !ctx->sb.s_attridx_fid || !key || !*key ||
        !axfs_read_index(ctx, (axfs_inode_t*)axfs_ino_a, ctx->sb.s_attridx_fid, &ctx->ea) || !ctx->ea.idx) return AXFS_MT_UNKN;
    if(axfs_bsearch_index(ctx, (axfs_inode_t*)axfs_ino_a, &ctx->ea, key, &offs, &fid) == 1) return ctx->ea.offs;
    if(type == -1) return AXFS_MT_UNKN;
    return 0;
}

/******** high level API for the AleXandria File System ********/

/**
 * Create a file system (quick format)
 */
int axfs_mkfs(void *dev, uint32_t blksize, uint32_t inoshift, uint64_t numblks, uint32_t nresblk, uint8_t *uuid, uint32_t jblks,
    uint32_t wrprot, uint32_t nbad, uint64_t *bad, const char *passphrase)
{
    int ret = 0;
    uint32_t i, j, k;
    uint32_t blksiz = 1 << (blksize + 9);
    uint32_t inosiz = blksiz >> inoshift, numino = 3 + !!jblks + (nbad && bad);
    uint32_t hdrsiz;
    uint64_t inofid;
    uint8_t *hdr, *inodes, *ptr, *ptr2 = NULL, *top, *fd = NULL, *fd2 = NULL;
#ifdef AXFS_NOMALLOC
    uint8_t tmp[65536];
#endif
    axfs_ctx_t ctx;
    axfs_super_t *sb;
    axfs_inode_t *ino_r;
    axfs_inode_v_t *ino_v;
    axfs_inode_j_t *ino_j;
    axfs_inode_d_t *ino_d;
    axfs_dirhdr_t *dir;
    axfs_extent_t ext, fre;

    /* check arguments */
    if(jblks == -1U) {
        jblks = numblks / 100;
        if(jblks < 64) jblks = 64;
        if(jblks > 256) jblks = 256;
    }
    i = nresblk + !!wrprot + !inoshift;
    if(blksize > 11 || inoshift > 12 || inosiz < 256 || inosiz > blksiz || numblks < 8 + i + jblks || (nbad && !bad))
        return 0;
    hdrsiz = i * blksiz + ((inosiz * (numino + !i) + blksiz - 1) & ~(blksiz - 1));
    /* free and bad lists might expand header by one-one block if their data stream descriptor don't fit into the inode */
    j = hdrsiz + 2 * blksiz;
    k = i * blksiz + (!i ? inosiz : 0);
#ifdef AXFS_NOMALLOC
    if(j > sizeof(tmp)) return 0;
    hdr = tmp;
#else
    if(!(hdr = (uint8_t*)malloc(j))) return 0;
#endif
    memset(hdr, 0, j);
    sb = (axfs_super_t*)hdr;
    sb->s_nresblks = i;
    inodes = hdr + k;
    inofid = (i << inoshift) + !i;
    k += numino * inosiz;

    /* failsafe, look for bad blocks in header and journal
     * technically only the superblock must be solid, we could relocate all the rest, but we're lazy
     * open an issue if you really need to create a file system on a faulty disk and I'll rewrite this properly */
    if(nbad) {
        j = hdrsiz / blksiz + jblks;
        for(i = 0; i < nbad && bad[i] < j; i++);
        if(i < nbad && bad[i] < j) goto err;
    }

    /* get free blocks area (might be altered by journal buffer) */
    fre.e_blk = hdrsiz / blksiz; fre.e_num = numblks - fre.e_blk; fre.e_len = 0;

    /* volume inode */
    sb->s_volume_fid = inofid++;
    ino_v = (axfs_inode_v_t*)inodes;
    memcpy(&ino_v->i_magic, AXFS_IN_MAGIC, 4);
    ino_v->i_type = AXFS_FT_INT_VOL;
    ino_v->i_nwrite = wrprot;
    ino_v->i_ninode = numino;
    if(k % blksiz) {
        /* add partially used inode block to data stream descriptor */
        ext.e_blk = sb->s_nresblks + inosiz * (numino + !sb->s_nresblks) / blksiz; ext.e_num = 1; ext.e_len = 0;
        axfs_pack(&ext, ino_v->i_d, inodes + inosiz);
        ino_v->i_size = blksiz;
    }
    ino_v->i_chksum = axfs_crc32(0, inodes + 8, inosiz - 8);
    inodes += inosiz;

    /* root inode */
    sb->s_root_fid = inofid++;
    ino_r = (axfs_inode_t*)inodes;
    memcpy(&ino_r->i_magic, AXFS_IN_MAGIC, 4);
    ino_r->i_type = AXFS_FT_DIR;
    ino_r->i_mime = AXFS_MT_ROOT;
    ino_r->i_nlinks = 1;
    ino_r->i_btime = dev_now_msec;
    memcpy(&ino_r->i_owner.Data1, "root", 4);
    ino_r->i_owner.access = AXFS_READ | AXFS_WRITE | AXFS_EXEC;
    ino_r->i_flags = 1;
    dir = (axfs_dirhdr_t*)((uint8_t*)ino_r + (inosiz == 256 ? 128 : 256));
    memcpy(dir->d_magic, AXFS_DR_MAGIC, 4);
    dir->d_size = sizeof(axfs_dirhdr_t);
    dir->d_self_fid = sb->s_root_fid;
    ino_r->i_size = dir->d_size + 1;
    ino_r->i_chksum = axfs_crc32(0, inodes + 8, inosiz - 8);
    inodes += inosiz;

    /* journal inode */
    if(jblks) {
        sb->s_journal_fid = inofid++;
        ino_j = (axfs_inode_j_t*)inodes;
        memcpy(&ino_j->i_magic, AXFS_IN_MAGIC, 4);
        ino_j->i_type = AXFS_FT_INT_JRNL;
        ino_j->i_blk = fre.e_blk;
        ino_j->i_len = jblks;
        ino_j->i_chksum = axfs_crc32(0, inodes + 8, inosiz - 8);
        inodes += inosiz;
        fre.e_blk += jblks; fre.e_num -= jblks;
    }

    /* free blocks inode */
    sb->s_freeblk_fid = inofid++;
    ino_d = (axfs_inode_d_t*)inodes;
    memcpy(&ino_d->i_magic, AXFS_IN_MAGIC, 4);
    ino_d->i_type = AXFS_FT_INT_FREE;
    ino_d->i_size = (fre.e_num - nbad) * blksiz;
    if(nbad && bad) {
        /* we must exclude bad blocks from free blocks list */
        ext.e_blk = fre.e_blk; ext.e_len = 0;
        ptr = fd = ino_d->i_d;
        top = inodes + inosiz;
        for(i = 0; i < nbad; i++) {
            while(i < nbad && bad[i] == ext.e_blk) { ext.e_blk++; i++; }
            ext.e_num = (i == nbad ? numblks : bad[i]) - ext.e_blk;
            /* if there's not enough space in the inode */
            if(ext.e_num && !(ptr2 = axfs_pack(&ext, ptr, top))) {
                if(top != inodes + inosiz) goto err;
                /* copy data stream descriptor to a new block */
                memcpy(hdr + hdrsiz, &ino_d->i_d, ptr - ino_d->i_d);
                ptr = fd = hdr + hdrsiz + (ptr - ino_d->i_d); top = hdr + hdrsiz + blksiz;
                axfs_pack(&ext, ptr, top);
                /* place an indirect descriptor in inode pointing to this new block */
                memset(&ino_d->i_d, 0, inodes + inosiz - ino_d->i_d);
                ext.e_blk = hdrsiz / blksiz; ext.e_num = (fre.e_num - nbad); ext.e_len = 1;
                axfs_pack(&ext, ino_d->i_d, inodes + inosiz);
                hdrsiz += blksiz;
                ino_d->i_nblks++;
            } else ptr = ptr2;
        }
        fd2 = ptr;
    } else
        axfs_pack(&fre, ino_d->i_d, inodes + inosiz);
    ino_d->i_chksum = axfs_crc32(0, inodes + 8, inosiz - 8);
    inodes += inosiz;

    /* bad blocks inode */
    if(nbad && bad) {
        sb->s_badblk_fid = inofid++;
        ino_d = (axfs_inode_d_t*)inodes;
        memcpy(&ino_d->i_magic, AXFS_IN_MAGIC, 4);
        ino_d->i_type = AXFS_FT_INT_BAD;
        ino_d->i_size = nbad * blksiz;
        ext.e_len = 0;
        ptr = ino_d->i_d;
        top = inodes + inosiz;
        for(i = 0; i < nbad; i++) {
            /* count continuous blocks */
            ext.e_blk = bad[i]; ext.e_num = 1;
            for(j = i + 1; j < nbad && bad[j] == ext.e_blk + ext.e_num; j++, i++) ext.e_num++;
            /* if there's not enough space in the inode */
            if(!(ptr2 = axfs_pack(&ext, ptr, top))) {
                if(top != inodes + inosiz) goto err;
                /* copy data stream descriptor to a new block */
                memcpy(hdr + hdrsiz, &ino_d->i_d, ptr - ino_d->i_d);
                ptr = hdr + hdrsiz + (ptr - ino_d->i_d); top = hdr + hdrsiz + blksiz;
                axfs_pack(&ext, ptr, top);
                /* place an indirect descriptor in inode pointing to this new block */
                memset(&ino_d->i_d, 0, inodes + inosiz - ino_d->i_d);
                ext.e_blk = hdrsiz / blksiz; ext.e_num = 1;
                axfs_pack(&ext, ino_d->i_d, inodes + inosiz);
                /* modify the first extent in free blocks inode's block list */
                ptr2 = axfs_unpack(fd, &ext);
                ext.e_num--;
                if(ext.e_num) axfs_pack(&ext, fd, fd + inosiz);
                else memcpy(fd, ptr2, fd2 - ptr2 + 1);
                ((axfs_inode_d_t*)(inodes - inosiz))->i_size -= blksiz;
                hdrsiz += blksiz;
                ino_d->i_nblks++;
            } else ptr = ptr2;
        }
        ino_d->i_chksum = axfs_crc32(0, inodes + 8, inosiz - 8);
        inodes += inosiz;
    }

    /* encryption */
    if(uuid) memcpy(&sb->s_uuid, uuid, 16);
    if(passphrase && *passphrase) {
        j = axfs_crc32(0, (uint8_t*)&dev_now_msec, 8);
        for(i = 0; i < 28; i += 4)
            *((uint32_t*)&sb->s_encrypt[i]) = *((uint32_t*)&ctx.sb.s_encrypt[i]) =
                (*((uint32_t*)&sb->s_uuid[i & 15]) ^ j ^ i) * 16807;
        for(i = 0; passphrase[i]; i++);
        ctx.dev = dev; ctx.blksiz = blksiz; ctx.sb.s_blksize = blksize;
        sb->s_enchash = ctx.sb.s_enchash = axfs_crc32(0, (uint8_t*)passphrase, i);
        sb->s_enctype = ctx.sb.s_enctype = AXFS_SB_E_SHA;
        axfs_key(&ctx, (uint8_t*)passphrase, i);
        for(i = sb->s_nresblks; i * blksiz < hdrsiz; i++)
            axfs_encrypt(&ctx, i, hdr + i * blksiz, hdr + i * blksiz);
    }

    /* superblock */
    memcpy(&sb->s_magic, AXFS_SB_MAGIC, 4); memcpy(&sb->s_magic2, AXFS_SB_MAGIC, 4);
    sb->s_major = AXFS_SB_MAJOR; sb->s_minor = AXFS_SB_MINOR;
    sb->s_blksize = blksize;
    sb->s_inoshift = inoshift;
    sb->s_nblks = numblks;
    sb->s_chksum = axfs_crc32(0, hdr + 4, 0xF4);

    /* write to disk */
    ret = dev_write(dev, 0, hdr, hdrsiz) ? hdrsiz / blksiz : 0;

    /* free buffer, return */
err:
#ifndef AXFS_NOMALLOC
    free(hdr);
#endif
    return ret;
}

/**
 * Load initial data and perform other tasks when mounting a volume
 */
int axfs_mount_load(axfs_ctx_t *ctx)
{
    uint64_t lbn;
    axfs_inode_v_t *ino_v;
    axfs_inode_j_t *ino_j;
    axfs_inode_t *ino;

    /* journal inode (optional) */
    if(ctx->sb.s_journal_fid) {
        ino_j = (axfs_inode_j_t*)(ctx->blk[3] + (ctx->sb.s_journal_fid & ctx->inomsk) * ctx->inosiz);
        lbn = ctx->sb.s_journal_fid >> ctx->sb.s_inoshift;
        if(!axfs_load_blk(ctx, lbn, 3) || memcmp(ino_j->i_magic, AXFS_IN_MAGIC, 4) || ino_j->i_type != AXFS_FT_INT_JRNL ||
          (!(ctx->sb.s_flags & AXFS_SB_F_NOCRC) && ino_j->i_chksum != axfs_crc32(0, (uint8_t*)ino_j + 8, ctx->inosiz - 8)) ||
          !ino_j->i_blk || !ino_j->i_len) return 0;
        memcpy(ctx->ino[3], ino_j, ctx->inosiz);
        /* if journal buffer not empty, replay */
        if(ino_j->i_head != ino_j->i_tail) {
            ctx->jbuf = NULL; ctx->jlen = 0;
            axfs_replay_journal(ctx);
        }
    }

    /* root inode */
    ino = (axfs_inode_t*)(ctx->blk[3] + (ctx->sb.s_root_fid & ctx->inomsk) * ctx->inosiz);
    lbn = ctx->sb.s_root_fid >> ctx->sb.s_inoshift;
    if(axfs_load_blk(ctx, lbn, 3) && !memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) &&
      ino->i_type == AXFS_FT_DIR && ino->i_mime == AXFS_MT_ROOT && ((ctx->sb.s_flags & AXFS_SB_F_NOCRC) ||
      ino->i_chksum == axfs_crc32(0, (uint8_t*)ino + 8, ctx->inosiz - 8)))
        memcpy(ctx->ino[1], ino, ctx->inosiz);
    else return 0;

    /* free block inode */
    ino = (axfs_inode_t*)(ctx->blk[3] + (ctx->sb.s_freeblk_fid & ctx->inomsk) * ctx->inosiz);
    lbn = ctx->sb.s_freeblk_fid >> ctx->sb.s_inoshift;
    if(axfs_load_blk(ctx, lbn, 3) && !memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) &&
      ino->i_type == AXFS_FT_INT_FREE && ((ctx->sb.s_flags & AXFS_SB_F_NOCRC) ||
      ino->i_chksum == axfs_crc32(0, (uint8_t*)ino + 8, ctx->inosiz - 8)))
        memcpy(ctx->ino[2], ino, ctx->inosiz);
    else return 0;

    /* attribute index inode (optional) */
    ino = (axfs_inode_t*)(ctx->blk[3] + (ctx->sb.s_attridx_fid & ctx->inomsk) * ctx->inosiz);
    lbn = ctx->sb.s_attridx_fid >> ctx->sb.s_inoshift;
    if(ctx->sb.s_attridx_fid && axfs_load_blk(ctx, lbn, 3) && !memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) &&
      ino->i_type == AXFS_FT_INT_AIDX && ((ctx->sb.s_flags & AXFS_SB_F_NOCRC) ||
      ino->i_chksum == axfs_crc32(0, (uint8_t*)ino + 8, ctx->inosiz - 8)))
        memcpy(ctx->ino[4], ino, ctx->inosiz);

    /* user quota inode (optional) */
    ino = (axfs_inode_t*)(ctx->blk[3] + (ctx->sb.s_uquota_fid & ctx->inomsk) * ctx->inosiz);
    lbn = ctx->sb.s_uquota_fid >> ctx->sb.s_inoshift;
    if(ctx->sb.s_uquota_fid && axfs_load_blk(ctx, lbn, 3) && !memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) &&
      ino->i_type == AXFS_FT_INT_UQTA && ((ctx->sb.s_flags & AXFS_SB_F_NOCRC) ||
      ino->i_chksum == axfs_crc32(0, (uint8_t*)ino + 8, ctx->inosiz - 8)))
        memcpy(ctx->ino[5], ino, ctx->inosiz);

    /* actually we don't need this, bad blocks are already excluded from the free blocks list */
#if 0
    /* bad block inode (optional) */
    ino = (axfs_inode_t*)(ctx->blk[3] + (ctx->sb.s_badblk_fid & ctx->inomsk) * ctx->inosiz);
    lbn = ctx->sb.s_badblk_fid >> ctx->sb.s_inoshift;
    if(ctx->sb.s_badblk_fid && axfs_load_blk(ctx, lbn, 3) && !memcmp(ino->i_magic, AXFS_IN_MAGIC, 4) &&
      ino->i_type == AXFS_FT_INT_BAD && ((ctx->sb.s_flags & AXFS_SB_F_NOCRC) ||
      ino->i_chksum == axfs_crc32(0, (uint8_t*)ino + 8, ctx->inosiz - 8)))
        memcpy(ctx->ino[6], ino, ctx->inosiz);
#endif

    /* update volume inode */
    ino_v = (axfs_inode_v_t*)(ctx->blk[3] + (ctx->sb.s_volume_fid & ctx->inomsk) * ctx->inosiz);
    lbn = ctx->sb.s_volume_fid >> ctx->sb.s_inoshift;
    if(axfs_load_blk(ctx, lbn, 3) && !memcmp(ino_v->i_magic, AXFS_IN_MAGIC, 4) &&
      ino_v->i_type == AXFS_FT_INT_VOL && ((ctx->sb.s_flags & AXFS_SB_F_NOCRC) ||
      ino_v->i_chksum == axfs_crc32(0, (uint8_t*)ino_v + 8, ctx->inosiz - 8))) {
        ino_v->i_state = !ctx->sb.s_journal_fid && ino_v->i_lmtime && !ino_v->i_lutime;
        ino_v->i_lmtime = dev_now_msec;
        ino_v->i_lutime = 0;
#ifndef AXFS_BASICMNT
        if(ino_v->i_nwrite && ino_v->i_cwrite) ino_v->i_cwrite--;
#endif
        ino_v->i_chksum = axfs_crc32(0, (uint8_t*)ino_v + 8, ctx->inosiz - 8);
        memcpy(ctx->ino[0], ino_v, ctx->inosiz);
#ifndef AXFS_BASICMNT
        axfs_write_blk(ctx, lbn, ctx->blk[3]);
        if(ino_v->i_nwrite && !ino_v->i_cwrite) axfs_relocate(ctx);
#endif
        return 1;
    }
    return 0;
}

/**
 * Mount a volume
 */
int axfs_mount(axfs_ctx_t *ctx, void *dev, const char *passphrase)
{
    uint32_t i;
    uint8_t tmp[512];

    /* we don't need all of this data, but we must only read in block sizes. Although we don't know exactly how big is yet */
    if(!ctx || !dev_read(dev, 0, &tmp, sizeof(tmp)) || memcmp(tmp + 4, AXFS_SB_MAGIC, 4) || memcmp(tmp + 252, AXFS_SB_MAGIC, 4) ||
      ((axfs_super_t*)tmp)->s_blksize > 11 || ((axfs_super_t*)tmp)->s_inoshift > 12 ||
      !((axfs_super_t*)tmp)->s_volume_fid || !((axfs_super_t*)tmp)->s_root_fid || !((axfs_super_t*)tmp)->s_freeblk_fid ||
      (!(((axfs_super_t*)tmp)->s_flags & AXFS_SB_F_NOCRC) && ((axfs_super_t*)tmp)->s_chksum != axfs_crc32(0, tmp + 4, 0xF4))) return 0;

    /* initialize our minimal block cache if not compiled without malloc support */
    memset(ctx, 0, sizeof(axfs_ctx_t));
    memcpy(&ctx->sb, tmp, sizeof(axfs_super_t));
    ctx->dev = dev;
    ctx->blksiz = 1 << (ctx->sb.s_blksize + 9);
    ctx->inosiz = ctx->blksiz >> ctx->sb.s_inoshift;
    ctx->inomsk = (1 << ctx->sb.s_inoshift) - 1;
#ifdef AXFS_NOMALLOC
    if(ctx->blksiz > sizeof(ctx->blk[0]) || ctx->inosiz > sizeof(ctx->ino[0])) return 0;
#else
    for(i = 0; i < sizeof(ctx->blk)/sizeof(ctx->blk[0]); i++)
        if(!(ctx->blk[i] = (uint8_t*)malloc(ctx->blksiz))) goto err;
    for(i = 0; i < sizeof(ctx->ino)/sizeof(ctx->ino[0]); i++) {
        if(!(ctx->ino[i] = (uint8_t*)malloc(ctx->inosiz))) goto err;
        memset(ctx->ino[i], 0, ctx->inosiz);
    }
#endif
    if(ctx->sb.s_enchash && passphrase && *passphrase) {
        for(i = 0; passphrase[i]; i++);
        if(ctx->sb.s_enchash == axfs_crc32(0, (uint8_t*)passphrase, i)) axfs_key(ctx, (uint8_t*)passphrase, i);
        else goto err;
    }
    memset(&ctx->lbn[0], 0xff, sizeof(ctx->lbn));
#ifdef AXFS_BASICMNT
    return 1;
#else
    return axfs_mount_load(ctx);
#endif

err:
#ifndef AXFS_NOMALLOC
    for(i = 0; i < sizeof(ctx->blk)/sizeof(ctx->blk[0]); i++)
        if(ctx->blk[i]) free(ctx->blk[i]);
    for(i = 0; i < sizeof(ctx->ino)/sizeof(ctx->ino[0]); i++)
        if(ctx->ino[i]) free(ctx->ino[i]);
#endif
    memset(ctx, 0, sizeof(axfs_ctx_t));
    return 0;
}

/**
 * Umount a volume
 */
int axfs_umount(axfs_ctx_t *ctx)
{
#ifndef AXFS_BASICMNT
    uint64_t lbn;
    axfs_inode_v_t *ino_v;
#endif
#ifndef AXFS_NOMALLOC
    uint32_t i;
#endif

    if(ctx) {
        axfs_free_index(&ctx->dir);
        axfs_replay_journal(ctx);
#ifndef AXFS_BASICMNT
        /* update umount timestamp in volume inode */
        ino_v = (axfs_inode_v_t*)(ctx->blk[0] + (ctx->sb.s_volume_fid & ctx->inomsk) * ctx->inosiz);
        lbn = ctx->sb.s_volume_fid >> ctx->sb.s_inoshift;
        if(ctx->sb.s_volume_fid && axfs_load_blk(ctx, lbn, 0) &&
          !memcmp(ino_v->i_magic, AXFS_IN_MAGIC, 4) && ino_v->i_type == AXFS_FT_INT_VOL) {
            if(ino_v->i_nwrite && ino_v->i_cwrite) ino_v->i_cwrite--;
            ino_v->i_lutime = dev_now_msec;
            ino_v->i_chksum = axfs_crc32(0, (uint8_t*)ino_v + 8, ctx->inosiz - 8);
            axfs_write_blk(ctx, lbn, ctx->blk[0]);
        }
#endif
#ifndef AXFS_NOMALLOC
        for(i = 0; i < sizeof(ctx->blk)/sizeof(ctx->blk[0]); i++)
            if(ctx->blk[i]) free(ctx->blk[i]);
        for(i = 0; i < sizeof(ctx->ino)/sizeof(ctx->ino[0]); i++)
            if(ctx->ino[i]) free(ctx->ino[i]);
#endif
        memset(ctx, 0, sizeof(axfs_ctx_t));
        return 1;
    }
    return 0;
}

/**
 * Replace encryption passphrase without re-encoding the volume
 */
int axfs_rekey(axfs_ctx_t *ctx, const char *oldpass, const char *newpass)
{
    int i, ol, nl;
    axfs_sha256_ctx_t sha;
    uint8_t tmp[32], tmp2[32], blk[512];

    if(!ctx || !ctx->sb.s_enchash || !oldpass || !*oldpass || !newpass || !*newpass ||
      /* superblock is smaller than the minimum block size */
      !dev_read(ctx->dev, 0, blk, sizeof(blk))) return 0;
    for(ol = 0; oldpass[ol]; ol++);
    for(nl = 0; newpass[nl]; nl++);
    axfs_sha256_i(&sha);
    axfs_sha256_u(&sha, oldpass, ol);
    axfs_sha256_f(&sha, tmp);
    axfs_sha256_i(&sha);
    axfs_sha256_u(&sha, tmp, 32);
    axfs_sha256_u(&sha, &ctx->sb.s_enchash, 4);
    axfs_sha256_f(&sha, tmp);
    for(i = 0; i < 28; i++) tmp[i] ^= ctx->sb.s_encrypt[i] ^ (uint8_t)(oldpass[0] + i);
    ctx->sb.s_enchash = axfs_crc32(0, (uint8_t*)newpass, nl);
    axfs_sha256_i(&sha);
    axfs_sha256_u(&sha, newpass, nl);
    axfs_sha256_f(&sha, tmp2);
    axfs_sha256_i(&sha);
    axfs_sha256_u(&sha, tmp2, 32);
    axfs_sha256_u(&sha, &ctx->sb.s_enchash, 4);
    axfs_sha256_f(&sha, tmp2);
    for(i = 0; i < 28; i++) ctx->sb.s_encrypt[i] = tmp[i] ^ (tmp2[i] ^ (uint8_t)(newpass[0] + i));
    ctx->sb.s_chksum = axfs_crc32(0, ctx->sb.s_magic, 0xF4);
    memcpy(blk, &ctx->sb, sizeof(axfs_super_t));
    return dev_write(ctx->dev, 0, blk, sizeof(blk));
}

/**
 * Look up a d_fid in at_fid and return its filename
 */
int axfs_lookupat_name(axfs_ctx_t *ctx, uint64_t at_fid, uint64_t d_fid, char *d_name)
{
    axfs_inode_t *ino;
    axfs_extent_t ext;
    uint8_t tmp[256], *d, *n;
    uint32_t l, s, offs;

    if(!ctx || !at_fid || !d_fid || d_fid == ctx->sb.s_root_fid || !d_name || !(ino = axfs_get_inode(ctx, at_fid, 1))) return 0;

    memset(tmp, 0, sizeof(tmp));
    for(offs = sizeof(axfs_dirhdr_t); offs < ctx->dir.hdr.d_size; offs += d - tmp + 1) {
        if(!(l = axfs_read_inode(ctx, ino, offs, sizeof(tmp), tmp)) || !tmp[0]) break;
        d = n = axfs_unpack(tmp, &ext); for(s = 0; d < tmp + l && *d; d++, s++);
        if(ext.e_blk == d_fid) { memcpy(d_name, n, s + 1); return s; }
    }
    return 0;
}

/**
 * Look up a filename in at_fid and return its fid
 */
uint64_t axfs_lookupat_fid(axfs_ctx_t *ctx, uint64_t at_fid, const char *d_name, uint32_t len)
{
    char dn[AXFS_FILENAME_MAX + 1];
    axfs_inode_t *ino;
    axfs_extent_t ext;
    uint8_t tmp[256], *d, *n;
    uint32_t l, s, offs;
    uint64_t fid = 0;

    if(!ctx || !at_fid || !d_name || len > AXFS_FILENAME_MAX || !(ino = axfs_get_inode(ctx, at_fid, 1)))
        return 0;
    if(!axfs_read_index(ctx, ino, at_fid, &ctx->dir)) return 0;
    if(!*d_name || !len) return at_fid;

    for(l = 0; *d_name && l < len; d_name++, l++)
        if(*d_name == AXFS_DIRSEP) { if(d_name[1]) dn[l] = '_'; else break; } else dn[l] = *d_name;
    dn[l] = 0;

    if(!ctx->dir.idx) {
        /* no indextable, read sequentially */
        memset(tmp, 0, sizeof(tmp));
        for(offs = sizeof(axfs_dirhdr_t); offs < ctx->dir.hdr.d_size; offs += d - tmp + 1) {
            if(!(l = axfs_read_inode(ctx, ino, offs, sizeof(tmp), tmp)) || !tmp[0]) break;
            /* decompress entry */
            d = n = axfs_unpack(tmp, &ext); for(s = 0; d < tmp + l && *d; d++, s++);
            if(ext.e_blk && s == len && !memcmp(n, dn, len)) return ext.e_blk;
        }
    } else {
        /* we can do O(log2) search */
        if(axfs_bsearch_index(ctx, ino, &ctx->dir, (const char*)dn, &offs, &fid) == 1) return fid;
    }
    return 0;
}

/**
 * Look up a path and return its fid
 */
uint64_t axfs_lookup_fid(axfs_ctx_t *ctx, const char *path)
{
    uint64_t fid = 0, parent;
    char *s, *e;

    if(!ctx || !path) return 0;
    parent = ctx->sb.s_root_fid;
    if(!*path) return parent;
    s = (char*)path; if(*s == AXFS_DIRSEP) { if(!s[1]) { return parent; } s++; }
    while(*s) {
        for(e = s; *e && *e != AXFS_DIRSEP; e++);
        fid = axfs_lookupat_fid(ctx, parent, s, e - s);
        if(!fid) return 0;
        if(e[0] && e[1]) { s = e + 1; parent = fid; }
        else break;
    }
    return fid;
}

/**
 * Get the base name of a path
 */
int axfs_basename(axfs_ctx_t *ctx, const char *path, uint64_t *at_fid, char **d_name)
{
    uint64_t fid = 0, parent;
    char *s, *e, *p, *b = NULL;

    if(!ctx || !path || !*path || !at_fid || !d_name) return 0;

    parent = ctx->sb.s_root_fid;
    *at_fid = 0; *d_name = NULL;
    s = (char*)path; if(*s == AXFS_DIRSEP) s++;
    for(p = s; *p && (*p != AXFS_DIRSEP || p[1]); p++) if(*p == AXFS_DIRSEP) b = p;
    if(b) {
        while(s < b && *s) {
            for(e = s; e < b && *e && *e != AXFS_DIRSEP; e++);
            fid = axfs_lookupat_fid(ctx, parent, s, e - s);
            if(!fid) return 0;
            if(e < b) { s = e + 1; parent = fid; }
            else break;
        }
    }
    *at_fid = parent;
    *d_name = s;
    return axfs_lookupat_fid(ctx, parent, s, p - s) ? -1 : 1;
}

/**
 * Create a hard link in a directory
 */
int axfs_link(axfs_ctx_t *ctx, uint64_t at_fid, uint64_t d_fid, const char *d_name)
{
    axfs_inode_t *ino;
    axfs_extent_t ext;
    uint32_t len, o, j;
    uint8_t *r, *n, *d, *p, run[16 + AXFS_FILENAME_MAX];
    int i, ret = 0;

    if(!ctx || !at_fid || !d_fid || !d_name || !*d_name || !(ino = axfs_get_inode(ctx, at_fid, 1)) ||
      ino->i_type != AXFS_FT_DIR || !axfs_get_inode(ctx, d_fid, 0)) return 0;

    /* create a compressed directory entry */
    ext.e_blk = d_fid; ext.e_num = 0; ext.e_len = 0;
    n = axfs_pack(&ext, run, run + 16);
    if(*d_name == AXFS_DIRSEP) d_name++;
    for(r = n; *d_name && r < n + AXFS_FILENAME_MAX; d_name++, r++)
        if(*d_name == AXFS_DIRSEP) { if(d_name[1]) *r = '_'; else break; } else *r = *d_name;
    *r++ = 0; *r = 0;
    len = (r - run);

    /* check if this name is unique or not (also caches the indextable in ctx->dir.idx) */
    if(!ctx->dir.idx && axfs_lookupat_fid(ctx, at_fid, (const char*)n, r - n - 1)) return 0;

#ifndef AXFS_NOMALLOC
    if((ctx->dir.idx = realloc(ctx->dir.idx, (ctx->dir.hdr.d_nument + 1) * sizeof(uint32_t))) &&
      (ctx->dir.data = (uint8_t*)realloc(ctx->dir.data, ctx->dir.hdr.d_size + 1)) &&
      axfs_read_inode(ctx, ino, 0, ctx->dir.hdr.d_size + 1, ctx->dir.data)) {
        if(ctx->dir.idxlen) {
            /* we have a valid indextable */
            if(ctx->dir.idxent != AXFS_IDXENT(ctx->dir.hdr.d_size + len)) {
                /* expand index table from 16 bits to 32 bits */
                for(i = ctx->dir.hdr.d_nument - 1; i >= 0; i--)
                    ((uint32_t*)ctx->dir.idx)[i] = ((uint16_t*)ctx->dir.idx)[i];
                ctx->dir.idxent = sizeof(uint32_t);
            }
        } else {
            /* we couldn't read in the indextable, we must rebuild it */
            ctx->dir.hdr.d_nument = 0;
            for(d = ctx->dir.data + sizeof(axfs_dirhdr_t); d < ctx->dir.data + ctx->dir.hdr.d_size && *d; ) {
                j = (uint32_t)(d - ctx->dir.data); d = axfs_unpack(d, &ext); p = d;
                while(d < ctx->dir.data + ctx->dir.hdr.d_size && *d) d++;
                d++;
                if(ext.e_blk) {
                    /* in this case this should never fail and never should find a match (get the position to insert to) */
                    if(axfs_bsearch_index(ctx, ino, &ctx->dir, (const char *)p, &o, NULL)) break;
                    axfs_insert_index(&ctx->dir, o, j);
                    ctx->dir.hdr.d_nument++;
                }
            }
        }
        /* add the new entry to the indextable */
        if(axfs_bsearch_index(ctx, ino, &ctx->dir, (const char*)n, &o, NULL)) return 0;
        axfs_insert_index(&ctx->dir, o, ctx->dir.hdr.d_size);
    }
#endif
    ctx->dir.hdr.d_nument++;
    if(ctx->dir.idx) {
        ctx->dir.idxlen = ctx->dir.hdr.d_nument * ctx->dir.idxent;
        ctx->dir.hdr.d_chksum = axfs_crc32(0, (uint8_t*)ctx->dir.idx, ctx->dir.idxlen);
    } else {
        ctx->dir.idxlen = 0;
        ctx->dir.hdr.d_chksum = 0;
    }

    /* normal write procedure */
    if(axfs_write_inode(ctx, ino, ctx->dir.hdr.d_size, len + 1, run) &&
       (!ctx->dir.idx || axfs_write_inode(ctx, ino, (ctx->dir.hdr.d_size + len + 4) & ~3, ctx->dir.idxlen, ctx->dir.idx))) {
        ctx->dir.hdr.d_size += len;
        axfs_write_inode(ctx, ino, 0, sizeof(axfs_dirhdr_t), &ctx->dir.hdr);
        if(!ctx->dir.idx) axfs_trunc_inode(ctx, ino, ctx->dir.hdr.d_size + 1);
        axfs_ino->i_nlinks++;
        axfs_put_inode(ctx, axfs_ino, d_fid, 0);
        axfs_put_inode(ctx, ino, at_fid, 1);
        ret = axfs_commit(ctx);
    }
    return ret;
}

/**
 * Delete a hard link from a directory
 */
int axfs_unlink(axfs_ctx_t *ctx, uint64_t at_fid, const char *d_name)
{
    if(!ctx || !at_fid || !d_name || !*d_name) return 0;
    return 1;
}

/**
 * Delete a file or adirectory sub-tree
 */
int axfs_remove(axfs_ctx_t *ctx, const char *path)
{
    if(!ctx || !path || !*path) return 0;
    axfs_commit(ctx);
    return 1;
}

/**
 * Create a device file
 */
int axfs_mknod(axfs_ctx_t *ctx, const char *path, uint64_t mode, uint64_t dev_major, uint64_t dev_minor)
{
    char *s = NULL;
    uint64_t fid, parent, tmp[3];

    if(!ctx || !path || !*path || axfs_basename(ctx, path, &parent, &s) != 1 || !s ||
      !(fid = axfs_alloc_inode(ctx, AXFS_FT_DEV, 0, parent))) return 0;
    tmp[0] = mode; tmp[1] = dev_major; tmp[2] = dev_minor;
    return axfs_write_inode(ctx, axfs_ino, 0, sizeof(tmp), tmp) && axfs_link(ctx, parent, fid, s);
}

/**
 * Create a named pipe
 */
int axfs_mkfifo(axfs_ctx_t *ctx, const char *path, uint64_t mode)
{
    char *s = NULL;
    uint64_t fid, parent;

    if(!ctx || !path || !*path || axfs_basename(ctx, path, &parent, &s) != 1 || !s ||
      !(fid = axfs_alloc_inode(ctx, AXFS_FT_PIPE, 0, parent))) return 0;
    return axfs_write_inode(ctx, axfs_ino, 0, sizeof(mode), &mode) && axfs_link(ctx, parent, fid, s);
}

/**
 * Create a directory, returns d_fid
 */
uint64_t axfs_mkdirat(axfs_ctx_t *ctx, uint64_t at_fid, const char *d_name)
{
    uint64_t fid;
    uint32_t l;

    if(!ctx || !at_fid || !d_name || !*d_name) return 0;
    for(l = 0; d_name[l]; l++);
    return axfs_lookupat_fid(ctx, at_fid, d_name, l) || !(fid = axfs_alloc_inode(ctx, AXFS_FT_DIR, 0, at_fid)) ||
      !axfs_link(ctx, at_fid, fid, d_name) ? 0 : fid;
}

/**
 * Create a directory
 */
int axfs_mkdir(axfs_ctx_t *ctx, const char *path)
{
    char *s = NULL;
    uint64_t fid, parent;

    return ctx && path && *path && axfs_basename(ctx, path, &parent, &s) == 1 && s &&
      (fid = axfs_alloc_inode(ctx, AXFS_FT_DIR, 0, parent)) ? axfs_link(ctx, parent, fid, s) : 0;
}

/**
 * Create a live directory (search results)
 */
int axfs_mksdr(axfs_ctx_t *ctx, const char *path, const char *search)
{
    char *s = NULL;
    uint64_t fid, parent;
    uint32_t len;

    if(!ctx || !path || !*path || !search || !*search || axfs_basename(ctx, path, &parent, &s) != 1 || !s ||
      !(fid = axfs_alloc_inode(ctx, AXFS_FT_SDR, 0, parent))) return 0;
    for(len = 0; search[len]; len++);
    len++;
    return axfs_write_inode(ctx, axfs_ino, 0, len, (void*)search) && axfs_link(ctx, parent, fid, s);
}

/**
 * Create a directory union
 */
int axfs_mkuni(axfs_ctx_t *ctx, const char *path, const char **targets)
{
    char *s = NULL;
    uint64_t fid, parent;
    uint32_t i, len, offs = 0;

    if(!ctx || !path || !*path || !targets || !*targets || !**targets || axfs_basename(ctx, path, &parent, &s) != 1 || !s ||
      !(fid = axfs_alloc_inode(ctx, AXFS_FT_UNI, 0, parent))) return 0;
    for(i = 0; targets[i]; i++) {
        for(len = 0; targets[i][len]; len++);
        len++;
        if(!axfs_write_inode(ctx, axfs_ino, offs, len, (void*)targets[i])) return 0;
        offs += len;
    }
    return axfs_write_inode(ctx, axfs_ino, offs, 1, (void*)"\0") && axfs_link(ctx, parent, fid, s);
}

/**
 * Create a symbolic link
 */
int axfs_symlink(axfs_ctx_t *ctx, const char *path, const char *target)
{
    char *s = NULL;
    uint64_t fid, parent;
    uint32_t len;

    if(!ctx || !path || !*path || !target || !*target || axfs_basename(ctx, path, &parent, &s) != 1 || !s ||
      !(fid = axfs_alloc_inode(ctx, AXFS_FT_LNK, 0, parent))) return 0;
    for(len = 0; target[len]; len++);
    len++;
    return axfs_write_inode(ctx, axfs_ino, 0, len, (void*)target) && axfs_link(ctx, parent, fid, s);
}

/**
 * Get symbolic link target
 */
int axfs_readlink(axfs_ctx_t *ctx, const char *path, char *outbuf, uint32_t outlen)
{
    axfs_inode_t *ino;
    uint64_t fid;

    return !ctx || !path || !*path || !outbuf || !outlen ||
      !(fid = axfs_lookup_fid(ctx, path)) || !(ino = axfs_get_inode(ctx, fid, 0)) ? 0 :
      !!axfs_read_inode(ctx, ino, 0, outlen - 1, outbuf);
}

/**
 * Create or open an existing file for reading / writing
 */
uint64_t axfs_openat(axfs_file_t *file, axfs_ctx_t *ctx, uint64_t at_fid, const char *d_name, int create)
{
    axfs_inode_t *ino;
    int i, l;
    uint8_t filetype = AXFS_FT_APPL;
    uint32_t mimetype = AXFS_MT_UNKN;

    if(!ctx || !file || !at_fid || !d_name || (!*d_name && at_fid != ctx->sb.s_root_fid)) return 0;

    memset(file, 0, sizeof(axfs_file_t));
    for(l = 0; d_name[l]; l++);
    file->fid = axfs_lookupat_fid(ctx, at_fid, d_name, l);
    if(!file->fid) {
        if(!create) return 0;
        for(i = 0; axfs_mimetypes[i].extention; i++)
            if(l > axfs_mimetypes[i].len &&
              !memcmp(d_name + l - axfs_mimetypes[i].len, axfs_mimetypes[i].extention, axfs_mimetypes[i].len)) {
                filetype = axfs_mimetypes[i].filetype;
                mimetype = axfs_attr_offs(ctx, axfs_mimetypes[i].mimetype, 0);
                break;
              }
        if(!(file->fid = axfs_alloc_inode(ctx, filetype, mimetype, at_fid))) return 0;
        if(!(i = axfs_link(ctx, at_fid, file->fid, d_name))) { axfs_free_inode(ctx, file->fid); file->fid = 0; }
    }
    if(!(ino = axfs_get_inode(ctx, file->fid, 0))) return 0;
#ifndef AXFS_NOMALLOC
    if(!(file->ino = (uint8_t*)malloc(ctx->inosiz))) return 0;
#endif
    if(create) { axfs_trunc_inode(ctx, (axfs_inode_t*)file->ino, 0); }
    else if(ino->i_type == AXFS_FT_DIR) axfs_read_index(ctx, ino, file->fid, &file->dir);
    memcpy(file->ino, ino, ctx->inosiz);
    file->ctx = ctx;
    return file->fid;
}

/**
 * Open an existing file for reading / writing
 */
int axfs_open(axfs_file_t *file, axfs_ctx_t *ctx, const char *path, int create)
{
    char *s = NULL;
    uint64_t fid;

    return axfs_basename(ctx, path, &fid, &s) == -1 ? !!axfs_openat(file, ctx, fid, s, create) : 0;
}

/**
 * Seek into position in an opened file
 */
uint64_t axfs_seek(axfs_file_t *file, int64_t offset, int whence)
{
    axfs_inode_t *ino;

    if(!file) return 0;
    ino = (axfs_inode_t*)file->ino;
    switch(whence) {
        case SEEK_SET: file->offs = offset < 0 ? 0 : ((uint64_t)offset > ino->i_size ? ino->i_size : (uint64_t)offset); break;
        case SEEK_CUR: file->offs = (int64_t)file->offs + offset < 0 ? 0 : (file->offs + offset > ino->i_size ? ino->i_size :
            file->offs + offset); break;
        case SEEK_END: file->offs = ino->i_size < (uint64_t)-offset ? 0 : ino->i_size; break;
    }
    return file->offs;
}

/**
 * Read from opened file
 */
uint64_t axfs_read(axfs_file_t *file, void *buffer, uint64_t size)
{
    uint64_t len;

    if(!file || !buffer || !size) return 0;
    len = axfs_read_inode(file->ctx, (axfs_inode_t*)file->ino, file->offs, size, buffer);
    file->offs += len;
    return len;
}

/**
 * Write to opened file
 */
uint64_t axfs_write(axfs_file_t *file, void *buffer, uint64_t size)
{
    uint64_t len;

    if(!file || !buffer || !size) return 0;
    len = axfs_write_inode(file->ctx, (axfs_inode_t*)file->ino, file->offs, size, buffer);
    file->offs += len;
    axfs_commit(file->ctx);
    return len;
}

/**
 * Close an opened file
 */
int axfs_close(axfs_file_t *file)
{
    if(!file || !file->fid) return 0;
#ifndef AXFS_NOMALLOC
    if(file->ino) free(file->ino);
#endif
    axfs_free_index(&file->dir);
    axfs_commit(file->ctx);
    memset(file, 0, sizeof(axfs_file_t));
    return 1;
}

/**
 * Traversing directory
 */
int axfs_readdir(axfs_file_t *file, axfs_dirent_t *ent)
{
    axfs_extent_t ext;
    uint8_t tmp[256], *d;
    uint32_t len;

    if(!file || !ent || !file->dir.fid) return 0;
    memset(ent, 0, sizeof(axfs_dirent_t));
    memset(tmp, 0, sizeof(tmp));
    if(!file->dir.idx) {
        /* no index, read sequentially */
        if(file->offs < sizeof(axfs_dirhdr_t)) file->offs = sizeof(axfs_dirhdr_t);
        if(!(len = axfs_read_inode(file->ctx, (axfs_inode_t*)file->ino, file->offs, sizeof(tmp), tmp)) || !tmp[0]) return 0;
    } else {
        /* we can do alphabetical order */
        if(file->offs >= file->dir.hdr.d_nument || !(len = axfs_read_inode(file->ctx, (axfs_inode_t*)file->ino,
            file->dir.idxent == 2 ? ((uint16_t*)file->dir.idx)[file->offs] : ((uint32_t*)file->dir.idx)[file->offs],
            sizeof(tmp), tmp)) || !tmp[0]) return 0;
    }
    /* decompress entry */
    for(d = axfs_unpack(tmp, &ext); d < tmp + len && *d && ent->d_nlen < AXFS_FILENAME_MAX; d++, ent->d_nlen++)
        ent->d_name[ent->d_nlen] = *d;
    if(!file->dir.idx) file->offs += d - tmp;
    ent->d_fid = ext.e_blk;
    file->offs++;
    return 1;
}

/**
 * Truncate an inode's data stream
 */
int axfs_truncate(axfs_file_t *file, uint64_t offs)
{
    return !file || !file->ctx || !file->fid ? 0 : axfs_trunc_inode(file->ctx, (axfs_inode_t*)file->ino, offs) &&
        axfs_put_inode(file->ctx, (axfs_inode_t*)file->ino, file->fid, 0) && axfs_commit(file->ctx);
}

/**
 * Set the inode's owner and their access bits
 */
int axfs_chowner(axfs_file_t *file, axfs_access_t *acl)
{
    axfs_inode_t *ino;

    if(!file || !file->ctx || !file->fid || !acl) return 0;
    ino = (axfs_inode_t*)file->ino;
    memcpy(&ino->i_owner, acl, sizeof(axfs_access_t));
    return axfs_put_inode(file->ctx, ino, file->fid, 0) && axfs_commit(file->ctx);
}

/**
 * Set the inode's group and their access bits
 */
int axfs_chgroup(axfs_file_t *file, axfs_access_t *acl)
{
    axfs_inode_t *ino;

    if(!file || !file->ctx || !file->fid || !acl) return 0;
    ino = (axfs_inode_t*)file->ino;
    memcpy(&ino->i_acl[0], acl, sizeof(axfs_access_t));
    return axfs_put_inode(file->ctx, ino, file->fid, 0) && axfs_commit(file->ctx);
}

/**
 * Set the inode's other groups and their access bits
 */
int axfs_chother(axfs_file_t *file, axfs_access_t *acl)
{
    axfs_inode_t *ino;

    if(!file || !file->ctx || !file->fid || !acl) return 0;
    ino = (axfs_inode_t*)file->ino;
    memcpy(&ino->i_acl[1], acl, sizeof(axfs_access_t));
    return axfs_put_inode(file->ctx, ino, file->fid, 0) && axfs_commit(file->ctx);
}

/**
 * Set basic attributes
 * for directories, `mime` should point to a GUI attribute array
 */
int axfs_setattr(axfs_file_t *file, uint8_t type, const char *mime, const char *icon)
{
    axfs_inode_t *ino;

    if(!file || !file->ctx || !file->fid || (type < AXFS_FT_BOOT && type != AXFS_FT_DIR) || type > 127 ||
      (((axfs_inode_t*)file->ino)->i_type < AXFS_FT_BOOT && ((axfs_inode_t*)file->ino)->i_type != AXFS_FT_DIR) ||
      ((axfs_inode_t*)file->ino)->i_type > 127) return 0;
    ino = (axfs_inode_t*)file->ino;
    if(ino->i_type == AXFS_FT_DIR) {
        if(mime) axfs_write_inode(file->ctx, ino, 24, 8, (uint8_t*)mime);
    } else {
        if(type != AXFS_FT_DIR) ino->i_type = type;
        ino->i_mime = axfs_attr_offs(file->ctx, mime, 0);
    }
    axfs_setxattr(file, "icon", icon);
    return axfs_put_inode(file->ctx, ino, file->fid, 0) && axfs_commit(file->ctx);
}

/**
 * Get basic attributes
 */
int axfs_getattr(axfs_file_t *file, axfs_stat_t *st, uint32_t outlen)
{
    axfs_inode_t *ino;
    int i, n;
    char *s, *e;

    if(!file || !file->ctx || !file->fid || !st || outlen < sizeof(axfs_stat_t) || ((axfs_inode_t*)file->ino)->i_type > 127)
        return 0;
    ino = (axfs_inode_t*)file->ino;
    memset(st, 0, outlen);
    st->st_ino = file->fid;
    st->st_size = ino->i_size;
    st->st_size_hi = ino->i_size_hi;
    st->st_type = ino->i_type;
    st->st_blksize = file->ctx->blksiz;
    st->st_blocks = ino->i_nblks;
    st->st_nlink = ino->i_nlinks;
    st->st_btime = ino->i_btime;
    st->st_ctime = ino->i_ctime;
    st->st_mtime = ino->i_mtime;
    if(ino->i_type == AXFS_FT_DIR)
        axfs_read_inode(file->ctx, ino, 24, 8, st->st_gui);
    memcpy(&st->st_owner, &ino->i_owner, sizeof(axfs_access_t));
    for(i = 0, n = file->ctx->inosiz == 256 ? 2 : 10; i < n; i++)
        memcpy(&st->st_acl[i], &ino->i_acl[i], sizeof(axfs_access_t));
    if(outlen > sizeof(axfs_stat_t) + 32) {
        s = (char*)st + sizeof(axfs_stat_t); e = s + outlen;
        i = axfs_getxattr(file, "mime", s, e - s - 1);
        if(i) { st->st_mime = s; s += i + 1; } else s++;
        i = axfs_getxattr(file, "icon", s, e - s - 1);
        if(i) { st->st_icon = s; s += i + 1; } else s++;
        i = axfs_getxattr(file, "path", s, e - s - 1);
        if(i) st->st_path = s;
    }
    return 1;
}

/**
 * Set extended attribute (value of NULL removes attribute)
 */
int axfs_setxattr(axfs_file_t *file, const char *key, const char *value)
{
    axfs_extent_t ext, attr;
    axfs_inode_t *ino;
    axfs_eahdr_t *ea;
    uint8_t *ds, *l, *d;
    uint32_t koffs;

    if(!file || !file->ctx || !file->fid || !key || !*key || !memcmp(key, "path", 5) || !memcmp(key, "size", 5)) return 0;
    ino = (axfs_inode_t*)file->ino;

    /* create attribute index file if it doesn't exists yet */
    if(!file->ctx->sb.s_attridx_fid && !axfs_alloc_spec(file->ctx, AXFS_FT_INT_AIDX)) return 0;

    if(!memcmp(key, "mime", 5)) {
        if(ino->i_type >= AXFS_FT_BOOT && ino->i_type < 128) {
            ino->i_mime = axfs_attr_offs(file->ctx, value, 0);
            return axfs_put_inode(file->ctx, ino, file->fid, 0) && axfs_commit(file->ctx);
        }
        return 0;
    }
    if(!(koffs = axfs_attr_offs(file->ctx, key, 1))) return 0;
    axfs_unpack(ino->i_attr, &attr);
    ea = (axfs_eahdr_t*)file->ctx->blk[0];
    ds = file->ctx->blk[0] + sizeof(axfs_eahdr_t);
    memset(&ext, 0, sizeof(ext));

    /* do we need to add an extended attributes block to the inode? */
    if(!attr.e_blk || !axfs_load_blk(file->ctx, attr.e_blk, 0) || memcmp(ea->a_magic, AXFS_EA_MAGIC, 4) || ea->a_self_fid != file->fid ||
      (!(file->ctx->sb.s_flags & AXFS_SB_F_NOCRC) && ea->a_chksum != axfs_crc32(0, file->ctx->blk[0] + 8, ea->a_size - 8))) {
        /* no ea block and we are supposed to delete attribute? nothing to do then */
        if(!value || !*value) return 1;
        /* allocate a new block */
        if(!attr.e_blk) {
            attr.e_blk = file->ctx->lbn[0] = axfs_alloc_blk(file->ctx, 1);
            ino->i_nblks++;
            axfs_pack(&attr, ino->i_attr, ino->i_attr + 16);
            if(!axfs_put_inode(file->ctx, ino, file->fid, 0)) return 0;
            /* no commit here, we need to write another meta block */
        }
        /* setup ea block */
        memset(file->ctx->blk[0], 0, file->ctx->blksiz);
        memcpy(ea->a_magic, AXFS_EA_MAGIC, 4);
        ea->a_self_fid = file->fid;
        ea->a_size = sizeof(axfs_eahdr_t);
        l = d = ds;
    } else {
        /* we have a valid ea block, locate attribute */
        for(l = d = ds; *d;) {
            l = d; d = axfs_unpack(d, &ext);
            if(ext.e_blk == koffs) break;
        }
    }
    ext.e_num = axfs_attr_offs(file->ctx, value, 0);
    if(ext.e_blk == koffs && !ext.e_num) ea->a_nument--;
    else if(ext.e_blk != koffs) ea->a_nument++;
    /* insert/update attribute (or delete if there's no value) */
    ext.e_len = 0;
    ext.e_blk = koffs;
    axfs_ext_ds(file->ctx, (axfs_inode_t*)file->ino, &ext, l, d, file->ctx->blk[0] + file->ctx->blksiz, 0);
    for(; *l; l = axfs_skip(l));
    ea->a_size = l - file->ctx->blk[0];
    /* no attributes? if so, free the entire ea block */
    if(ea->a_size <= sizeof(axfs_eahdr_t) + 1) {
        axfs_free_blk(file->ctx, attr.e_blk, 1);
        attr.e_blk = 0;
        ino->i_nblks--;
        axfs_pack(&attr, ino->i_attr, ino->i_attr + 16);
        return axfs_put_inode(file->ctx, ino, file->fid, 0) && axfs_commit(file->ctx);
    }
    ea->a_chksum = axfs_crc32(0, file->ctx->blk[0] + 8, ea->a_size - 8);
    return axfs_write_meta(file->ctx, file->ctx->lbn[0], file->ctx->blk[0]) && axfs_commit(file->ctx);
}

/**
 * Get extended attribute
 */
int axfs_getxattr(axfs_file_t *file, const char *key, char *outval, uint32_t outlen)
{
    axfs_extent_t ext;
    axfs_inode_t *ino;
    axfs_eahdr_t *ea;
    char tmp[256];
    uint32_t l, len;
    uint64_t size, fid;
    uint8_t *d, *e;

    if(!file || !file->ctx || !file->fid || !key || !*key || !outval || !outlen) return 0;
    ino = (axfs_inode_t*)file->ino;

    /* built-in: file size (just convert binary to ASCII) */
    if(!memcmp(key, "size", 5)) {
        if(!ino->i_size) { *outval = '0'; return 1; }
        memset(tmp, 0, sizeof(tmp)); l = 0;
        for(size = ino->i_size; size && l < 127; l++, size /= 10)
            tmp[127 - l] = (size % 10) + '0';
        if(outlen > l) { memcpy(outval, tmp + 128 - l, l + 1); return l; }
        return 0;
    }

    /* built-in: file type */
    if(!memcmp(key, "mime", 5)) {
        if(file->ctx->sb.s_attridx_fid && ino->i_mime != AXFS_MT_UNKN) {
            memset(tmp, 0, sizeof(tmp));
            len = axfs_read_inode(file->ctx, (axfs_inode_t*)file->ctx->ino[4], ino->i_mime, sizeof(tmp), tmp);
            for(l = 0; l < len && tmp[l]; l++);
            if(l && outlen >= l) { memcpy(outval, tmp, l); return l; }
        } else {
            if(outlen > 24) { memcpy(outval, "application/octet-stream\0", 25); return 24; }
        }
        return 0;
    }

    /* built-in: file path */
    if(!memcmp(key, "path", 5)) {
        file->ctx->lbn[3] = -1U; size = file->ctx->blksiz; file->ctx->blk[3][size--] = 0;
        for(fid = file->fid; ino->i_attr[0]; ) {
            axfs_unpack(ino->i_attr, &ext);
            if(!(l = axfs_lookupat_name(file->ctx, ext.e_num, fid, tmp))) break;
            if(size > l) {
                if(size != file->ctx->blksiz - 1) file->ctx->blk[3][size--] = AXFS_DIRSEP;
                size -= l; memcpy(file->ctx->blk[3] + size, tmp, l);
            } else break;
            fid = ext.e_num;
            ino = (axfs_inode_t*)file->ctx->ino[7];
        }
        len = file->ctx->blksiz - size;
        if(len > 1 && outlen > len) { memcpy(outval, file->ctx->blk[3] + size, len); return len - 1; }
        return 0;
    }

    if(!file->ctx->sb.s_attridx_fid || !ino->i_attr[0]) return 0;
    axfs_unpack(ino->i_attr, &ext);
    ea = (axfs_eahdr_t*)file->ctx->blk[0];

    /* everything else, even "icon" is just a standard extended attribute */
    for(len = 0; key[len]; len++);
    if(ext.e_blk && axfs_load_blk(file->ctx, ext.e_blk, 0) && !memcmp(ea->a_magic, AXFS_EA_MAGIC, 4) && ea->a_self_fid == file->fid &&
      (file->ctx->sb.s_flags & AXFS_SB_F_NOCRC || ea->a_chksum == axfs_crc32(0, file->ctx->blk[0] + 8, ea->a_size - 8)) &&
      (fid = axfs_attr_offs(file->ctx, key, -1)) != AXFS_MT_UNKN) {
        /* only compare offset with e_blk */
        for(e = file->ctx->blk[0] + ea->a_size, d = file->ctx->blk[0] + sizeof(axfs_eahdr_t); d < e && *d; ) {
            d = axfs_unpack(d, &ext); memset(tmp, 0, sizeof(tmp));
            if(ext.e_blk == fid) {
                /* attribute key found, now load attribute value and copy it to outval */
                for(len = 0, l = sizeof(tmp); l == sizeof(tmp) && outlen > 1;) {
                    memset(tmp, 0, sizeof(tmp));
                    if(!axfs_read_inode(file->ctx, (axfs_inode_t*)file->ctx->ino[4], ext.e_num + len, sizeof(tmp), tmp)) break;
                    for(l = 0; l < sizeof(tmp) && tmp[l] && outlen > 1; l++, len++)
                        *outval++ = tmp[l];
                }
                *outval = 0;
                return len;
            }
        }
    }
    return 0;
}

/**
 * Get all extended attribute keys
 */
int axfs_listxattr(axfs_file_t *file, char *outval, uint32_t outlen)
{
    axfs_extent_t ext;
    axfs_inode_t *ino;
    axfs_eahdr_t *ea;
    uint32_t ret = 0, l;
    uint8_t *d, *e;
    char tmp[256];

    if(!file || !file->ctx || !file->fid || !outval || !outlen) return 0;
    ino = (axfs_inode_t*)file->ino;

    if(outlen >= 5) { memcpy(outval + ret, "path\0", 5); ret += 5; outlen -= 5; }
    if(outlen >= 5) { memcpy(outval + ret, "size\0", 5); ret += 5; outlen -= 5; }
    if(outlen >= 5) { memcpy(outval + ret, "mime\0", 5); ret += 5; outlen -= 5; }
    if(file->ctx->sb.s_attridx_fid && ino->i_attr[0]) {
        axfs_unpack(ino->i_attr, &ext);
        ea = (axfs_eahdr_t*)file->ctx->blk[0];
        if(ext.e_blk && axfs_load_blk(file->ctx, ext.e_blk, 0) && !memcmp(ea->a_magic, AXFS_EA_MAGIC, 4) && ea->a_self_fid == file->fid &&
          (file->ctx->sb.s_flags & AXFS_SB_F_NOCRC || ea->a_chksum == axfs_crc32(0, file->ctx->blk[0] + 8, ea->a_size - 8)))
            for(e = file->ctx->blk[0] + ea->a_size, d = file->ctx->blk[0] + sizeof(axfs_eahdr_t); d < e && *d; ) {
                d = axfs_unpack(d, &ext); memset(tmp, 0, sizeof(tmp));
                if(ext.e_blk >= sizeof(axfs_eahdr_t) && axfs_read_inode(file->ctx, (axfs_inode_t*)file->ctx->ino[4], ext.e_blk,
                  sizeof(tmp), tmp) && tmp[0]) {
                    for(l = 0; l < sizeof(tmp) && tmp[l]; l++);
                    if(outlen >= l + 1) { memcpy(outval + ret, tmp, l); outval[ret + l] = 0; ret += l + 1; outlen -= l + 1; }
                }
            }
    }
    return ret;
}

#endif /* AXFS_IMPLEMENTATION */
