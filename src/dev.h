/*
 * src/dev.h
 * https://gitlab.com/bztsrc/alexandriafs
 *
 * Copyright (C) 2023 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Common low-level device handling functions
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <sys/stat.h>
#ifndef PATH_MAX
#define PATH_MAX 4096
#endif
#ifdef __WIN32__
#include <windows.h>
HANDLE dev_f = 0;
WCHAR szFile[PATH_MAX];
#else
char *realpath(char*, char*);
int fdatasync(int);
int dev_f;
#endif
struct stat dev_st;
uint64_t dev_offs, dev_size, dev_now_msec;
int64_t read_size;
uint8_t dev_uuid[16];
uint32_t dev_part = 0;
uint8_t dev_gpt[512];
int verbose = 0;
int dev_read(void *dev, uint64_t off, void *buf, uint32_t size);
void dev_close(void);

typedef struct {
  uint64_t  Signature;
  uint32_t  Revision;
  uint32_t  HeaderSize;
  uint32_t  CRC32;
  uint32_t  Reserved;
  uint64_t  MyLBA;
  uint64_t  AlternateLBA;
  uint64_t  FirstUsableLBA;
  uint64_t  LastUsableLBA;
  uint8_t   DiskGUID[16];
  uint64_t  PartitionEntryLBA;
  uint32_t  NumberOfPartitionEntries;
  uint32_t  SizeOfPartitionEntry;
  uint32_t  PartitionEntryArrayCRC32;
} __attribute__((packed)) gpt_header_t;

typedef struct {
  uint8_t   PartitionTypeGUID[16];
  uint8_t   UniquePartitionGUID[16];
  uint64_t  StartingLBA;
  uint64_t  EndingLBA;
  uint64_t  Attributes;
  uint16_t  PartitionName[36];
} __attribute__((packed)) gpt_entry_t;

/**
 * Read a file entirely into memory
 */
uint8_t* readfileall(char *file)
{
#ifdef __WIN32__
    HANDLE f;
    DWORD r, t;
    int i;
#else
    FILE *f;
#endif
    uint8_t *data = NULL;

    read_size = 0;
    if(!file || !*file) return NULL;
    data = (unsigned char*)strrchr(file, '/');
    if(data && !*(data + 1)) return NULL;
    data = NULL;
#ifdef __WIN32__
    memset(&szFile, 0, sizeof(szFile));
    MultiByteToWideChar(CP_UTF8, 0, file, -1, szFile, PATH_MAX);
    for(i = 0; szFile[i]; i++) if(szFile[i] == L'/') szFile[i] = L'\\';
    f = CreateFileW(szFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
    if(f != INVALID_HANDLE_VALUE) {
        r = GetFileSize(f, NULL);
        read_size = (int64_t)r;
        data = (uint8_t*)malloc(read_size + 1);
        if(!data) { fprintf(stderr, "%s: unable to allocate memory\r\n", tool); exit(1); }
        memset(data, 0, read_size + 1);
        if(!ReadFile(f, data, r, &t, NULL)) t = 0;
        read_size = (int64_t)t;
        CloseHandle(f);
    }
#else
    f = fopen(file, "rb");
    if(f) {
        fseek(f, 0L, SEEK_END);
        read_size = (int64_t)ftell(f);
        fseek(f, 0L, SEEK_SET);
        data = (uint8_t*)malloc(read_size + 1);
        if(!data) { fprintf(stderr, "%s: unable to allocate memory\r\n", tool); exit(1); }
        memset(data, 0, read_size + 1);
        read_size = (int64_t)fread(data, 1, read_size, f);
        fclose(f);
    }
#endif
    if(!read_size && data) { free(data); data = NULL; }
    return data;
}

/**
 * Open a device
 */
int dev_open(char *file)
{
    time_t t;
#ifdef __WIN32__
    DWORD r;
    int i;

    memset(&szFile, 0, sizeof(szFile));
    MultiByteToWideChar(CP_UTF8, 0, file, -1, szFile, PATH_MAX);
    for(i = 0; szFile[i]; i++) if(szFile[i] == L'/') szFile[i] = L'\\';
    dev_f = CreateFileW(szFile, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_READ, NULL, OPEN_EXISTING,
        FILE_FLAG_NO_BUFFERING, NULL);
    if(dev_f == INVALID_HANDLE_VALUE) { dev_f = NULL; return 0; }
    r = GetFileSize(dev_f, NULL);
    dev_size = (uint64_t)r;
#else
    char *full, *mnt, *s;
    int l;

    memset(&dev_st, 0, sizeof(dev_st));

    /* resolve relative paths and symlinks first */
    if(!(full = realpath(file, NULL))) full = file;
    /* check if the file is mounted by any chance */
    mnt = (char*)readfileall(
#ifdef __linux__
        "/proc/self/mountinfo"
#else
        "/etc/mtab"
#endif
        );
    if(mnt) {
        for(s = mnt, l = strlen(full); *s; s++) {
            /* find beginning of a line */
            while(*s && (*s == '\r' || *s == '\n' || *s == ' ' || *s == '\t')) s++;
            if(!memcmp(s, full, l)) {
                fprintf(stderr, "%s: '%s' is mounted, please umount it first\r\n", tool, file);
                free(mnt); if(full != file) free(full);
                return 0;
            }
            /* skip to the end of the line */
            while(*s && *s != '\r' && *s != '\n') s++;
        }
        free(mnt);
    }
    stat(full, &dev_st);
    dev_f = open(full, O_RDWR | (S_ISBLK(dev_st.st_mode) ? O_SYNC | O_EXCL : 0));
    if(full != file) free(full);
    if(dev_f < 3) { dev_f = 0; return 0; }
    dev_size = dev_st.st_size;
#endif
    dev_offs = 0;
    /* if partition requested, look for GPT and update dev_offs and dev_size with the partition's */
    memset(dev_gpt, 0, sizeof(dev_gpt));
    if(!dev_read(NULL, 512, dev_gpt, sizeof(dev_gpt))) { dev_close(); return 0; }
    if(dev_part) {
        if(!memcmp(dev_gpt, "EFI PART", 8)) {
            if(dev_part - 1 >= ((gpt_header_t*)(dev_gpt))->NumberOfPartitionEntries) {
                fprintf(stderr, "%s: no such partition (max %u)\r\n", tool, ((gpt_header_t*)(dev_gpt))->NumberOfPartitionEntries + 1);
                dev_close(); return 0;
            }
            if(!dev_read(NULL, ((gpt_header_t*)(dev_gpt))->SizeOfPartitionEntry * (dev_part - 1) +
              ((gpt_header_t*)(dev_gpt))->PartitionEntryLBA * 512, dev_gpt, sizeof(dev_gpt))) {
                fprintf(stderr, "%s: unable to read partition entry! Corrupted dev_gpt?\r\n", tool);
                dev_close(); return 0;
            }
            dev_offs = ((gpt_entry_t*)(dev_gpt))->StartingLBA * 512;
            dev_size = (((gpt_entry_t*)(dev_gpt))->EndingLBA - ((gpt_entry_t*)(dev_gpt))->StartingLBA + 1) * 512;
            memcpy(dev_uuid, &((gpt_entry_t*)(dev_gpt))->UniquePartitionGUID, 16);
        } else {
            fprintf(stderr, "%s: partitioning table not found\r\n", tool);
            dev_close(); return 0;
        }
    }
    t = time(NULL); dev_now_msec = (uint64_t)t * 1000000;
    return 1;
}

/**
 * Read from the device
 */
int dev_read(void *dev, uint64_t off, void *buf, uint32_t size)
{
#ifdef __WIN32__
    DWORD r = size, t;
    LARGE_INTEGER pos;
    pos.QuadPart = off + dev_offs;
    SetFilePointerEx(dev_f, pos, NULL, FILE_BEGIN);
    memset(buf, 0, size);
    if(!ReadFile(dev_f, buf, r, &t, NULL) || r != t) return 0;
#else
    memset(buf, 0, size);
    if(lseek(dev_f, (off_t)off + dev_offs, SEEK_SET) != (off_t)off || read(dev_f, buf, (ssize_t)size) != (ssize_t)size) return 0;
#endif
    (void)dev;
    return 1;
}

/**
 * Write to the device
 */
int dev_write(void *dev, uint64_t off, void *buf, uint32_t size)
{
#ifdef __WIN32__
    DWORD r = size, t;
    LARGE_INTEGER pos;
    pos.QuadPart = off + dev_offs;
#endif
    if(verbose > 2) printf("dev_write offset %08lx %d bytes\n", (long unsigned int)off, size);
#ifdef __WIN32__
    SetFilePointerEx(dev_f, pos, NULL, FILE_BEGIN);
    if(!WriteFile(dev_f, buf, r, &t, NULL) || r != t) return 0;
#else
    if(lseek(dev_f, (off_t)off + dev_offs, SEEK_SET) != (off_t)off || write(dev_f, buf, (ssize_t)size) != (ssize_t)size) return 0;
#endif
    (void)dev;
    return 1;
}

/**
 * Close a device
 */
void dev_close(void)
{
    if(dev_f) {
#ifdef __WIN32__
        CloseHandle(dev_f);
#else
        if(S_ISBLK(dev_st.st_mode)) fdatasync(dev_f);
        close(dev_f);
#endif
        dev_f = 0;
    }
}
