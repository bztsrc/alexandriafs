/*
 * src/fuse.c
 * https://gitlab.com/bztsrc/alexandriafs
 *
 * Copyright (C) 2023 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief FUSE driver for AleXandria File System
 */

char *tool = "fuse.axfs";
#include "dev.h"

#define AXFS_IMPLEMENTATION
#include <libaxfs.h>

#define _FILE_OFFSET_BITS 64    /* needed by FUSE */
#include <errno.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#define __USE_GNU
#include <sys/statvfs.h>

#define FUSE_USE_VERSION    29
#include <fuse.h>

axfs_ctx_t ctx;

int fillattr(axfs_inode_t *ino, struct stat *st)
{
    st->st_size = ino->i_size;
    st->st_nlink = ino->i_nlinks;
    st->st_mtime = st->st_atime = ino->i_mtime/1000000;
    st->st_mtimensec = st->st_atimensec = (ino->i_mtime%1000000)*1000;
    st->st_ctime = ino->i_ctime/1000000;
    st->st_ctimensec = (ino->i_ctime%1000000)*1000;
    st->st_mode =
        (ino->i_owner.access & AXFS_READ  ? S_IRUSR : 0) |
        (ino->i_owner.access & AXFS_WRITE ? S_IWUSR : 0) |
        (ino->i_owner.access & AXFS_EXEC  ? S_IXUSR : 0) |
        (ino->i_acl[0].access & AXFS_READ  ? S_IRGRP : 0) |
        (ino->i_acl[0].access & AXFS_WRITE ? S_IWGRP : 0) |
        (ino->i_acl[0].access & AXFS_EXEC  ? S_IXGRP : 0) |
        (ino->i_acl[1].access & AXFS_READ  ? S_IROTH : 0) |
        (ino->i_acl[1].access & AXFS_WRITE ? S_IWOTH : 0) |
        (ino->i_acl[1].access & AXFS_EXEC  ? S_IXOTH : 0);
    if(ino->i_type <= AXFS_FT_UNI) st->st_mode |= __S_IFDIR; else
    if(ino->i_type == AXFS_FT_LNK) st->st_mode |= __S_IFLNK; else
    if(ino->i_type == AXFS_FT_PIPE) st->st_mode |= __S_IFIFO; else
    if(ino->i_type == AXFS_FT_SOCK) st->st_mode |= __S_IFSOCK; else
    if(ino->i_type >= AXFS_FT_BOOT) st->st_mode |= __S_IFREG;
    st->st_uid = getuid();
    st->st_gid = getgid();
    st->st_blocks = ino->i_nblks << ctx.sb.s_blksize; /* must be in 512 byte units */
    return 0;
}

int fuse_getattr(const char *path, struct stat *st)
{
    axfs_inode_t *ino;
    uint64_t fid = axfs_lookup_fid(&ctx, path);
    /* yeah... libc handles null args for %s just fine, but here's this gcc bug throwing warnings and errors... */
    if(verbose) printf("fuse_getattr %s\r\n", path ? path : "(null)");
    if(!fid || !(ino = axfs_get_inode(&ctx, fid, 0))) return -ENOENT;
    st->st_ino = fid;
    return fillattr(ino, st);
}

int fuse_readlink(const char *path, char *outbuf, size_t outlen)
{
    if(verbose) printf("fuse_readlink %s %lx %lu\r\n", path ? path : "(null)", (uintptr_t)outbuf, outlen);
    return axfs_readlink(&ctx, path, outbuf, outlen) ? 0 : -ENOENT;
}

int fuse_mknod(const char *path, mode_t mode, dev_t dev)
{
    if(verbose) printf("fuse_mknod %s %x %lx\r\n", path ? path : "(null)", mode, dev);
    return axfs_mknod(&ctx, path, mode, major(dev), minor(dev)) ? 0 : -EEXIST;
}

int fuse_mkdir(const char *path, mode_t mode)
{
    if(verbose) printf("fuse_mkdir %s %x\r\n", path ? path : "(null)", mode);
    return axfs_mkdir(&ctx, path) ? 0 : -EEXIST;
}

int fuse_unlink(const char *path)
{
    char *s = NULL;
    uint64_t fid;

    if(verbose) printf("fuse_unlink %s\r\n", path ? path : "(null)");
    return axfs_basename(&ctx, path, &fid, &s) && axfs_unlink(&ctx, fid, s) ? 0 : -ENOENT;
}

int fuse_rmdir(const char *path)
{
    if(verbose) printf("fuse_rmdir %s\r\n", path ? path : "(null)");
    return axfs_remove(&ctx, path) ? 0 : -ENOENT;
}

int fuse_symlink(const char *path, const char *target)
{
    if(verbose) printf("fuse_symlink %s %s\r\n", path ? path : "(null)", target ? target : "(null)");
    return axfs_symlink(&ctx, path, target) ? 0 : -EEXIST;
}

int fuse_rename(const char *path, const char *newpath)
{
    uint64_t fid1, fid2, fid3;
    char *s1 = NULL, *s2 = NULL;

    if(verbose) printf("fuse_rename %s %s\r\n", path ? path : "(null)", newpath ? newpath : "(null)");
    return (!axfs_basename(&ctx, path, &fid1, &s1) || !axfs_basename(&ctx, newpath, &fid2, &s2) ||
     !(fid3 = axfs_lookupat_fid(&ctx, fid1, s1, strlen(s1))) ||
     !axfs_link(&ctx, fid2, fid3, s2) || !axfs_unlink(&ctx, fid1, s1)) ? -ENOENT : 0;
}

int fuse_link(const char *path, const char *newpath)
{
    uint64_t fid1, fid2;
    char *s = NULL;

    if(verbose) printf("fuse_link %s %s\r\n", path ? path : "(null)", newpath ? newpath : "(null)");
    return (!(fid1 = axfs_lookup_fid(&ctx, path)) ||
      !axfs_basename(&ctx, newpath, &fid2, &s) || !axfs_link(&ctx, fid2, fid1, s)) ? -ENOENT : 0;
}

int fuse_chmod(const char *path, mode_t mode)
{
    axfs_inode_t *ino;
    uint64_t fid = axfs_lookup_fid(&ctx, path);

    if(verbose) printf("fuse_chmod %s %x\r\n", path ? path : "(null)", mode);
    if(!fid || !(ino = axfs_get_inode(&ctx, fid, 0))) return -ENOENT;
    ino->i_owner.access =  (mode & S_IRUSR ? AXFS_READ : 0) | (mode & S_IWUSR ? AXFS_WRITE : 0) | (mode & S_IXUSR ? AXFS_EXEC : 0);
    ino->i_acl[0].access = (mode & S_IRGRP ? AXFS_READ : 0) | (mode & S_IWGRP ? AXFS_WRITE : 0) | (mode & S_IXGRP ? AXFS_EXEC : 0);
    ino->i_acl[1].access = (mode & S_IROTH ? AXFS_READ : 0) | (mode & S_IWOTH ? AXFS_WRITE : 0) | (mode & S_IXOTH ? AXFS_EXEC : 0);
    memset(&ino->i_acl[1], 0xff, 15);
    return axfs_put_inode(&ctx, ino, fid, 0) && axfs_commit(&ctx) ? 0 : -EPERM;
}

int fuse_chown(const char *path, uid_t uid, gid_t gid)
{
    axfs_inode_t *ino;
    uint64_t fid = axfs_lookup_fid(&ctx, path);

    if(verbose) printf("fuse_chown %s %u %u\r\n", path ? path : "(null)", uid, gid);
    if(!fid || !(ino = axfs_get_inode(&ctx, fid, 0))) return -ENOENT;
    memset(&ino->i_owner, 0, 15); ino->i_owner.Data1 = uid;
    memset(&ino->i_acl[0], 0, 15); ino->i_acl[0].Data1 = gid;
    memset(&ino->i_acl[1], 0xff, 15);
    return axfs_put_inode(&ctx, ino, fid, 0) && axfs_commit(&ctx) ? 0 : -EPERM;
}

int fuse_truncate(const char *path, off_t off)
{
    axfs_file_t file;
    int ret;

    if(verbose) printf("fuse_truncate %s %lx\r\n", path ? path : "(null)", off);
    ret = axfs_open(&file, &ctx, path, 0) && axfs_truncate(&file, off);
    axfs_close(&file);
    return ret ? 0 : -ENOENT;
}

int fuse_open(const char *path, struct fuse_file_info *info)
{
    axfs_file_t *file;

    if(!(file = (axfs_file_t*)malloc(sizeof(axfs_file_t)))) return -ENOMEM;
    memset(file, 0, sizeof(axfs_file_t));
    info->fh = (uint64_t)(uintptr_t)file;
    if(verbose) printf("fuse_open %s -> %lx\r\n", path ? path : "(null)", info->fh);
    return axfs_open(file, &ctx, path, 0);
}

int fuse_create(const char *path, mode_t mode, struct fuse_file_info *info)
{
    axfs_file_t *file;
    axfs_inode_t *ino;

    if(!(file = (axfs_file_t*)malloc(sizeof(axfs_file_t)))) return -ENOMEM;
    memset(file, 0, sizeof(axfs_file_t));
    info->fh = (uint64_t)(uintptr_t)file;
    if(verbose) printf("fuse_create %s %x -> %lx\r\n", path ? path : "(null)", mode, info->fh);
    if(axfs_open(file, &ctx, path, 1)) {
        ino = (axfs_inode_t*)file->ino;
        ino->i_owner.access =  (mode & S_IRUSR ? AXFS_READ : 0) | (mode & S_IWUSR ? AXFS_WRITE : 0) | (mode & S_IXUSR ? AXFS_EXEC : 0);
        ino->i_acl[0].access = (mode & S_IRGRP ? AXFS_READ : 0) | (mode & S_IWGRP ? AXFS_WRITE : 0) | (mode & S_IXGRP ? AXFS_EXEC : 0);
        ino->i_acl[1].access = (mode & S_IROTH ? AXFS_READ : 0) | (mode & S_IWOTH ? AXFS_WRITE : 0) | (mode & S_IXOTH ? AXFS_EXEC : 0);
        memset(&ino->i_acl[1], 0xff, 15);
        if(S_ISDIR(mode)) { ino->i_type = AXFS_FT_DIR; ino->i_mime = 0; }
        if(S_ISLNK(mode)) { ino->i_type = AXFS_FT_LNK; ino->i_mime = 0; }
        if(S_ISFIFO(mode)) { ino->i_type = AXFS_FT_PIPE; ino->i_mime = 0; }
        if(__S_ISTYPE(mode, __S_IFSOCK)) { ino->i_type = AXFS_FT_SOCK; ino->i_mime = 0; }
        return axfs_put_inode(&ctx, ino, file->fid, 0) && axfs_commit(&ctx) ? 0 : -EPERM;
    }
    return -EBADF;
}

int fuse_read(const char *path, char *buf, size_t size, off_t offs, struct fuse_file_info *info)
{
    axfs_file_t *file = (axfs_file_t*)(uintptr_t)info->fh;

    (void)path;
    if(verbose > 1) printf("fuse_read %lx %lx:%lu %lx\r\n", info->fh, offs, size, (uintptr_t)buf);
    if(!file) return -EBADF;
    file->offs = offs;
    return (int)axfs_read(file, (void*)buf, size);
}

int fuse_write(const char *path, const char *buf, size_t size, off_t offs, struct fuse_file_info *info)
{
    axfs_file_t *file = (axfs_file_t*)(uintptr_t)info->fh;

    (void)path;
    if(verbose > 1) printf("fuse_write %lx %lx:%lu %lx\r\n", info->fh, offs, size, (uintptr_t)buf);
    if(!file) return -EBADF;
    file->offs = offs;
    return (int)axfs_write(file, (void*)buf, size);
}

int fuse_statfs(const char *path, struct statvfs *vfs)
{
    (void)path;
    memset(vfs, 0, sizeof(struct statvfs));
    vfs->f_bsize = vfs->f_frsize = ctx.blksiz;
    vfs->f_blocks = ctx.sb.s_nblks;
    vfs->f_bfree = vfs->f_bavail = ((axfs_inode_t*)ctx.ino[2])->i_size / ctx.blksiz;
    vfs->f_files = ((axfs_inode_v_t*)ctx.ino[0])->i_ninode;
    vfs->f_namemax = AXFS_FILENAME_MAX;
    vfs->f_flag = ST_NOATIME | ST_NODIRATIME | ST_NODEV | ST_NOSUID;
    memcpy(&vfs->f_fsid, AXFS_SB_MAGIC, 4);
    if(verbose) printf("fuse_statfs free blocks %lu, %lu%% used\r\n", vfs->f_bfree, 100 - vfs->f_bfree * 100 / vfs->f_blocks);
    return 0;
}

int fuse_flush(const char *path, struct fuse_file_info *info)
{
    (void)path;
    if(verbose > 1) printf("fuse_flush %lx\r\n", info->fh);
    axfs_commit(&ctx);
    return 0;
}

int fuse_close(const char *path, struct fuse_file_info *info)
{
    axfs_file_t *file = (axfs_file_t*)(uintptr_t)info->fh;

    (void)path;
    if(verbose) printf("fuse_close %lx\r\n", info->fh);
    if(!file) return -EBADF;
    axfs_close(file);
    free(file);
    return 0;
}

int fuse_fsync(const char *path, int datasync, struct fuse_file_info *info)
{
    if(verbose > 1) printf("fuse_fsync %s %d %lx\r\n", path ? path : "(null)", datasync, info->fh);
    axfs_commit(&ctx);
    return 0;
}

int fuse_setxattr(const char *path, const char *key, const char *value, size_t len, int flags)
{
    axfs_file_t file = { 0 };
    int ret;

    (void)len; (void)flags;
    if(verbose) printf("fuse_setxattr %s %s %s\r\n", path ? path : "(null)", key, value);
    ret = axfs_open(&file, &ctx, path, 0) && axfs_setxattr(&file, key, value);
    axfs_close(&file);
    return ret ? 0 : -ENOENT;
}

int fuse_getxattr(const char *path, const char *key, char *value, size_t len)
{
    axfs_file_t file = { 0 };
    int ret;

    if(verbose) printf("fuse_getxattr %s %s\r\n", path ? path : "(null)", key);
    ret = axfs_open(&file, &ctx, path, 0) && axfs_getxattr(&file, key, value, len);
    axfs_close(&file);
    return ret ? 0 : -ENOENT;
}

int fuse_listxattr(const char *path, char *buf, size_t len)
{
    axfs_file_t file = { 0 };
    int ret;

    if(verbose) printf("fuse_listxattr %s\r\n", path ? path : "(null)");
    ret = axfs_open(&file, &ctx, path, 0) && axfs_listxattr(&file, buf, len);
    axfs_close(&file);
    return ret ? 0 : -ENOENT;
}

int fuse_removexattr(const char *path, const char *key)
{
    axfs_file_t file = { 0 };
    int ret;

    if(verbose) printf("fuse_removexattr %s %s\r\n", path ? path : "(null)", key);
    ret = axfs_open(&file, &ctx, path, 0) && axfs_setxattr(&file, key, NULL);
    axfs_close(&file);
    return ret ? 0 : -ENOENT;
}

int fuse_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offs, struct fuse_file_info *info)
{
    axfs_file_t *file = (axfs_file_t*)(uintptr_t)info->fh;
    axfs_inode_t *ino;
    axfs_dirent_t ent;
    struct stat st;
    int ret = 0;

    (void)path;
    if(verbose > 1) printf("fuse_readdir %lx %lx\r\n", info->fh, offs);
    if(!file) return -EBADF;
    file->offs = offs;
    if(!offs) {
        filler(buf, ".", NULL, 0);
        filler(buf, "..", NULL, 0);
    }
    if(axfs_readdir(file, &ent)) {
        if((ino = axfs_get_inode(file->ctx, ent.d_fid, 0))) {
            fillattr(ino, &st);
            ret = filler(buf, ent.d_name, &st, file->offs);
        }
    }
    return ret;
}

void *fuse_init(struct fuse_conn_info *conn)
{
    if(verbose) {
        printf("fuse_init\r\n");
        printf("  max_write %u\r\n", conn->max_write);
        printf("  block size %u\r\n", ctx.blksiz);
        printf("  inode size %u\r\n", ctx.inosiz);
        printf("  numblocks %lu\r\n", ctx.sb.s_nblks);
    }
    return conn;
}

void fuse_destr(void *private_data)
{
    (void)private_data;
    if(verbose) printf("\rfuse_destroy\r\n");
    axfs_umount(&ctx);
    dev_close();
}

int fuse_fgetattr(const char *path, struct stat *st, struct fuse_file_info *info)
{
    axfs_file_t *file = (axfs_file_t*)(uintptr_t)info->fh;

    (void)path;
    if(verbose > 1) printf("fuse_fgetattr %lx\r\n", info->fh);
    if(!file) return -EBADF;
    st->st_ino = file->fid;
    return fillattr((axfs_inode_t*)file->ino, st);
}

int fuse_utimens(const char *path, const struct timespec tv[2])
{
    axfs_inode_t *ino;
    uint64_t fid = axfs_lookup_fid(&ctx, path);

    if(verbose) printf("fuse_utimens %s %lu.%lu\r\n", path ? path : "(null)", tv[1].tv_sec, tv[1].tv_nsec);
    if(!fid || !(ino = axfs_get_inode(&ctx, fid, 0))) return -ENOENT;
    ino->i_mtime = tv[1].tv_sec * 1000000 + tv[1].tv_nsec / 1000;
    return axfs_put_inode(&ctx, ino, fid, 0) && axfs_commit(&ctx) ? 0 : -EPERM;
}

static struct fuse_operations operations = {
.getattr = fuse_getattr,
.readlink = fuse_readlink,
.mknod = fuse_mknod,
.mkdir = fuse_mkdir,
.unlink = fuse_unlink,
.rmdir = fuse_rmdir,
.symlink = fuse_symlink,
.rename = fuse_rename,
.link = fuse_link,
.chmod = fuse_chmod,
.chown = fuse_chown,
.truncate = fuse_truncate,
.open = fuse_open,
.read = fuse_read,
.write = fuse_write,
.statfs = fuse_statfs,
.flush = fuse_flush,
.release = fuse_close,
.fsync = fuse_fsync,
.setxattr = fuse_setxattr,
.getxattr = fuse_getxattr,
.listxattr = fuse_listxattr,
.removexattr = fuse_removexattr,
.opendir = fuse_open,
.readdir = fuse_readdir,
.releasedir = fuse_close,
.fsyncdir = fuse_fsync,
.init = fuse_init,
.destroy = fuse_destr,
.create = fuse_create,
.fgetattr = fuse_fgetattr,
.utimens = fuse_utimens,
.flag_nullpath_ok = 1,
.flag_nopath = 1
};

void usage(void)
{
    printf("AleXandriaFS FUSE %d.%d driver Copyright (c) 2023 bzt, GPLv3+\r\nhttps://gitlab.com/bztsrc/alexandriafs\r\n\r\n",
        FUSE_MAJOR_VERSION, FUSE_MINOR_VERSION);
    printf("  %s [-e <pass>] [-p <num>] <image|device> <mount point> [options]\r\n\r\n", tool);
    printf("    -e <pass>              encryption passphrase\r\n");
    printf("    -p <num>               select a GPT partition (starts at 1)\r\n");
    printf("    -f                     run in the foreground\r\n");
    printf("    -h                     help, print all FUSE options\r\n\r\n");
}

/**
 * Main function
 */
int main(int argc, char **argv)
{
    char *arg[16] = { 0 }, *fn = NULL, *enc = NULL;
    int i, ac = 1;

    /* parse arguments */
    if(argc < 2 || argc > 15) {
        usage();
        printf("You should have received a copy of the GNU General Public License v3\r\n"
            "along with this program. If not, see <https://www.gnu.org/licenses/>.\r\n\r\n");
        return 1;
    }
    arg[0] = argv[0];
    for(i = 1; i < argc && argv[i] && ac < 15; i++)
        if(argv[i][0] == '-') {
            switch(argv[i][1]) {
                case 'e': enc = argv[++i]; break;
                case 'p': dev_part = atoi(argv[++i]); break;
                case 'd': case 'f': verbose++; arg[ac++] = argv[i]; break;
                case 'h': break;
                default: arg[ac++] = argv[i]; break;
            }
        } else
        if(!fn) fn = argv[i];
        else arg[ac++] = argv[i];
    if(fn) {
        /* open device */
        if(!dev_open(fn)) { fprintf(stderr, "%s: unable to open '%s'\r\n", tool, fn); return 2; }
        if(!dev_read(NULL, 0, &ctx.sb, sizeof(ctx.sb))) { fprintf(stderr, "%s: unable to read superblock\r\n", tool); dev_close(); return 2; }
        if(!memcmp(&ctx.sb, "\x1f\x8b\x08", 3)) { fprintf(stderr, "%s: gzip compressed image!\r\n", tool); dev_close(); return 2; }
        if(memcmp(&ctx.sb.s_magic, AXFS_SB_MAGIC, 4) || memcmp(&ctx.sb.s_magic2, AXFS_SB_MAGIC, 4) ||
          ctx.sb.s_blksize > 11 || ctx.sb.s_inoshift > 12 || !ctx.sb.s_volume_fid || !ctx.sb.s_root_fid || !ctx.sb.s_freeblk_fid)
          { fprintf(stderr, "%s: not a valid AleXandria File System\r\n", tool); dev_close(); return 2; }
        if(!(ctx.sb.s_flags & AXFS_SB_F_NOCRC) && ctx.sb.s_chksum != axfs_crc32(0, ctx.sb.s_magic, 0xF4))
          fprintf(stderr, "%s: invalid checksum in AXFS superblock\r\n", tool);
        if(ctx.sb.s_enchash && (!enc || !*enc || ctx.sb.s_enchash != axfs_crc32(0, (uint8_t*)enc, strlen(enc))))
          { fprintf(stderr, "%s: bad encryption passphrase\r\n", tool); dev_close(); return 2; }
        /* this should never fail (except on malloc and bad passphrase), because we did all the checks above,
         * providing detailed error messages. We still want to do this, because it sets up internal context */
        if(!axfs_mount(&ctx, NULL, enc)) { fprintf(stderr, "%s: unable to mount\r\n", tool); dev_close(); return 1; }
    } else {
        usage();
        arg[ac++] = "-ho";
    }

    /* let FUSE do the rest */
    return fuse_main(ac, arg, &operations, &ctx);
}
