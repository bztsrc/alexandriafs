/*
 * src/mkfs.c
 * https://gitlab.com/bztsrc/alexandriafs
 *
 * Copyright (C) 2023 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Format with AleXandria File System
 */

char *tool = "mkfs.axfs";
#include "dev.h"

#define AXFS_IMPLEMENTATION
#include <libaxfs.h>

#include <dirent.h>

axfs_ctx_t ctx;
axfs_file_t file;
char full[PATH_MAX];
int skipbytes = 0;

typedef struct {
  uint32_t  Data1;
  uint16_t  Data2;
  uint16_t  Data3;
  uint8_t   Data4[8];
} __attribute__((packed)) guid_t;

/**
 * Convert hex string into integer
 */
uint64_t gethex(char *ptr, int len)
{
    uint64_t ret = 0;
    for(;len--;ptr++) {
        if(*ptr>='0' && *ptr<='9') {          ret <<= 4; ret += (unsigned int)(*ptr-'0'); }
        else if(*ptr >= 'a' && *ptr <= 'f') { ret <<= 4; ret += (unsigned int)(*ptr-'a'+10); }
        else if(*ptr >= 'A' && *ptr <= 'F') { ret <<= 4; ret += (unsigned int)(*ptr-'A'+10); }
        else break;
    }
    return ret;
}

/**
 * Parse a GUID in string into binary representation
 */
void getguid(char *ptr, guid_t *guid)
{
    int i;

    if(!ptr || !*ptr || ptr[8] != '-' || ptr[13] != '-' || ptr[18] != '-') return;
    memset(guid, 0, sizeof(guid_t));
    guid->Data1 = gethex(ptr, 8); ptr += 9;
    guid->Data2 = gethex(ptr, 4); ptr += 5;
    guid->Data3 = gethex(ptr, 4); ptr += 5;
    guid->Data4[0] = gethex(ptr, 2); ptr += 2;
    guid->Data4[1] = gethex(ptr, 2); ptr += 2; if(*ptr == '-') ptr++;
    for(i = 2; i < 8; i++, ptr += 2) guid->Data4[i] = gethex(ptr, 2);
}

/**
 * Print status
 */
void status(char *msg, char *par)
{
#ifndef __WIN32__
#define CL "s\x1b[K"
#else
#define CL "-70s"
#endif
    if(!par) printf("\r%" CL "\r", msg);
    else printf("\r%s: %" CL "\r", msg, par);
    fflush(stdout);
}

/**
 * Recursively parse a directory
 */
int parsedir(char *directory, int parent, uint64_t to)
{
#ifdef __WIN32__
    WIN32_FIND_DATAW ffd;
    HANDLE h;
    int i, j;
#else
    DIR *dir;
    struct dirent *ent;
    struct stat st;
#endif
    int r = 1;
    uint64_t fid = to;
    unsigned char *tmp;

    if(!parent) { parent = strlen(directory); skipbytes = parent + 1; strncpy(full, directory, sizeof(full)-1); }
#ifdef __WIN32__
    memset(&szFile, 0, sizeof(szFile));
    MultiByteToWideChar(CP_UTF8, 0, directory, -1, szFile, PATH_MAX);
    for(i = 0; szFile[i]; i++) if(szFile[i] == L'/') szFile[i] = L'\\';
    if(i && szFile[i - 1] != L'\\') szFile[i++] = L'\\';
    wcscpy_s(szFile + i, 255, L"*.*");
    h = FindFirstFileW(szFile, &ffd);
    if(h != INVALID_HANDLE_VALUE) {
        do {
            if(!wcscmp(ffd.cFileName, L".") || !wcscmp(ffd.cFileName, L"..")) continue;
            wcscpy_s(szFile + i, 255, ffd.cFileName);
            memset(full, 0, sizeof(full)); parent = 0;
            WideCharToMultiByte(CP_UTF8, 0, szFile, -1, full, sizeof(full) - 1, NULL, NULL);
            for(j = 0; full[j]; j++) if(full[j] == '\\') { full[j] = '/'; parent = j; }
            read_size = 0;
            status("Adding", full + skipbytes);
            if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
                fid = axfs_mkdirat(&ctx, to, full + parent + 1);
                r = parsedir(full, strlen(full), fid);
            } else {
                tmp = readfileall(full);
                if(axfs_openat(&file, &ctx, to, full + parent + 1, 1)) {
                    axfs_write(&file, tmp, read_size);
                    axfs_close(&file);
                }
                if(tmp) free(tmp);
            }
        } while(r && FindNextFileW(h, &ffd) != 0);
        FindClose(h);
    }
#else
    if((dir = opendir(directory)) != NULL) {
        while(r && (ent = readdir(dir)) != NULL) {
            if(!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, "..")) continue;
            strncpy(full + parent, "/", sizeof(full)-parent-1);
            strncat(full + parent, ent->d_name, sizeof(full)-parent-1);
            if(stat(full, &st)) continue;
            read_size = 0;
            status("Adding", full + skipbytes);
            if(S_ISDIR(st.st_mode)) {
                fid = axfs_mkdirat(&ctx, to, full + parent + 1);
                r = parsedir(full, strlen(full), fid);
            } else
            if(S_ISREG(st.st_mode)) {
                tmp = readfileall(full);
                if(axfs_openat(&file, &ctx, to, full + parent + 1, 1)) {
                    axfs_write(&file, tmp, read_size);
                    axfs_close(&file);
                }
                if(tmp) free(tmp);
            }
        }
        closedir(dir);
    }
#endif
    if(parent + 1 == skipbytes) skipbytes = 0;
    return r;
}

/**
 * Main function
 */
int main(int argc, char **argv)
{
    axfs_access_t *uuid = (axfs_access_t*)&dev_uuid;
    int i, j, full = 0, bs = 4096, is = 512, nres = 0, jblks = -1, wrprot = 0;
    int wmin = 128, nbad = 0, blksiz, inosh;
    char *fn = NULL, *dir = NULL, *enc = NULL, *c, line[256];
    uint64_t size = 0, b, *bad = NULL;
    uint8_t *blk1, *blk2, *blk3;

    /* generate a random file system UUID */
    srand(time(NULL));
    i = rand(); memcpy(&dev_uuid[0], &i, 4); i = rand(); memcpy(&dev_uuid[4], &i, 4);
    i = rand(); memcpy(&dev_uuid[8], &i, 4); i = rand(); memcpy(&dev_uuid[12], &i, 4);

    /* parse command line */
    printf("AleXandriaFS format Copyright (c) 2023 bzt, GPLv3+\r\n");
    if(argc < 2) {
usage:  printf("https://gitlab.com/bztsrc/alexandriafs\r\n\r\n");
        printf("  %s [options] <image|device>\r\n\r\n", tool);
        printf("  -v            be verbose\r\n");
        printf("  -f            full format (detect bad blocks, very slow!)\r\n");
        printf("  -c <size>     create image of size (only image files, suffix K,M,G)\r\n");
        printf("  -b <size>     set block size in bytes (default 4096)\r\n");
        printf("  -i <size>     set inode size in bytes (default 512)\r\n");
        printf("  -r <num>      set number of reserved blocks (default 0)\r\n");
        printf("  -j <num>      set journal size in blocks (0: none, -1: calculate)\r\n");
        printf("  -w <cnt>      relocate special inodes after this many writes (default 0)\r\n");
        printf("  -u <uuid>     specify file system's unique identifier\r\n");
        printf("  -e <pass>     encrypt with passphrase\r\n");
        printf("  -d <dir>      fill up file system with contents of this directory\r\n");
        printf("  -p <num>      select a GPT partition on device (starts at 1)\r\n");
        printf("\r\nYou should have received a copy of the GNU General Public License v3\r\n"
            "along with this program. If not, see <https://www.gnu.org/licenses/>.\r\n\r\n");
        return 1;
    }
    for(i = 1; i < argc && argv[i]; i++)
        if(argv[i][0] == '-') {
            switch(argv[i][1]) {
                case 'b': bs = atoi(argv[++i]); break;
                case 'i': is = atoi(argv[++i]); break;
                case 'j': jblks = atoi(argv[++i]); break;
                case 'w': wrprot = atoi(argv[++i]); break;
                case 'r': nres = atoi(argv[++i]); break;
                case 'u': getguid(argv[++i], (guid_t*)&dev_uuid); break;
                case 'e': enc = argv[++i]; break;
                case 'p': dev_part = atoi(argv[++i]); break;
                case 'd': dir = argv[++i]; break;
                case 'c':
                    c = argv[++i];
                    size = (uint64_t)atol(c);
                    while(*c && *c >= '0' && *c <= '9') c++;
                    switch(*c) {
                        case 'k': case 'K': size <<= 10; break;
                        case 'm': case 'M': size <<= 20; break;
                        case 'g': case 'G': size <<= 30; break;
                    }
                break;
                default:
                    for(j = 1; argv[i][j]; j++)
                        switch(argv[i][j]) {
                            case 'v': verbose++; break;
                            case 'f': full++; break;
                            default: goto usage;
                        }
                break;
            }
        } else
        if(!fn) fn = argv[i]; else goto usage;
    if(!fn) goto usage;
    printf("\r\n");

    /* sanity check on arguments */
    if(bs < 512 || bs > 65536) { fprintf(stderr, "%s: out of bounds block size\r\n", tool); return 1; }
    for(i = 1, blksiz = 0; i < bs; i <<= 1, blksiz++);
    blksiz -= 9;
    if(i != bs) { fprintf(stderr, "%s: block size is not power of two\r\n", tool); return 1; }
    if(is < 256 || is > bs) { fprintf(stderr, "%s: out of bounds inode size\r\n", tool); return 1; }
    for(i = 1; i < is; i <<= 1);
    if(i != is) { fprintf(stderr, "%s: inode size is not power of two\r\n", tool); return 1; }
    for(inosh = 0; (is << inosh) != bs; inosh++);
    if(wrprot && wrprot < wmin) { wrprot = wmin; fprintf(stderr, "%s: write cycle protection too low, setting to %u\r\n", tool, wmin); }

    /* open device */
    if(!dev_open(fn) || size) {
        if(dev_f) dev_close();
        if(!S_ISBLK(dev_st.st_mode) && size) {
            /* create a sparse file */
#ifdef __WIN32__
            MultiByteToWideChar(CP_UTF8, 0, out, -1, szFile, PATH_MAX);
            for(i = 0; szFile[i]; i++) if(szFile[i] == L'/') szFile[i] = L'\\';
            dev_f = CreateFileW(szFile, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
#else
            umask(0111);
            dev_f = open(fn, O_WRONLY | O_CREAT | O_TRUNC, 0666);
#endif
            if(!dev_f) goto err;
            i = 0;
            if(!dev_write(NULL, size - 1, &i, 1)) { dev_close(); goto err; }
            /* re-open with correct flags */
            dev_close();
            if(!dev_open(fn)) goto err;
        } else {
err:        fprintf(stderr, "%s: unable to open '%s'\r\n", tool, fn);
            return 2;
        }
    }
    if(!dev_offs && !memcmp(dev_gpt, "EFI PART", 8)) {
        printf("Disk contains a GUID Partitioning Table, but no -p flag given.\r\n");
        printf("Overwrite GPT (y/N)? "); fflush(stdout);
        line[0] = 0; fgets(line, sizeof(line) - 1, stdin);
        if(line[0] != 'y') { dev_close(); return 0; }
    }

    /* full format */
    dev_size &= ~((uint64_t)bs - 1);
    if(jblks == -1) {
        jblks = dev_size / (uint64_t)bs / 100;
        if(jblks < 64) jblks = 64;
        if(jblks > 256) jblks = 256;
    }
    b = 8 + nres + !!wrprot + !inosh + jblks;
    if((dev_size / (uint64_t)bs) < b) {
        b = (b * bs + 1023) >> 10; i = 'K';
        if(b > 1023) { b = (b + 1023) >> 10; i = 'M'; }
        fprintf(stderr, "%s: disk size too small (minimum volume size is %lu %ciB for this configuration)\r\n", tool, b, i);
        dev_close(); return 1;
    }
    if(full || (enc && *enc)) {
        blk1 = (uint8_t*)malloc(bs);
        blk2 = (uint8_t*)malloc(bs);
        blk3 = (uint8_t*)malloc(bs);
        if(!blk1 || !blk2 || !blk3) { fprintf(stderr, "%s: unable to allocate memory\r\n", tool); dev_close(); return 1; }
        memset(blk1, 0xff, bs); memset(blk3, 0, bs);
        printf("Formatting disk, please be patient...\r\n");
        for(b = 0; b < dev_size; b += bs) {
            printf("\r%3u%%, block %lu / %lu, bad blocks %u", (uint32_t)(b * 100 / dev_size), b / (uint64_t)bs,
                dev_size / (uint64_t)bs, nbad); fflush(stdout);
            if(enc && *enc)
                for(i = 0; i < bs; i += 4) *((uint32_t*)(blk3 + i)) = rand();
            dev_write(NULL, b, blk3, bs);
            if(full) {
                if(!dev_write(NULL, b, blk1, bs) || !dev_read(NULL, b, blk2, bs) || memcmp(blk1, blk2, bs)) {
                    /* collect bad blocks */
                    bad = (uint64_t*)realloc(bad, (nbad + 1) * sizeof(uint64_t));
                    if(!bad) { fprintf(stderr, "\n%s: unable to allocate memory\r\n", tool); dev_close(); return 1; }
                    bad[nbad++] = b / (uint64_t)bs;
                } else dev_write(NULL, b, blk3, bs);
            }
        }
        free(blk1); free(blk2); free(blk3);
        status("Done.", NULL);
        printf("\r\n");
    }

    if(verbose) {
        printf("UUID:             %08X-%04X-%04X-%02X%02X%02X%02X%02X%02X%02X%02X\r\n", uuid->Data1, uuid->Data2, uuid->Data3,
            uuid->Data4[0], uuid->Data4[1], uuid->Data4[2], uuid->Data4[3],
            uuid->Data4[4], uuid->Data4[5], uuid->Data4[6], uuid->access);
        printf("Position:         %016lx - %016lx\r\n", dev_offs, dev_size);
        memset(line, 0, sizeof(line)); line[1] = 'i'; line[2] = 'B'; size = dev_size;
        if(size > 1023) { size = (size + 1023) >> 10; line[0] = 'K';
            if(size > 1023) { size = (size + 1023) >> 10; line[0] = 'M';
                if(size > 1023) { size = (size + 1023) >> 10; line[0] = 'G';
                    if(size > 1023) { size = (size + 1023) >> 10; line[0] = 'T'; }
                }
            }
        }
        printf("Total capacity:   %lu blocks (%lu %s)\r\n", dev_size / (uint64_t)bs, size, line);
        printf("Block size:       %u bytes (s_blksize %u)\r\n", bs, blksiz);
        printf("Inode size:       %u bytes (s_inoshift %u)\r\n", is, inosh);
        printf("Inodes per block: %u\r\n", 1 << inosh);
        printf("Reserved blocks:  %u blocks\r\n", nres);
        printf("First usable LBN: %u\r\n", nres + !!wrprot);
        printf("Journal buffer:   %u blocks\r\n", jblks);
        printf("Wrcycle protect:  %s", wrprot ? "yes" : "no");
        if(wrprot) printf(" (relocate every %u writes)", wrprot);
        printf("\r\nEncryption:       %s\r\n", enc && *enc ? "yes" : "no");
        printf("Bad blocks:       %u blocks\r\n", nbad);
        printf("\r\n");
    }

    /* create file system */
    if(!(i = axfs_mkfs(NULL, blksiz, inosh, dev_size / (uint64_t)bs, nres, dev_uuid, jblks, wrprot, nbad, bad, enc))) {
        fprintf(stderr, "\n%s: unable to create file system\r\n", tool);
        dev_close(); return 1;
    } else {
        printf("AXFS with %lu blocks created successfully.\r\n", dev_size / (uint64_t)bs);
    }

    /* fill up with data */
    if(dir) {
        if(!axfs_mount(&ctx, NULL, enc)) { fprintf(stderr, "%s: unable to mount\r\n", tool); dev_close(); return 1; }
        parsedir(dir, 0, ctx.sb.s_root_fid);
        axfs_umount(&ctx);
    }

    dev_close();
    return 0;
}
