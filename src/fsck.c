/*
 * src/fsck.c
 * https://gitlab.com/bztsrc/alexandriafs
 *
 * Copyright (C) 2023 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief File system checker for AleXandria File System
 */

char *tool = "fsck.axfs";
#include "dev.h"

#define AXFS_IMPLEMENTATION
#define AXFS_BASICMNT
#include <libaxfs.h>

int force = 0;
axfs_ctx_t ctx;

/**
 * Main function
 */
int main(int argc, char **argv)
{
    uint32_t i, ret = 0;
    char *fn = NULL, *enc = NULL;

    /* parse command line */
    printf("AleXandriaFS checker and fixer Copyright (c) 2023 bzt, GPLv3+\r\n");
    if(argc < 2) {
usage:  printf("https://gitlab.com/bztsrc/alexandriafs\r\n\r\n");
        printf("  %s [options] <image|device>\r\n\r\n", tool);
        printf("  -v            be verbose\r\n");
        printf("  -f            force fix (answer yes to all questions)\r\n");
        printf("  -e <pass>     encryption passphrase\r\n");
        printf("  -p <num>      select a GPT partition on device (starts at 1)\r\n");
        printf("\r\nYou should have received a copy of the GNU General Public License v3\r\n"
            "along with this program. If not, see <https://www.gnu.org/licenses/>.\r\n\r\n");
        return 1;
    }
    for(i = 1; (int)i < argc && argv[i]; i++)
        if(argv[i][0] == '-') {
            switch(argv[i][1]) {
                case 'v': verbose++; break;
                case 'f': force++; break;
                case 'e': enc = argv[++i]; break;
                case 'p': dev_part = atoi(argv[++i]); break;
                default: goto usage;
            }
        } else
        if(!fn) fn = argv[i]; else goto usage;
    if(!fn) goto usage;
    printf("\r\n");

    /* open device */
    if(!dev_open(fn)) { fprintf(stderr, "%s: unable to open '%s'\r\n", tool, fn); return 2; }
    memset(&ctx, 0, sizeof(axfs_ctx_t));
    if(!dev_read(NULL, 0, &ctx.sb, sizeof(ctx.sb))) { fprintf(stderr, "%s: unable to read superblock\r\n", tool); dev_close(); return 2; }
    if(!memcmp(&ctx.sb, "\x1f\x8b\x08", 3)) { fprintf(stderr, "%s: gzip compressed image!\r\n", tool); dev_close(); return 2; }
    if(memcmp(&ctx.sb.s_magic, AXFS_SB_MAGIC, 4) || memcmp(&ctx.sb.s_magic2, AXFS_SB_MAGIC, 4) ||
      ctx.sb.s_blksize > 11 || ctx.sb.s_inoshift > 12 || !ctx.sb.s_volume_fid || !ctx.sb.s_root_fid || !ctx.sb.s_freeblk_fid)
      { fprintf(stderr, "%s: not a valid AleXandria File System\r\n", tool); dev_close(); return 2; }
    if(ctx.sb.s_chksum != axfs_crc32(0, ctx.sb.s_magic, 0xF4))
      fprintf(stderr, "%s: invalid checksum in AXFS superblock\r\n", tool);
    if(ctx.sb.s_enchash && (!enc || !*enc || ctx.sb.s_enchash != axfs_crc32(0, (uint8_t*)enc, strlen(enc))))
      { fprintf(stderr, "%s: bad encryption passphrase\r\n", tool); dev_close(); return 2; }
    /* this should never fail (except on malloc and bad passphrase), because we did all the checks above,
     * providing detailed error messages. We still want to do this, because it sets up internal context
     * must specify AXFS_BASICMNT, because we need to open faulty volumes too*/
    if(!axfs_mount(&ctx, NULL, enc)) { fprintf(stderr, "%s: unable to mount\r\n", tool); dev_close(); return 1; }

    axfs_umount(&ctx);
    dev_close();
    return ret;
}
