/*
 * src/tool.c
 * https://gitlab.com/bztsrc/alexandriafs
 *
 * Copyright (C) 2023 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief no root privilege required AleXandriaFS swiss army knife
 */

char *tool = "tool.axfs";
#include "dev.h"

void dump_hex(void *data, uint32_t n, uint32_t prefix);

#define AXFS_IMPLEMENTATION
#define AXFS_BASICMNT
#include <libaxfs.h>

int force = 0;
char tmpstr[256];
const char *filetypes[] = { "DIR", "RES", "UNI", "LNK", "DEV", "PIPE", "SOCK", "BOOT", "APPL", "AUDIO", "FONT", "IMAGE", "MESSAGE",
    "MODEL", "MULTI", "TEXT", "VIDEO", "DBASE", "DOC" };
const char *finttypes[] = { "UQTA", "AIDX", "JRNL", "VOL", "BAD", "FREE" };
axfs_ctx_t ctx;

/**
 * Convert hex string into integer
 */
uint64_t gethex(char *ptr, int len)
{
    uint64_t ret = 0;
    for(;len--;ptr++) {
        if(*ptr>='0' && *ptr<='9') {          ret <<= 4; ret += (unsigned int)(*ptr-'0'); }
        else if(*ptr >= 'a' && *ptr <= 'f') { ret <<= 4; ret += (unsigned int)(*ptr-'a'+10); }
        else if(*ptr >= 'A' && *ptr <= 'F') { ret <<= 4; ret += (unsigned int)(*ptr-'A'+10); }
        else break;
    }
    return ret;
}

/**
 * Hex dump
 */
void dump_hex(void *data, uint32_t n, uint32_t prefix)
{
    uint8_t *ptr = (uint8_t*)data;
    uint32_t i, j, k, l = 0;

    for(j = 0; j < n; j += 16, ptr += 16) {
        k = j + 16 < n ? 16 : n - j;
        if(l) {
            for(i = 0; i < k && !ptr[i]; i++);
            if(i >= k && k == 16) {
                while(j < n) {
                    j += 16; ptr += 16; k = j + 16 < n ? 16 : n - j;
                    for(i = 0; i < k && !ptr[i]; i++);
                    if(i < k) break;
                }
                if(j < n) { for(i = 0; i < (j ? prefix : 2); i++) { printf(" "); } printf("*\r\n"); }
                else break;
            }
        }
        for(i = 0; i < (j ? prefix : 2); i++) printf(" ");
        printf("%04x: ", j);
        for(i = 0; i < 16; i++) if(i < k) printf("%02x ", ptr[i]); else printf("   ");
        printf(" ");
        for(i = 0, l = 1; i < k && i < 16; i++) {
            if(ptr[i]) l = 0;
            printf("%c", ptr[i] >= 32 && ptr[i] < 127 ? ptr[i] : '.');
        }
        printf("\r\n");
    }
}

/**
 * Dump an acl
 */
char *dump_acl(axfs_access_t *acl)
{
    sprintf(tmpstr, "%08X-%04X-%04X-%02X%02X%02X%02X%02X%02X%02X:%s%c%c%c%c%c%c%c",
        acl->Data1, acl->Data2, acl->Data3, acl->Data4[0], acl->Data4[1], acl->Data4[2], acl->Data4[3], acl->Data4[4],
        acl->Data4[5], acl->Data4[6], acl->access & AXFS_NOT ? "!" : "", acl->access & AXFS_READ ? 'r' : '-',
        acl->access & AXFS_WRITE ? 'w' : '-', acl->access & AXFS_EXEC ? 'x' : '-', acl->access & AXFS_APPEND ? 'a' : '-',
        acl->access & AXFS_DELETE ? 'd' : '-', acl->access & AXFS_SUID ? 's' : '-', acl->access & AXFS_SGID ? 'g' : '-');
    return tmpstr;
}

/**
 * Dump a timestamp
 */
char *dump_time(uint64_t ts)
{
    time_t t = ts / 1000000;
    struct tm *tm = localtime(&t);
    sprintf(tmpstr, "%04u-%02u-%02u %02u:%02u:%02u.%06u", tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min,
        tm->tm_sec, (uint32_t)(ts % 1000000));
    return tmpstr;
}

/**
 * Dump one block run
 */
uint8_t *dump_run(uint8_t *ptr)
{
    axfs_extent_t ext = { 0 };
    if(!*ptr) { sprintf(tmpstr, "(end of list)"); ptr++; }
    else { ptr = axfs_unpack(ptr, &ext); sprintf(tmpstr, "LBN %lu, num %lu, len %u", ext.e_blk, ext.e_num, ext.e_len); }
    return ptr;
}

/**
 * Dump a block
 */
void dump_lbn(uint64_t lbn)
{
    uint32_t i, j = 0;
    axfs_inode_t *ino;
    axfs_dirhdr_t *dir = (axfs_dirhdr_t*)ctx.blk[0];
    axfs_eahdr_t *ea = (axfs_eahdr_t*)ctx.blk[0];

    /* get the block */
    printf("  Disk offset:      %016lx\r\n", dev_offs + lbn * ctx.blksiz);
    if(lbn >= ctx.sb.s_nblks || !axfs_read_blk(&ctx, lbn, ctx.blk[0]))
      { fprintf(stderr, "%s: unable to read block\r\n", tool); printf("\r\n"); return; }

    /* check if this is an inode block */
    for(i = 0; i < ctx.blksiz && memcmp(ctx.blk[0] + i, AXFS_IN_MAGIC, 4); i += ctx.inosiz);
    if(i < ctx.blksiz) {
        for(i = j = 0; i < ctx.blksiz; i += ctx.inosiz, j++) {
            ino = (axfs_inode_t*)(ctx.blk[0] + i);
            printf("  Inode entry:      %04x: fid %016lx, ", i, (lbn << ctx.sb.s_inoshift) | j);
            if(!memcmp(ctx.blk[0] + i, AXFS_IN_MAGIC, 4)) {
                printf("used, type %02x %08x (AXFS_FT_%s%s", ino->i_type, ino->i_mime, ino->i_type & 0x80 ? "INT_" : "",
                    ino->i_type < (uint8_t)(sizeof(filetypes)/sizeof(filetypes[0])) ? filetypes[ino->i_type] :
                    (ino->i_type >= AXFS_FT_INT_UQTA ? finttypes[ino->i_type - AXFS_FT_INT_UQTA] : "?"));
                if(ino->i_type == AXFS_FT_DIR && ino->i_mime == AXFS_MT_ROOT) printf(", AXFS_MT_ROOT");
                printf(")\r\n");
            } else
            if(!memcmp(ctx.blk[0] + i + 4, AXFS_SB_MAGIC, 4)) printf("AleXandriaFS SuperBlock (%s)\r\n", !lbn && !i ? "valid" : "BAD");
            else printf("free\r\n");
        }
        goto hex;
    }
    j = 0;

    /* check for superblock */
    if(!memcmp(ctx.blk[0] + 4, AXFS_SB_MAGIC, 4)) { printf("  AleXandriaFS SuperBlock (%s)\r\n", !lbn ? "valid" : "BAD"); j++; } else
    /* check for directory */
    if(!memcmp(ctx.blk[0], AXFS_DR_MAGIC, 4)) {
        printf("  Directory\r\n");
        printf("  Magic:            %c%c%c%c\r\n", dir->d_magic[0], dir->d_magic[1], dir->d_magic[2], dir->d_magic[3]);
        printf("  Index checksum:   %08x\r\n", dir->d_chksum);
        printf("  Entries size:     %u bytes\r\n", dir->d_size);
        printf("  Num of entries:   %u record(s)\r\n", dir->d_nument);
        printf("  Inode backref:    fid %016lx\r\n", dir->d_self_fid);
        printf("  GUI attribs:      %02x %02x %02x %02x %02x %02x %02x %02x\r\n",
            dir->d_gui_attr[0], dir->d_gui_attr[1], dir->d_gui_attr[2], dir->d_gui_attr[3],
            dir->d_gui_attr[4], dir->d_gui_attr[5], dir->d_gui_attr[6], dir->d_gui_attr[7]);
        j++;
    } else
    /* check for extended attributes */
    if(!memcmp(ctx.blk[0], AXFS_EA_MAGIC, 4) || !memcmp(ctx.blk[0], AXFS_IK_MAGIC, 4) || !memcmp(ctx.blk[0], AXFS_IV_MAGIC, 4)) {
        printf("  Extended attributes%s\r\n", !memcmp(ctx.blk[0], AXFS_IK_MAGIC, 4) ? " (index keys)" : (
            !memcmp(ctx.blk[0], AXFS_IV_MAGIC, 4) ? " (index values)" : ""));
        printf("  Magic:            %c%c%c%c\r\n", ea->a_magic[0], ea->a_magic[1], ea->a_magic[2], ea->a_magic[3]);
        printf("  Checksum:         %08x\r\n", ea->a_chksum);
        printf("  Entries size:     %u bytes\r\n", ea->a_size);
        printf("  Num of entries:   %u record(s)\r\n", ea->a_nument);
        if(!memcmp(ctx.blk[0], AXFS_IV_MAGIC, 4))
            printf("  Index key offset: %08lx\r\n", ea->a_self_fid);
        else
            printf("  Inode backref:    fid %016lx\r\n", ea->a_self_fid);
    }

hex:/* hexdump contents */
    for(i = ctx.blksiz - 1; i > 0 && !ctx.blk[0][i]; i--);
    if(!i && !ctx.blk[0][0]) printf("  (empty block)\r\n");
    else { printf("%s\r\n", !j ? "  Data block" : ""); dump_hex(ctx.blk[0], i + 1, 2); }
}

/**
 * Dump an inode
 */
void dump_inode(uint64_t fid)
{
    uint8_t *d = NULL, p;
    uint32_t i;
    uint64_t off = (fid & ((1 << ctx.sb.s_inoshift) - 1)) * ctx.inosiz;
    axfs_extent_t ext = { 0 };
    axfs_inode_t *ino = (axfs_inode_t*)(ctx.blk[0] + off);
    axfs_inode_s_t *ino_s = (axfs_inode_s_t*)(ctx.blk[0] + off);
    axfs_inode_v_t *ino_v = (axfs_inode_v_t*)(ctx.blk[0] + off);
    axfs_inode_j_t *ino_j = (axfs_inode_j_t*)(ctx.blk[0] + off);
    axfs_access_t empty_acl = { 0 }, catchall_acl;

    printf("  Location:         LBN %lu, index %u\r\n", fid >> ctx.sb.s_inoshift, (uint32_t)(fid & ((1 << ctx.sb.s_inoshift) - 1)));
    printf("  Disk offset:      %016lx\r\n", dev_offs + (fid >> ctx.sb.s_inoshift) * ctx.blksiz + off);
    if((fid >> ctx.sb.s_inoshift) >= ctx.sb.s_nblks || !axfs_read_blk(&ctx, fid >> ctx.sb.s_inoshift, ctx.blk[0]))
      { fprintf(stderr, "%s: unable to read block\r\n", tool); printf("\r\n"); return; }
    if(!fid && !memcmp(ctx.blk[0] + 4, AXFS_SB_MAGIC, 4)) { printf("  (AleXandriaFS Superblock, not an inode)\r\n\r\n"); return; }
    if(memcmp(ino->i_magic, AXFS_IN_MAGIC, 4)) { printf("  (not a valid AXFS inode)\r\n\r\n"); return; }
    printf("  Magic:            %c%c%c%c\r\n", ino->i_magic[0], ino->i_magic[1], ino->i_magic[2], ino->i_magic[3]);
    printf("  Checksum:         %08x (%s)\r\n", ino->i_chksum,
        ino->i_chksum == axfs_crc32(0, ino->i_magic + 8, ctx.inosiz - 8) ? "valid" : "BAD");
    printf("  Allocated blocks: %lu\r\n", ino->i_nblks);
    printf("  File size:        %lu bytes +(%u << 64)\r\n", ino->i_size, ino->i_size_hi);
    printf("  Flags:            %02x %s\r\n", ino->i_flags, ino->i_flags ? "(inlined)" : "");
    printf("  File type:        %02x (AXFS_FT_%s%s)\r\n", ino->i_type, ino->i_type & 0x80 ? "INT_" : "",
        ino->i_type < (uint8_t)(sizeof(filetypes)/sizeof(filetypes[0])) ? filetypes[ino->i_type] :
        (ino->i_type >= AXFS_FT_INT_UQTA ? finttypes[ino->i_type - AXFS_FT_INT_UQTA] : "?"));
    printf("  %s     %08x%s\r\n", ino->i_type == AXFS_FT_INT_VOL ? "State (fsck):" : "Mime type:   ", ino->i_mime,
        ino->i_type == AXFS_FT_DIR && ino->i_mime == AXFS_MT_ROOT ? " (AXFS_MT_ROOT)" : "");
    if(ino->i_type > 128) {
        if(ino->i_type == AXFS_FT_INT_FREE || ino->i_type == AXFS_FT_INT_BAD || ino->i_type == AXFS_FT_INT_AIDX ||
          ino->i_type == AXFS_FT_INT_UQTA)
            d = ((axfs_inode_d_t*)ino)->i_d;
        else
        if(ino->i_type == AXFS_FT_INT_VOL) {
            printf("  Number of inodes: %lu\r\n", ino_v->i_ninode);
            printf("  Last mount time:  %lu (%s)\r\n", ino_v->i_lmtime, dump_time(ino_v->i_lmtime));
            printf("  Last umount time: %lu (%s)\r\n", ino_v->i_lutime, dump_time(ino_v->i_lutime));
            printf("  Inode writes:     max %u countdown %u\r\n", ino_v->i_nwrite, ino_v->i_cwrite);
            d = ino_v->i_d;
        } else
        if(ino->i_type == AXFS_FT_INT_JRNL) {
            printf("  Journal buffer:   LBN %lu, %lu blocks\r\n", ino_j->i_blk, ino_j->i_len);
            printf("  Circular buffer:  head %lu, tail %lu blocks\r\n", ino_j->i_head, ino_j->i_tail);
            d = ino_j->i_d;
        } else
            printf("  invalid special inode type\r\n");
    } else {
        printf("  Number of links:  %lu\r\n", ino->i_nlinks);
        printf("  Birth time:       %lu (%s)\r\n", ino->i_btime, dump_time(ino->i_btime));
        printf("  Changed time:     %lu (%s)\r\n", ino->i_ctime, dump_time(ino->i_ctime));
        printf("  Modified time:    %lu (%s)\r\n", ino->i_mtime, dump_time(ino->i_mtime));
        if(ino->i_attr[0]) {
            axfs_unpack(ino->i_attr, &ext);
            if(ext.e_blk) sprintf(tmpstr, "AXEA LBN %lu, ", ext.e_blk); else tmpstr[0] = 0;
            sprintf(tmpstr + strlen(tmpstr), "parent fid %016lx", ext.e_num);
        } else
            sprintf(tmpstr, "(none)");
        printf("  Extended attribs: %s\r\n", tmpstr);
        printf("  Owner:            %s\r\n", dump_acl(&ino->i_owner));
        memset(&catchall_acl, 0xff, 15);
        for(i = 0; i < (ctx.inosiz == 256 ? 2 : 10) && memcmp(&ino->i_acl[i], &empty_acl, 15); i++) {
            printf("  %s   %s\r\n", !i ? "Access Control:" : "               ", dump_acl(&ino->i_acl[i]));
            if(memcmp(&ino->i_acl[i], &catchall_acl, 15)) break;
        }
        d = ctx.inosiz == 256 ? ino_s->i_d : ino->i_d;
        if(ino->i_flags & 1) {
            off = (uint8_t*)ino + ctx.inosiz - d; if(ino->i_size < off) off = ino->i_size;
            printf("  Inlined data:   "); dump_hex(d, off, 20); d = NULL;
        }
    }
    if(d) {
        for(p = *d, i = 0; d && d < (uint8_t*)ino + ctx.inosiz; i++) {
            p = *d; d = dump_run(d); printf("  %s      %s\r\n", !i ? "Data stream:" : "            ", tmpstr);
            if(!p) break;
        }
    }
    printf("\r\n");
}

/**
 * Recursively display a data stream mapping
 */
void dump_ds(uint8_t *ds, uint8_t *top, uint64_t offs, uint32_t lvl)
{
    axfs_extent_t ext;
    uint32_t i;

    if(!ds || !top || lvl > 2) return;
    lvl++;

    while(ds && ds < top && *ds) {
        ds = axfs_unpack(ds, &ext);
        for(i = 0; i < lvl; i++) printf("  ");
        printf("%016lx - %016lx, LBN %lu, num %lu, len %u", offs, offs + ctx.blksiz * ext.e_num, ext.e_blk, ext.e_num, ext.e_len);
        if(!ext.e_blk)
            printf(" (sparse)\r\n");
        else {
            printf("\r\n");
            for(i = 0; i < ext.e_len; i++) {
                if(!axfs_read_blk(&ctx, ext.e_blk + (uint64_t)i, ctx.blk[lvl]))
                    { fprintf(stderr, "%s: unable to read block (LBN %lu)\r\n", tool, ext.e_blk + (uint64_t)i); printf("\r\n"); break; }
                dump_ds(ctx.blk[lvl], ctx.blk[lvl] + ctx.blksiz, offs, lvl);
            }
        }
        offs += ctx.blksiz * ext.e_num;
    }
}

/**
 * Dump the full data sream mapping in an inode
 */
void dump_dsmap(uint64_t fid)
{
    uint64_t off = (fid & ((1 << ctx.sb.s_inoshift) - 1)) * ctx.inosiz;
    axfs_inode_t *ino = (axfs_inode_t*)(ctx.blk[0] + off);

    printf("  Location:         LBN %lu, index %u\r\n", fid >> ctx.sb.s_inoshift, (uint32_t)(fid & ((1 << ctx.sb.s_inoshift) - 1)));
    printf("  Disk offset:      %016lx\r\n", dev_offs + (fid >> ctx.sb.s_inoshift) * ctx.blksiz + off);
    if((fid >> ctx.sb.s_inoshift) >= ctx.sb.s_nblks || !axfs_read_blk(&ctx, fid >> ctx.sb.s_inoshift, ctx.blk[0]))
      { fprintf(stderr, "%s: unable to read block\r\n", tool); printf("\r\n"); return; }
    if(memcmp(ino->i_magic, AXFS_IN_MAGIC, 4)) { printf("  (not a valid AXFS inode)\r\n\r\n"); return; }
    printf("  Allocated blocks: %lu\r\n", ino->i_nblks);
    printf("  File size:        %lu bytes +(%u << 64)\r\n", ino->i_size, ino->i_size_hi);
    printf("  File type:        %02x (AXFS_FT_%s%s)\r\n", ino->i_type, ino->i_type & 0x80 ? "INT_" : "",
        ino->i_type < (uint8_t)(sizeof(filetypes)/sizeof(filetypes[0])) ? filetypes[ino->i_type] :
        (ino->i_type >= AXFS_FT_INT_UQTA ? finttypes[ino->i_type - AXFS_FT_INT_UQTA] : "?"));
    printf("  Flags:            %02x %s\r\n\r\n", ino->i_flags, ino->i_flags ? "(inlined)" : "");
    if(ino->i_flags & 1)
        printf("  (no block runs, inlined data)\r\n");
    else
        dump_ds(ctx.blk[0] + off + (ino->i_type == AXFS_FT_INT_FREE || ino->i_type == AXFS_FT_INT_BAD || ino->i_type == AXFS_FT_INT_AIDX ||
            ino->i_type == AXFS_FT_INT_UQTA ? 32 : (ino->i_type == AXFS_FT_INT_VOL || ino->i_type == AXFS_FT_INT_JRNL ? 64 :
            (ctx.inosiz == 256 ? 128 : 256))), (uint8_t*)ino + ctx.inosiz, 0, 0);
}

/**
 * Dump superblock
 */
void dump_sb(void)
{
    axfs_access_t *uuid = (axfs_access_t*)&ctx.sb.s_uuid;
    uint64_t capacity;
    char unit = 'K';

    printf("Superblock:\r\n");
    printf("  Location:         LBN %u, index %u\r\n", 0, 0);
    printf("  Disk offset:      %016x\r\n", 0);
    printf("  Magic:            %c%c%c%c\r\n", ctx.sb.s_magic[0], ctx.sb.s_magic[1], ctx.sb.s_magic[2], ctx.sb.s_magic[3]);
    printf("  Version:          %u.%u\r\n", ctx.sb.s_major, ctx.sb.s_minor);
    printf("  Block size:       %u bytes (s_blksize %u)\r\n", ctx.blksiz, ctx.sb.s_blksize);
    printf("  Inode size:       %u bytes (s_inoshift %u)\r\n", ctx.inosiz, ctx.sb.s_inoshift);
    printf("  Flags:            %04x %s\r\n", ctx.sb.s_flags, ctx.sb.s_flags & AXFS_SB_F_NOCRC ? "(AXFS_SB_F_NOCRC)" : "");
    printf("  Enctype:          %02x (%s)\r\n", ctx.sb.s_enctype, !ctx.sb.s_enctype ? "AXFS_SB_E_SHA" :
        (ctx.sb.s_enctype == 1 ? "AXFS_SB_E_AES" : "?"));
    printf("  Encyrpt:          %02x %02x %02x %02x ... %02x %02x %02x %02x\r\n", ctx.sb.s_encrypt[0], ctx.sb.s_encrypt[1],
        ctx.sb.s_encrypt[2], ctx.sb.s_encrypt[3], ctx.sb.s_encrypt[24], ctx.sb.s_encrypt[25], ctx.sb.s_encrypt[26], ctx.sb.s_encrypt[27]);
    printf("  Enchash:          %04x%s\r\n", ctx.sb.s_enchash, !ctx.sb.s_enchash ? " (no encryption)" : "");
    printf("  UUID:             %08X-%04X-%04X-%02X%02X%02X%02X%02X%02X%02X%02X\r\n", uuid->Data1, uuid->Data2, uuid->Data3,
        uuid->Data4[0], uuid->Data4[1], uuid->Data4[2], uuid->Data4[3],
        uuid->Data4[4], uuid->Data4[5], uuid->Data4[6], uuid->access);
    capacity = (ctx.sb.s_nblks * ctx.blksiz + 1023) >> 10;
    if(capacity >= 1024) { capacity += 1023; capacity >>= 10; unit = 'M';
        if(capacity >= 1024) { capacity += 1023; capacity >>= 10; unit = 'G';
            if(capacity >= 1024) { capacity += 1023; capacity >>= 10; unit = 'T';
                if(capacity >= 1024) { capacity += 1023; capacity >>= 10; unit = 'P';
                    if(capacity >= 1024) { capacity += 1023; capacity >>= 10; unit = 'Z'; }}}}}
    printf("  Total blocks:     %lu (%lu %ciB)\r\n", ctx.sb.s_nblks, capacity, unit);
    printf("  Volume fid:       %016lx\r\n", ctx.sb.s_volume_fid);
    printf("  Root dir fid:     %016lx\r\n", ctx.sb.s_root_fid);
    printf("  Free blocks fid:  %016lx\r\n", ctx.sb.s_freeblk_fid);
    printf("  Journal fid:      %016lx\r\n", ctx.sb.s_journal_fid);
    printf("  Attributes fid:   %016lx\r\n", ctx.sb.s_attridx_fid);
    printf("  User quota fid:   %016lx\r\n", ctx.sb.s_uquota_fid);
    printf("  Bad blocks fid:   %016lx\r\n", ctx.sb.s_badblk_fid);
    printf("  Checksum:         %08x (%s)\r\n", ctx.sb.s_chksum, ctx.sb.s_chksum == axfs_crc32(0, ctx.sb.s_magic, 0xF4) ? "valid" : "BAD");
    printf("\r\n");
    if(ctx.sb.s_volume_fid) { printf("Volume inode:\r\n"); dump_inode(ctx.sb.s_volume_fid); }
    if(ctx.sb.s_root_fid) { printf("Root directory inode:\r\n"); dump_inode(ctx.sb.s_root_fid); }
    if(ctx.sb.s_freeblk_fid) { printf("Free blocks inode:\r\n"); dump_inode(ctx.sb.s_freeblk_fid); }
    if(ctx.sb.s_journal_fid) { printf("Journal inode:\r\n"); dump_inode(ctx.sb.s_journal_fid); }
    if(ctx.sb.s_attridx_fid) { printf("Attribute index inode:\r\n"); dump_inode(ctx.sb.s_attridx_fid); }
    if(ctx.sb.s_uquota_fid) { printf("User quota inode:\r\n"); dump_inode(ctx.sb.s_uquota_fid); }
    if(ctx.sb.s_badblk_fid) { printf("Bad blocks inode:\r\n"); dump_inode(ctx.sb.s_badblk_fid); }
}

/**
 * cat command
 */
void cmd_cat(char *path)
{
    axfs_file_t file = { 0 };
    uint64_t size;
    uint8_t *data;

    axfs_mount_load(&ctx);
    if(axfs_open(&file, &ctx, path, 0)) {
        size = ((axfs_inode_t*)file.ino)->i_size;
        data = (uint8_t*)malloc(size + 1);
        if(!data) { fprintf(stderr, "%s: unable to allocate memory\r\n", tool); return; }
        axfs_read(&file, data, size);
        data[size] = 0;
        printf("%s", data);
        free(data);
        axfs_close(&file);
    } else fprintf(stderr, "%s: path '%s' not found in image\r\n", tool, path);
}

/**
 * ls command
 */
void cmd_ls(char *path)
{
    axfs_file_t file = { 0 };
    axfs_dirent_t ent;
    axfs_inode_t *ino;
    uint8_t *d;

    axfs_mount_load(&ctx);
    if(!path || !*path) path = "/";
    if(axfs_open(&file, &ctx, path, 0)) {
        ino = (axfs_inode_t *)file.ino;
        d = ctx.inosiz == 256 ? ((axfs_inode_s_t *)ino)->i_d : ino->i_d;

        switch(ino->i_type) {
            case AXFS_FT_DIR:
                printf("  Magic:            %c%c%c%c\r\n", file.dir.hdr.d_magic[0], file.dir.hdr.d_magic[1], file.dir.hdr.d_magic[2], file.dir.hdr.d_magic[3]);
                printf("  Checksum:         %08x (%s)\r\n", file.dir.hdr.d_chksum,
                    !file.dir.hdr.d_chksum || (file.dir.idx && file.dir.idxlen &&
                    file.dir.hdr.d_chksum == axfs_crc32(0, (uint8_t*)file.dir.idx, file.dir.idxlen)) ? "valid" : "BAD");
                printf("  Entries size:     %u bytes\r\n", file.dir.hdr.d_size);
                printf("  Num of entries:   %u record(s)\r\n\r\n", file.dir.hdr.d_nument);
                while(axfs_readdir(&file, &ent))
                    printf("  %016lx %s\r\n", ent.d_fid, ent.d_name);
            break;
            case AXFS_FT_SDR:
                printf("  Search directory\r\n\r\n  %s\r\n", (char*)d);
            break;
            case AXFS_FT_UNI:
                printf("  Directory union\r\n\r\n");
                for(; *d; d += strlen((char*)d) + 1) printf("  %s\r\n", d);
            break;
            default: fprintf(stderr, "%s: path '%s' not directory (type %02x AXFS_FT_%s%s)\r\n", tool, path, ino->i_type,
                ino->i_type & 0x80 ? "INT_" : "",
                ino->i_type < (uint8_t)(sizeof(filetypes)/sizeof(filetypes[0])) ? filetypes[ino->i_type] :
                (ino->i_type >= AXFS_FT_INT_UQTA ? finttypes[ino->i_type - AXFS_FT_INT_UQTA] : "?")); break;
        }
        axfs_close(&file);
    } else fprintf(stderr, "%s: path '%s' not found in image\r\n", tool, path);
}

/**
 * mkdir command
 */
void cmd_mkdir(char *path)
{
    axfs_mount_load(&ctx);
    if(!axfs_mkdir(&ctx, path)) { fprintf(stderr, "%s: unable to create directory\r\n", tool); }
}

/**
 * mksdr command
 */
void cmd_mksdr(const char *path, const char *target)
{
    axfs_mount_load(&ctx);
    if(!axfs_mksdr(&ctx, path, target)) { fprintf(stderr, "%s: unable to create search directory\r\n", tool); }
}

/**
 * mkuni command
 */
void cmd_mkuni(const char *path, const char **targets)
{
    axfs_mount_load(&ctx);
    if(!axfs_mkuni(&ctx, path, targets)) { fprintf(stderr, "%s: unable to create union\r\n", tool); }
}

/**
 * mkfifo command
 */
void cmd_mkfifo(const char *path, uint64_t mode)
{
    axfs_mount_load(&ctx);
    if(!axfs_mkfifo(&ctx, path, mode)) { fprintf(stderr, "%s: unable to create named pipe\r\n", tool); }
}

/**
 * ln command
 */
void cmd_ln(const char *path, const char *target)
{
    axfs_mount_load(&ctx);
    if(!axfs_symlink(&ctx, path, target)) { fprintf(stderr, "%s: unable to create symlink\r\n", tool); }
}

/**
 * cp command
 */
void cmd_cp(char *from, char *to)
{
    axfs_file_t file = { 0 };
    FILE *f;
    uint64_t size;
    uint8_t *data;

    axfs_mount_load(&ctx);
    if(!from || !to || !*from || !*to || (*from != ':' && *to != ':') || (*from == ':' && *to == ':')) {
        fprintf(stderr, "%s: exactly one of source or destination must start with ':' indicating path in image\r\n", tool); return;
    }
    if(*from == ':') {
        f = fopen(to, "wb");
        if(!f) { fprintf(stderr, "%s: unable to write '%s' on host\r\n", tool, to); return; }
        if(axfs_open(&file, &ctx, from + 1, 0)) {
            size = ((axfs_inode_t*)file.ino)->i_size;
            data = (uint8_t*)malloc(size);
            if(!data) { fprintf(stderr, "%s: unable to allocate memory\r\n", tool); fclose(f); axfs_close(&file); return; }
            axfs_read(&file, data, size);
            fwrite(data, 1, size, f);
            free(data);
            axfs_close(&file);
        } else fprintf(stderr, "%s: path '%s' not found in image\r\n", tool, from + 1);
        fclose(f);
    } else {
        f = fopen(from, "rb");
        if(!f) { fprintf(stderr, "%s: unable to read '%s' on host\r\n", tool, from); return; }
        fseek(f, 0, SEEK_END);
        size = (uint64_t)ftell(f);
        fseek(f, 0, SEEK_SET);
        data = (uint8_t*)malloc(size);
        if(!data) { fprintf(stderr, "%s: unable to allocate memory\r\n", tool); fclose(f); return; }
        if(axfs_open(&file, &ctx, to + 1, 1)) {
            size = fread(data, 1, size, f);
            axfs_write(&file, data, size);
            axfs_close(&file);
        } else fprintf(stderr, "%s: unable to write '%s' in image\r\n", tool, to + 1);
        free(data);
        fclose(f);
    }
}

/**
 * rm command
 */
void cmd_rm(const char *path)
{
    axfs_mount_load(&ctx);
    if(!axfs_remove(&ctx, path)) { fprintf(stderr, "%s: unable to remove\r\n", tool); }
}

/**
 * rekey command
 */
void cmd_rekey(char *enc, char *key)
{
    if(!axfs_rekey(&ctx, enc, key)) { fprintf(stderr, "%s: unable to replace passphrase\r\n", tool); }
    else printf("Passphrase changed.\r\n");
}

/**
 * Main function
 */
int main(int argc, char **argv)
{
    int i;
    char *fn = NULL, *enc = NULL;

    /* parse command line */
    printf("AleXandriaFS tool Copyright (c) 2023 bzt, GPLv3+\r\n");
    if(argc < 2) {
usage:  printf("https://gitlab.com/bztsrc/alexandriafs\r\n\r\n");
        printf("  %s [options] <image|device> <cmd> [args]\r\n\r\n", tool);
        printf(" Options:\r\n");
        printf("  -v            be verbose\r\n");
        printf("  -e <pass>     encryption passphrase\r\n");
        printf("  -p <num>      select a GPT partition on device (starts at 1)\r\n");
        printf("\r\n Commands:\r\n");
        printf("  sb                dump superblock and special inodes\r\n");
        printf("  inode <fid>       dump inode (fid must be in hex)\r\n");
        printf("  dsmap <fid>       dump data stream mapping\r\n");
        printf("  lbn <lbn>         dump contents of a block\r\n");
        printf("  hexdump <path>    dump contents of a file in hex\r\n");
        printf("  cat <path>        send contents of a file to stdout\r\n");
        printf("  ls <path>         list directory\r\n");
        printf("  mkdir <path>      create directory\r\n");
        printf("  mksdr <path>      create search directory\r\n");
        printf("  mkuni <pa> <trgs> create directory union\r\n");
        printf("  mkfifo <p> <mode> create named pipe (mode must be in hex)\r\n");
        printf("  ln <path> <trg>   create symbolik link\r\n");
        printf("  cp :<src> <dst>   copy file from image to host\r\n");
        printf("  cp <src> :<dst>   copy file from host to image\r\n");
        printf("  rm <path>         remove file or directory\r\n");
        printf("  rekey <newpass>   replace encryption passphrase (requires -e)\r\n");
        printf("\r\nYou should have received a copy of the GNU General Public License v3\r\n"
            "along with this program. If not, see <https://www.gnu.org/licenses/>.\r\n\r\n");
        return 1;
    }
    for(i = 1; i < argc && argv[i] && !fn; i++)
        if(argv[i][0] == '-') {
            switch(argv[i][1]) {
                case 'v': verbose++; break;
                case 'e': enc = argv[++i]; break;
                case 'p': dev_part = atoi(argv[++i]); break;
                default: goto usage;
            }
        } else
        if(!fn) fn = argv[i];
    if(!fn || i >= argc) goto usage;
    printf("\r\n");

    /* open device */
    if(!dev_open(fn)) { fprintf(stderr, "%s: unable to open '%s'\r\n", tool, fn); return 2; }
    if(!dev_read(NULL, 0, &ctx.sb, sizeof(ctx.sb))) { fprintf(stderr, "%s: unable to read superblock\r\n", tool); dev_close(); return 2; }
    if(!memcmp(&ctx.sb, "\x1f\x8b\x08", 3)) { fprintf(stderr, "%s: gzip compressed image!\r\n", tool); dev_close(); return 2; }
    if(memcmp(&ctx.sb.s_magic, AXFS_SB_MAGIC, 4) || memcmp(&ctx.sb.s_magic2, AXFS_SB_MAGIC, 4) ||
      ctx.sb.s_blksize > 11 || ctx.sb.s_inoshift > 12 || !ctx.sb.s_volume_fid || !ctx.sb.s_root_fid || !ctx.sb.s_freeblk_fid) {
        fprintf(stderr, "%s: not a valid AleXandria File System\r\n", tool);
        if(memcmp(&ctx.sb.s_magic, AXFS_SB_MAGIC, 4) || memcmp(&ctx.sb.s_magic2, AXFS_SB_MAGIC, 4)) fprintf(stderr, "    magic mismatch\r\n");
        if(ctx.sb.s_blksize > 11)   fprintf(stderr, "    block size too big\r\n");
        if(ctx.sb.s_inoshift > 12)  fprintf(stderr, "    inode shift too big\r\n");
        if(!ctx.sb.s_volume_fid)    fprintf(stderr, "    volume fid missing\r\n");
        if(!ctx.sb.s_root_fid)      fprintf(stderr, "    root fid missing\r\n");
        if(!ctx.sb.s_freeblk_fid)   fprintf(stderr, "    freeblk fid missing\r\n");
        dev_close(); return 2;
    }
    if(!(ctx.sb.s_flags & AXFS_SB_F_NOCRC) && ctx.sb.s_chksum != axfs_crc32(0, ctx.sb.s_magic, 0xF4))
      fprintf(stderr, "%s: invalid checksum in AXFS superblock\r\n", tool);
    if(ctx.sb.s_enchash && (!enc || !*enc || ctx.sb.s_enchash != axfs_crc32(0, (uint8_t*)enc, strlen(enc))))
      { fprintf(stderr, "%s: bad encryption passphrase\r\n", tool); dev_close(); return 2; }
    /* this should never fail (except on malloc and bad passphrase), because we did all the checks above,
     * providing detailed error messages. We still want to do this, because it sets up internal context */
    if(!axfs_mount(&ctx, NULL, enc)) { fprintf(stderr, "%s: unable to mount\r\n", tool); dev_close(); return 1; }

    /* choose operating mode */
    if(!strcmp(argv[i], "sb")) { dump_sb(); } else
    if(!strcmp(argv[i], "inode") && i + 1 < argc) { dump_inode(gethex(argv[i + 1], 16)); } else
    if(!strcmp(argv[i], "dsmap") && i + 1 < argc) { dump_dsmap(gethex(argv[i + 1], 16)); } else
    if(!strcmp(argv[i], "lbn") && i + 1 < argc) { dump_lbn(gethex(argv[i + 1], 16)); } else
    if(!strcmp(argv[i], "cat") && i + 1 < argc) { cmd_cat(argv[i + 1]); } else
    if(!strcmp(argv[i], "ls") && i < argc) { cmd_ls(argv[i + 1]); } else
    if(!strcmp(argv[i], "mkdir") && i + 1 < argc) { cmd_mkdir(argv[i + 1]); } else
    if(!strcmp(argv[i], "mksdr") && i + 2 < argc) { cmd_mksdr(argv[i + 1], argv[i + 2]); } else
    if(!strcmp(argv[i], "mkuni") && i + 2 < argc) { cmd_mkuni(argv[i + 1], (const char**)argv + i + 2); } else
    if(!strcmp(argv[i], "mkfifo") && i + 2 < argc) { cmd_mkfifo(argv[i + 1], gethex(argv[i + 2], 16)); } else
    if(!strcmp(argv[i], "ln") && i + 2 < argc) { cmd_ln(argv[i + 1], argv[i + 2]); } else
    if(!strcmp(argv[i], "cp") && i + 2 < argc) { cmd_cp(argv[i + 1], argv[i + 2]); } else
    if(!strcmp(argv[i], "rm") && i + 1 < argc) { cmd_rm(argv[i + 1]); } else
    if(!strcmp(argv[i], "rekey") && i + 1 < argc) { cmd_rekey(enc, argv[i + 1]); } else
        fprintf(stderr, "%s: unknown command or bad number or arguments\r\n", tool);

    axfs_umount(&ctx);
    dev_close();
    return 0;
}
