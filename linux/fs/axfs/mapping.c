// SPDX-License-Identifier: GPL-2.0-only
/*
 *  linux/fs/axfs/mapping.c
 *
 *  Copyright (C) 2023 bzt
 *
 *  AleXandriaFS support.
 */

#include "vfs.h"

int axfs_get_block(struct inode *inode, sector_t block, struct buffer_head *bh_result, int create)
{
	struct super_block *sb = inode->i_sb;
	struct axfs_sb_info *s = (struct axfs_sb_info *)sb->s_fs_info;
	struct axfs_inode_info *axfs_ino = container_of(inode, struct axfs_inode_info, vfs_inode);
	axfs_extent_t ext;
	uint8_t *d = axfs_ino->br, lvl = 0;
	uint64_t o = 0, e;

	if (create)
		return -EPERM;

	if(axfs_ino->flags & 1) {
		/* inlined data */
		get_bh(bh_result);
		memcpy(bh_result->b_data, d, inode->i_size);
		return 0;
	} else {
		while(*d) {
			d = axfs_unpack(d, &ext);
			e = o + ext.e_num * s->block_size;
			if(o == e) break;
			if(block >= o && block < e) {
				if(ext.e_len) {
					/* indirect */
					if(lvl++ > 3) break;
					map_bh(bh_result, inode->i_sb, ext.e_blk);
					d = bh_result->b_data;
				} else {
					/* direct */
					if(ext.e_blk)
						map_bh(bh_result, inode->i_sb, ext.e_blk + o - block);
					else
						/* sparse file */
						get_bh(bh_result);
						memset(bh_result->b_data, 0, s->block_size);
					return 0;
				}
			} else o = e;
		}
	}
	return -EFBIG;
}
