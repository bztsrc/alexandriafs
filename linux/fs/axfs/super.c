// SPDX-License-Identifier: GPL-2.0-only
/*
 *  linux/fs/axfs/super.c
 *
 *  Copyright (C) 2023 bzt
 *
 *  AleXandriaFS support.
 */

#define AXFS_IMPLEMENTATION
#include "vfs.h"

enum {
	Opt_uid, Opt_gid, Opt_debug, Opt_err,
};

static const match_table_t axfs_tokens = {
	{Opt_uid, "uid=%d"},
	{Opt_gid, "gid=%d"},
	{Opt_debug, "debug"},
	{Opt_err, NULL}
};

static int axfs_parse_options(char *options, struct axfs_sb_info *sb)
{
	char *p;
	substring_t args[MAX_OPT_ARGS];
	int option;
	kuid_t uid;
	kgid_t gid;

	/* Initialize options */
	sb->uid = GLOBAL_ROOT_UID;
	sb->gid = GLOBAL_ROOT_GID;
	sb->debug = 0;

	if (!options)
		return 1;

	while ((p = strsep(&options, ",")) != NULL) {
		int token;

		if (!*p)
			continue;

		token = match_token(p, axfs_tokens, args);
		switch (token) {
		case Opt_uid:
			if (match_int(&args[0], &option))
				return 0;
			uid = INVALID_UID;
			if (option >= 0)
				uid = make_kuid(current_user_ns(), option);
			if (!uid_valid(uid)) {
				pr_err("Invalid uid %d, "
					   "using default\n", option);
				break;
			}
			sb->uid = uid;
			break;
		case Opt_gid:
			if (match_int(&args[0], &option))
				return 0;
			gid = INVALID_GID;
			if (option >= 0)
				gid = make_kgid(current_user_ns(), option);
			if (!gid_valid(gid)) {
				pr_err("Invalid gid %d, "
					   "using default\n", option);
				break;
			}
			sb->gid = gid;
			break;
		case Opt_debug:
			sb->debug = 1;
			break;
		default:
			pr_err("Unrecognized mount option \"%s\" "
				   "or missing value\n", p);
			return 0;
		}
	}
	return 1;
}

static int axfs_show_options(struct seq_file *m, struct dentry *root)
{
	struct axfs_sb_info *sb = (struct axfs_sb_info *)(root->d_sb->s_fs_info);

	if (uid_valid(sb->uid) && !uid_eq(sb->uid, GLOBAL_ROOT_UID))
		seq_printf(m, ",uid=%u",
			   from_kuid_munged(&init_user_ns, sb->uid));
	if (gid_valid(sb->gid) && !gid_eq(sb->gid, GLOBAL_ROOT_GID))
		seq_printf(m, ",gid=%u",
			   from_kgid_munged(&init_user_ns, sb->gid));
	if (sb->debug)
		seq_puts(m, ",debug");
	return 0;
}

static int axfs_statfs(struct dentry *dentry, struct kstatfs *buf)
{
	struct super_block *sb = dentry->d_sb;
	struct axfs_sb_info *s = (struct axfs_sb_info *)sb->s_fs_info;
	u64 id = huge_encode_dev(sb->s_bdev->bd_dev);

	buf->f_type = 0; memcpy(&buf->f_type, AXFS_SB_MAGIC, 4);
	buf->f_bsize = sb->s_blocksize;
	buf->f_blocks = s->num_blocks;
	buf->f_bfree = s->free_blocks;
	buf->f_bavail = buf->f_bfree;
	buf->f_files = 0;   /* UNKNOWN */
	buf->f_ffree = 0;   /* UNKNOWN */
	buf->f_fsid = u64_to_fsid(id);
	buf->f_namelen = AXFS_FILENAME_MAX;

	return 0;
}

static int axfs_remount(struct super_block *sb, int *flags, char *data)
{
	sync_filesystem(sb);
	if (!(*flags & SB_RDONLY))
		return -EINVAL;
	return 0;
}

static void axfs_put_super(struct super_block *sb)
{
	kfree(sb->s_fs_info);
	sb->s_fs_info = NULL;
}

static struct inode *axfs_nfs_get_inode(struct super_block *sb, uint64_t ino, uint32_t generation)
{
	(void)generation;
	return axfs_iget(sb, ino);
}

static struct dentry *axfs_fh_to_dentry(struct super_block *sb, struct fid *fid, int fh_len, int fh_type)
{
	return generic_fh_to_dentry(sb, fid, fh_len, fh_type, axfs_nfs_get_inode);
}

static struct dentry *axfs_fh_to_parent(struct super_block *sb, struct fid *fid, int fh_len, int fh_type)
{
	return generic_fh_to_parent(sb, fid, fh_len, fh_type, axfs_nfs_get_inode);
}

static struct dentry *axfs_get_parent(struct dentry *child)
{
/*
	struct inode *parent;
	struct axfs_inode_info *axfs_ino = container_of(d_inode(child), struct axfs_inode_info, vfs_inode);

	parent = befs_iget(child->d_sb, (unsigned long)axfs_ino->parent);
	if (IS_ERR(parent))
		return ERR_CAST(parent);

	return d_obtain_alias(parent);
*/
	(void)child;
	return ERR_PTR(-ENOENT);
}

static const struct super_operations axfs_sops = {
	.alloc_inode    = axfs_alloc_inode,
	.free_inode     = axfs_free_inode,
	.statfs         = axfs_statfs,
	.remount_fs     = axfs_remount,
	.put_super      = axfs_put_super,
	.show_options   = axfs_show_options,
};

static const struct export_operations axfs_export_operations = {
	.encode_fh  = generic_encode_ino32_fh,
	.fh_to_dentry   = axfs_fh_to_dentry,
	.fh_to_parent   = axfs_fh_to_parent,
	.get_parent = axfs_get_parent,
};

int axfs_fill_super(struct super_block *s, void *data, int silent)
{
	long ret = -EINVAL;
	int blocksize;
	struct axfs_sb_info *sb;
	struct buffer_head *bh;
	struct inode *root;
	axfs_super_t *super;
	axfs_inode_v_t *vol;
	axfs_inode_d_t *d;
	uint64_t lbn, off;

	s->s_fs_info = kzalloc(sizeof(struct axfs_sb_info), GFP_KERNEL);
	if (s->s_fs_info == NULL)
		goto unacquire_none;

	sb = (struct axfs_sb_info *)s->s_fs_info;
	if (!axfs_parse_options((char *) data, sb)) {
		if (!silent)
			pr_err("cannot parse mount options");
		goto unacquire_priv_sbp;
	}

	if (!sb_rdonly(s)) {
		pr_warn("No write support. Marking filesystem read-only");
		s->s_flags |= SB_RDONLY;
	}

	blocksize = sb_min_blocksize(s, 512);
	if (!blocksize) {
		if (!silent)
			pr_err("unable to set blocksize");
		goto unacquire_priv_sbp;
	}

	bh = sb_bread(s, 0);
	if (!bh) {
		if (!silent)
			pr_err("unable to read superblock");
		goto unacquire_priv_sbp;
	}

	super = (axfs_super_t*)bh->b_data;
	sb->block_size = 1 << (super->s_blksize + 9);
	sb->inode_size = sb->block_size >> super->s_inoshift;

	if(memcmp(bh->b_data + 4, AXFS_SB_MAGIC, 4) || memcmp(bh->b_data + 252, AXFS_SB_MAGIC, 4) ||
	  super->s_blksize > 11 || (sb->inode_size != 512 && sb->inode_size != 1024) ||
	  !super->s_volume_fid || !super->s_root_fid || !super->s_freeblk_fid ||
	  (!(super->s_flags & AXFS_SB_F_NOCRC) && super->s_chksum != crc32_le(0, bh->b_data + 4, 0xF4))) {
		if (!silent)
			pr_err("not a valid AleXandriaFS superblock");
		goto unacquire_bh;
	}

	sb->inode_shft = super->s_inoshift;
	sb->inode_mask = (1 << super->s_inoshift) - 1;
	sb->volume_fid = ftoh64(super->s_volume_fid);
	sb->root_fid = ftoh64(super->s_root_fid);
	sb->free_fid = ftoh64(super->s_freeblk_fid);
	sb->num_blocks = ftoh64(super->s_nblks);
	sb->flags = ftoh16(super->s_flags);

	brelse(bh);

	sb_set_blocksize(s, (ulong) sb->block_size);

	/* no need to put these in cache, we just need a few fields which we store in sb */
	lbn = sb->volume_fid >> sb->inode_shft;
	off = (sb->volume_fid & sb->inode_mask) * sb->inode_size;
	bh = sb_bread(s, lbn);
	if (!bh) {
		if (!silent)
			pr_err("unable to read volume inode");
		goto unacquire_bh;
	}
	vol = (axfs_inode_v_t*)(bh->b_data + off);
	sb->ninodes = ftoh64(vol->i_ninode);

	brelse(bh);

	lbn = sb->free_fid >> sb->inode_shft;
	off = (sb->free_fid & sb->inode_mask) * sb->inode_size;
	bh = sb_bread(s, lbn);
	if (!bh) {
		if (!silent)
			pr_err("unable to read free inode");
		goto unacquire_bh;
	}
	d = (axfs_inode_d_t*)(bh->b_data + off);
	sb->free_blocks = ftoh64(d->i_size) / sb->block_size;

	brelse(bh);

	s->s_magic = 0; memcpy(&s->s_magic, AXFS_SB_MAGIC, 4);
	s->s_op = &axfs_sops;
	s->s_export_op = &axfs_export_operations;
	s->s_time_min = 0;
	s->s_time_max = 0xffffffffffffll;
	root = axfs_iget(s, sb->root_fid);
	if (IS_ERR(root)) {
		ret = PTR_ERR(root);
		goto unacquire_priv_sbp;
	}
	s->s_root = d_make_root(root);
	if (!s->s_root) {
		if (!silent)
			pr_err("get root inode failed");
		goto unacquire_priv_sbp;
	}

	return 0;

unacquire_bh:
	brelse(bh);

unacquire_priv_sbp:
	kfree(s->s_fs_info);
	s->s_fs_info = NULL;

unacquire_none:
	return ret;
}
