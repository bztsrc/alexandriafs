// SPDX-License-Identifier: GPL-2.0-only
/*
 *  linux/fs/axfs/vfs.h
 *
 *  Copyright (C) 2023 bzt
 *
 *  AleXandriaFS support.
 */

#include <linux/module.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/stat.h>
#include <linux/buffer_head.h>
#include <linux/vfs.h>
#include <linux/parser.h>
#include <linux/namei.h>
#include <linux/sched.h>
#include <linux/cred.h>
#include <linux/exportfs.h>
#include <linux/seq_file.h>
#include <linux/blkdev.h>
#include <linux/crc32.h>
#include <asm/byteorder.h>

#define _STDINT_H 1
typedef __u8 uint8_t;
typedef __u16 uint16_t;
typedef __u32 uint32_t;
typedef __u64 uint64_t;
#include "axfs.h"

#define ftoh16(x) le16_to_cpu((__force __le16)x)
#define ftoh32(x) le32_to_cpu((__force __le32)x)
#define ftoh64(x) le64_to_cpu((__force __le64)x)
#define htof16(x) (__force __le16)cpu_to_le16(x)
#define htof32(x) (__force __le32)cpu_to_le32(x)
#define htof64(x) (__force __le64)cpu_to_le64(x)

struct axfs_sb_info {
	kgid_t gid;
	kuid_t uid;
	int debug;
	uint16_t flags;
	uint32_t block_size;
	uint32_t inode_size;
	uint64_t inode_mask;
	uint64_t inode_shft;
	uint64_t volume_fid;
	uint64_t root_fid;
	uint64_t free_fid;
	uint64_t ninodes;
	uint64_t num_blocks;
	uint64_t free_blocks;
};

struct axfs_inode_info {
	uint8_t type;
	uint8_t flags;
	uint8_t br[768];
	struct inode vfs_inode;
};

/* inode.c */
int __init axfs_init_inodecache(void);
void axfs_destroy_inodecache(void);
struct inode *axfs_alloc_inode(struct super_block *sb);
void axfs_free_inode(struct inode *inode);
struct inode *axfs_iget(struct super_block *sb, unsigned long ino);

/* mapping.c */
int axfs_get_block(struct inode *inode, sector_t block, struct buffer_head *bh_result, int create);

/* dir.c */
struct dentry *axfs_lookup(struct inode *dir, struct dentry *dentry, unsigned int flags);
int axfs_readdir(struct file *file, struct dir_context *ctx);

/* super.c */
int axfs_fill_super(struct super_block *s, void *data, int silent);
