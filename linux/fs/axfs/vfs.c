// SPDX-License-Identifier: GPL-2.0-only
/*
 *  linux/fs/axfs/vfs.c
 *
 *  Copyright (C) 2023 bzt
 *
 *  AleXandriaFS support.
 */

#include "vfs.h"

MODULE_DESCRIPTION("AleXandria File System driver");
MODULE_AUTHOR("bzt");
MODULE_LICENSE("GPL");
MODULE_SOFTDEP("pre: crc32");

static struct dentry *axfs_mount(struct file_system_type *fs_type,
	int flags, const char *dev_name, void *data)
{
	return mount_bdev(fs_type, flags, dev_name, data, axfs_fill_super);
}

static struct file_system_type axfs_type = {
	.owner      = THIS_MODULE,
	.name       = "axfs",
	.mount      = axfs_mount,
	.kill_sb    = kill_block_super,
	.fs_flags   = FS_REQUIRES_DEV,
};
MODULE_ALIAS_FS("axfs");

static int __init init_axfs(void)
{
	int err;

	err = axfs_init_inodecache();
	if (err)
		goto unacquire_none;

	err = register_filesystem(&axfs_type);
	if (err)
		goto unacquire_inodecache;

	return 0;

unacquire_inodecache:
	axfs_destroy_inodecache();

unacquire_none:
	return err;
}

static void __exit exit_axfs(void)
{
	axfs_destroy_inodecache();

	unregister_filesystem(&axfs_type);
}

module_init(init_axfs)
module_exit(exit_axfs)

