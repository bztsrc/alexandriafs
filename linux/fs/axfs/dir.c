// SPDX-License-Identifier: GPL-2.0-only
/*
 *  linux/fs/axfs/dir.c
 *
 *  Copyright (C) 2023 bzt
 *
 *  AleXandriaFS support.
 */

#include "vfs.h"

static uint64_t axfs_get_dirent(struct inode *dir, struct buffer_head **bh, uint64_t bs, uint64_t offs, axfs_dirent_t *ent)
{
	/* the maximum length of a compressed entry is 256 bytes */
	axfs_extent_t ext;
	uint64_t rem;
	uint8_t *d, *s, *n, tmp[256];

	memset(ent, 0, sizeof(axfs_dirent_t));
	if(!*bh && axfs_get_block(dir, offs / bs, *bh, 0))
		return 0;
	d = (*bh)->b_data + (offs % bs);
	/* move beyond header */
	if(!offs) {
		if(!memcmp(d, AXFS_DR_MAGIC, 4)) {
			brelse(*bh);
			return 0;
		}
		offs += sizeof(axfs_dirhdr_t);
		d += sizeof(axfs_dirhdr_t);
	}
	/* if entry crosses a block boundary */
	if((offs / bs) != ((offs + sizeof(tmp)) / bs)) {
		rem = (offs % bs);
		memcpy(tmp, d, bs - rem);
		brelse(*bh);
		if(axfs_get_block(dir, offs / bs + 1, *bh, 0))
			return 0;
		memcpy(tmp + rem, (*bh)->b_data, sizeof(tmp) - rem);
		d = tmp;
	}
	/* read in one entry */
	if(*d) {
		s = d; d = axfs_unpack(d, &ext); for(n = d; *d; d++, ent->d_nlen++){}; d++;
		ent->d_fid = ext.e_blk;
		memcpy(ent->d_name, n, ent->d_nlen);
		offs += d - s;
	} else {
		/* end of directory list */
		offs = 0;
	}
	brelse(*bh);
	return offs;
}

struct dentry *axfs_lookup(struct inode *dir, struct dentry *dentry, unsigned int flags)
{
	struct inode *inode;
	struct super_block *sb = dir->i_sb;
	struct axfs_sb_info *s = (struct axfs_sb_info *)sb->s_fs_info;
	struct buffer_head *bh = NULL;
	axfs_dirent_t ent;
	uint64_t offs = 0;

	while(offs < dir->i_size) {
		if(!(offs = axfs_get_dirent(dir, &bh, s->block_size, offs, &ent)))
			break;
		/* d_fid is zero for deleted records */
		if(ent.d_fid && !memcmp(ent.d_name, dentry->d_name.name, ent.d_nlen) && !dentry->d_name.name[ent.d_nlen]) {
			inode = axfs_iget(sb, (ino_t) ent.d_fid);
			return d_splice_alias(inode, dentry);
		}
	}
	return NULL;
}

int axfs_readdir(struct file *file, struct dir_context *ctx)
{
	struct inode *dir = file_inode(file);
	struct super_block *sb = dir->i_sb;
	struct axfs_sb_info *s = (struct axfs_sb_info *)sb->s_fs_info;
	struct buffer_head *bh = NULL;
	axfs_dirent_t ent;

	while(ctx->pos < dir->i_size) {
		if(!(ctx->pos = axfs_get_dirent(dir, &bh, s->block_size, ctx->pos, &ent)))
			break;
		if(ent.d_fid && !dir_emit(ctx, ent.d_name, ent.d_nlen, (ino_t) ent.d_fid, DT_UNKNOWN))
			break;
	}
	return 0;
}
