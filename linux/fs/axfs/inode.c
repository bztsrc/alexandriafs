// SPDX-License-Identifier: GPL-2.0-only
/*
 *  linux/fs/axfs/inode.c
 *
 *  Copyright (C) 2023 bzt
 *
 *  AleXandriaFS support.
 */

#include "vfs.h"

static struct kmem_cache* axfs_inode_cachep;

static void init_once(void *foo)
{
	struct axfs_inode_info *i = (struct axfs_inode_info *) foo;

	inode_init_once(&i->vfs_inode);
}

int __init axfs_init_inodecache(void)
{
	axfs_inode_cachep = kmem_cache_create("axfs_inode_cache",
						 sizeof(struct axfs_inode_info),
						 0, (SLAB_RECLAIM_ACCOUNT|
						 SLAB_MEM_SPREAD|SLAB_ACCOUNT),
						 init_once);
	if (axfs_inode_cachep == NULL)
		return -ENOMEM;

	return 0;
}

void axfs_destroy_inodecache(void)
{
	rcu_barrier();
	kmem_cache_destroy(axfs_inode_cachep);
}

struct inode *axfs_alloc_inode(struct super_block *sb)
{
	struct axfs_inode_info *i;

	i = alloc_inode_sb(sb, axfs_inode_cachep, GFP_KERNEL);
	if (!i)
		return NULL;
	return &i->vfs_inode;
}

void axfs_free_inode(struct inode *inode)
{
	kmem_cache_free(axfs_inode_cachep, container_of(inode, struct axfs_inode_info, vfs_inode));
}

static int axfs_read_folio(struct file *file, struct folio *folio)
{
	return block_read_full_folio(folio, axfs_get_block);
}

static sector_t axfs_bmap(struct address_space *mapping, sector_t block)
{
	return generic_block_bmap(mapping, block, axfs_get_block);
}

static const struct address_space_operations axfs_aops = {
	.read_folio = axfs_read_folio,
	.bmap       = axfs_bmap,
};

static const struct file_operations axfs_dir_operations = {
	.read       = generic_read_dir,
	.iterate_shared = axfs_readdir,
	.llseek     = generic_file_llseek,
};

static const struct inode_operations axfs_dir_inode_operations = {
	.lookup     = axfs_lookup,
};

struct inode *axfs_iget(struct super_block *sb, unsigned long ino)
{
	struct axfs_sb_info *s = (struct axfs_sb_info *)sb->s_fs_info;
	struct buffer_head *bh;
	uint64_t lbn, off;
	axfs_inode_t *inod;
	struct axfs_inode_info *axfs_ino;
	struct inode *inode;

	inode = iget_locked(sb, ino);
	if (!inode)
		return ERR_PTR(-ENOMEM);
	if (!(inode->i_state & I_NEW))
		return inode;

	axfs_ino = container_of(inode, struct axfs_inode_info, vfs_inode);

	lbn = inode->i_ino >> s->inode_shft;
	off = (inode->i_ino & s->inode_mask) * s->inode_size;
	bh = sb_bread(sb, lbn);
	if (!bh) {
		/*printk(KERN_ERR "unable to read inode block %lu", lbn);*/
		goto unacquire_none;
	}

	inod = (axfs_inode_t*)(bh->b_data + off);
	if(memcmp(bh->b_data, AXFS_IN_MAGIC, 4) ||
	  (inod->i_type != AXFS_FT_DIR && inod->i_type != AXFS_FT_LNK && inod->i_type < AXFS_FT_APPL) || inod->i_type & 0x80 ||
	  (!(s->flags & AXFS_SB_F_NOCRC) && inod->i_chksum != crc32_le(0, bh->b_data + 8, s->inode_size - 8))) {
		/*printk(KERN_ERR "not a valid inode %lx", inode->i_no);*/
		goto unacquire_bh;
	}

	inode->i_mode = (umode_t) (inod->i_owner.access & 7) |
		(inod->i_type == AXFS_FT_DIR ? S_IFDIR : (inod->i_type == AXFS_FT_LNK ? S_IFLNK : S_IFREG));

	inode->i_uid = s->uid;
	inode->i_gid = s->gid;

	set_nlink(inode, ftoh64(inod->i_nlinks));

	inode_set_mtime(inode, ftoh64(inod->i_mtime) / 1000000, 0);
	inode_set_ctime(inode, ftoh64(inod->i_ctime) / 1000000, 0);
	inode_set_atime_to_ts(inode, inode_get_mtime(inode));

	axfs_ino->type = inod->i_type;
	axfs_ino->flags = inod->i_flags;
	memset(axfs_ino->br, 0, 768);
	memcpy(axfs_ino->br, inod->i_d, s->inode_size - offsetof(axfs_inode_t, i_d));

	inode->i_size = ftoh64(inod->i_size);
	inode->i_mapping->a_ops = &axfs_aops;

	if (S_ISREG(inode->i_mode)) {
		inode->i_fop = &generic_ro_fops;
	} else if (S_ISDIR(inode->i_mode)) {
		inode->i_op = &axfs_dir_inode_operations;
		inode->i_fop = &axfs_dir_operations;
	} else if (S_ISLNK(inode->i_mode)) {
		inode->i_link = axfs_ino->br;
		inode->i_op = &simple_symlink_inode_operations;
	}

	brelse(bh);
	unlock_new_inode(inode);
	return inode;

unacquire_bh:
	brelse(bh);

unacquire_none:
	iget_failed(inode);
	return ERR_PTR(-EIO);
}

