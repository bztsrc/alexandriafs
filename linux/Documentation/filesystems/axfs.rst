.. SPDX-License-Identifier: GPL-2.0-only

======================
AleXandriaFS for Linux
======================

Disclaimer
==========
The AleXandriaFS is designed for modern storage devices without any moving
parts (SSDs, SD cards, USB sticks, etc.), provides all the features you'd
expect from an enterprise-grade file system (write cycle protection, access
control lists, extended attributes, journaling, encryption etc.) and it is
very efficient, flexible, and very performant. Its goal is to use as small
portion of the storage as possible for its meta data. Supports disks up to
several Zettabytes.

How to Install
==============
step 1.  Copy the AXFS  files into the source code tree of linux.

Assuming that your kernel source is in /foo/bar/linux and you have downloaded
the repo to /path/to/alexandriafs, you would do the following:

	cd /path/to/alexandriafs
	cp -r linux/fs /foo/bar/linux/fs
	cp axfs.h /foo/bar/linux/fs/axfs

Add the following line to /foo/bar/linux/fs/Kconfig

	        default m if EXT2_FS_XATTR || EXT4_FS
	+source "fs/axfs/Kconfig"
	 source "fs/jfs/Kconfig"

after the ext2 options (probably before the source jfs line). Then add the
following line as well to /foo/bar/linux/fs/Makefile

	 obj-$(CONFIG_EXT2_FS)          += ext2/
	+obj-$(CONFIG_AXFS_FS)          += axfs/
	 obj-$(CONFIG_JBD2)             += jbd2/

again, after the line with ext2 in it.

step 2.  Configuration & make kernel

To use the AleXandriaFS module, you must enable it at configure time::

	cd /foo/bar/linux
	make menuconfig (or xconfig)

Under the "File systems" menu will be an option called "AleXandriaFS support".
Enable that option (it is fine to make it a module).

Using AleXandria
================
To use the AleXandria File System, use filesystem type 'axfs'.

ex::

    mount -t axfs /dev/sda1 /mnt

Mount Options
=============

=============  ===========================================================
uid=nnn        All files in the partition will be owned by user id nnn.
gid=nnn	       All files in the partition will be in group nnn.
debug          The driver will output debugging information to the syslog.
=============  ===========================================================

How to Get Latest Version
=========================
The latest version is currently available at:
<https://gitlab.com/bztsrc/alexandriafs/>
