The AleXandria File System Supporting Library
=============================================

[[_TOC_]]

Preface
-------

AleXandriaFS comes with an stb-style single header library written in ANSI C to provide the algorithms and high-level API for
accessing files on an AleXandriaFS formatted volume.

To use, in exactly one of your source files define `AXFS_IMPLEMENTATION`, provide a variable and two functions and include
`libaxfs.h`. You'll also need `axfs.h` which contains the low-level on disk format structures and defines.

```c
int dev_read(void *dev, uint64_t off, void *buf, uint32_t size);
int dev_write(void *dev, uint64_t off, void *buf, uint32_t size);
uint64_t dev_now_msec;

#define AXFS_IMPLEMENTATION
#include "libaxfs.h"
```

The `dev_read` function is responsible for loading data from the underlyig device. Likewise `dev_write` should write to that
device. The `dev` argument can be anything, only used by these two. For both, `off` and `size` are in bytes, however they always
be passed as the multiple of block size. When partitions are used, then it is the caller's duty to add the partition's offset to
`off` for both. The library doesn't care about partitions, so this way you can use it with whatever partitioning or slicing scheme
you want (the tools provided with AleXandriaFS use the GPT scheme btw.).

The `dev_now_msec` variable should contain the current timestamp in microseconds and in UTC (eg. `time(NULL) * 1000000`). It is
the caller's duty to keep this variable up to date before each axfs function call. The library itself does no time related
libc calls, this way you can use it with whatever time source you'd like (for example `gettimeofday` or `clock_gettime`, just
make sure the result is without time zones and daylight saving, aka. UTC).

Normally `libaxfs.h` depends on libc only, and it allocates memory on its own. You can turn this off with the `AXFS_NOMALLOC`
define. In this case the biggest block size that the library can handle is 4096 bytes, but in return there we'll be no dynamic
memory allocation at all, just static memory management. You can totally ommit libc in this case, you just have to provide the
`memset`, `memcpy` and `memcmp` triumvirate (compilers should have built-in versions of these).

By default, the directory separator is `/` which you can change with the `AXFS_DIRSEP` define. This must be a one byte long ASCII
character literal (UTF-8 multi-bytes not allowed as separator).

No linkage needed, just this include alone is enough.

Public API
----------

The header provides two typedefs, `axfs_ctx_t` and `axfs_file_t`. The first is a common context, used by all functions. It
has to be initialized with `axfs_mount`, and must be freed with `axfs_umount`. The other structure is used by the file functions
(pretty much like `FILE*`) and must be initialized with `axfs_open` and freed with `axfs_close`. While you'll need one context per
volume, but you can have as many file structures as you want.

### Creating a file system

```c
int axfs_mkfs(void *dev, uint32_t blksize, uint32_t inoshift, uint64_t numblks, uint32_t nresblk, uint8_t *uuid, uint32_t jblks,
    uint32_t wrprot, uint32_t nbad, uint64_t *bad, const char *passphrase);
```

Creates the file system using quick format.

| Argument        | Description                                                                                |
|-----------------|--------------------------------------------------------------------------------------------|
| `dev`           | could be anything, not used, just passed around to the dev_read and dev_write functions    |
| `blksize`       | the block size in bits minus 9 (0: min 512 bytes, default 3: 4096 bytes, 7: 65536 bytes)   |
| `inoshift`      | how much to shift block size to the right to get inode size (default 3, see below)         |
| `numblks`       | total number of blocks on the volume                                                       |
| `nresblk`       | number of blocks reserved for the boot loader (default 0, no boot loader)                  |
| `uuid`          | unique identifier of the volume (should be random)                                         |
| `jblks`         | journal buffer's size in blocks (0: no journaling, -1: calculate as 1% min 64, max 256)    |
| `wrprot`        | write cycle protection, number of writes allowed before relocating inodes (default 0, off) |
| `nbad`          | number of bad blocks (default 0, none)                                                     |
| `bad`           | array of block numbers with `nbad` elements, list of bad blocks (default NULL, none)       |
| `passphrase`    | encrypt volume using this passphrase (default NULL, no encryption)                         |

The `inoshift` parameter can be calculated from an `inodesize` in bytes as
```c
for(inoshift = 0; (inodesize << inoshift) != (1 << (blksize + 9)); inoshift++);
```
One inode's size can't be bigger than the block size, and it must be 256 bytes at a minimum, otherwise `axfs_mkfs` will return 0.

On success, 1 is returned.

### Mounting / umounting

```c
int axfs_mount(axfs_ctx_t *ctx, void *dev, const char *passphrase);
```

Initializes `ctx`. The `dev` parameter could be anything, not used, just passed to the dev_read and dev_write functions. If the
volume is encrypted, then `passphrase` must be given, otherwise NULL or empty string. On success returns 1, 0 on error.

```c
int axfs_umount(axfs_ctx_t *ctx);
```

Destructs `ctx`. On success returns 1, 0 on error.

### Changing encryption passphrase

```c
int axfs_rekey(axfs_ctx_t *ctx, const char *oldpass, const char *newpass);
```

For encrypted volumes, this changes the passphrase from `oldpass` to `newpass` without re-encoding the entire volume. Both has to
be a zero terminated UTF-8 string, and `ctx` must be mounted. On success returns 1, 0 on error.

### Path lookup

```c
uint64_t axfs_lookup_fid(axfs_ctx_t *ctx, const char *path);
```

Returns the fid for a path, or 0 on error. Can be used as a "does file exists?" check, otherwise you won't need the fid with the
high-level API.

### Deleting files and directories

```c
int axfs_remove(axfs_ctx_t *ctx, const char *path);
```

Removes a file from the directory hierarchy, and if the number of links decreases to zero in its inode, then frees the file's
content and the inode too. There's no rmdir(), this works for directories too but only empty directories can be removed. Returns
1 on success, 0 on error.

### Creating device files

```c
int axfs_mknod(axfs_ctx_t *ctx, const char *path, uint64_t mode, uint64_t dev_major, uint64_t dev_minor);
```

Creates a device file. Returns 1 on success, 0 on error.

### Creating named pipes

```c
int axfs_mkfifo(axfs_ctx_t *ctx, const char *path, uint64_t mode);
```

Creates a named pipe. Returns 1 on success, 0 on error.

### Creating directory

```c
int axfs_mkdir(axfs_ctx_t *ctx, const char *path);
```

Creates a directory. Returns 1 on success, 0 on error.

### Creating search directory

```c
int axfs_mksdr(axfs_ctx_t *ctx, const char *path, const char *search);
```

Creates a live directory, that is, always presents up-to-date search results. Returns 1 on success, 0 on error.


### Creating union

```c
int axfs_mkuni(axfs_ctx_t *ctx, const char *path, const char **targets);
```

Creates a directory union. The `targets` argument must be an array of zero terminated UTF-8 strings, the last element must be NULL,
terminating the list. Returns 1 on success, 0 on error.

### Creating symbolic link

```c
int axfs_symlink(axfs_ctx_t *ctx, const char *path, const char *target);
```

Creates a symbolic link. Returns 1 on success, 0 on error.

### Get symlink target

```c
int axfs_readlink(axfs_ctx_t *ctx, const char *path, char *outbuf, uint32_t outlen);
```

Returns the symlink's target in `outbuf`, at most `outlen` - 1 (for the terminator). Returns 1 on success, 0 on error.

### Opening files

```c
int axfs_open(axfs_file_t *file, axfs_ctx_t *ctx, const char *path, int create);
```

Opens a file for reading and writing. If the file doesn't exists, and `create` is non-zero then it is created, otherwise 0
returned.  The `file` structure must be allocated by the caller, and it may use as many of these as it pleases. Returns 1 on
success, 0 on error.

### Positioning in opened files

```c
uint64_t axfs_seek(axfs_file_t *file, int64_t offset, int whence);
```

Sets the current file offset pointer. If `whence` is 0 (`SEEK_SET`), then from the beginning of the file, if 1 (`SEEK_CUR`), then
`offset` is added to the current file offset, and if 2 (`SEEK_END`), then it is added to the end of the file. Returns the current
offset.

### Reading from opened files

```c
uint64_t axfs_read(axfs_file_t *file, void *buffer, uint64_t size);
```

Reads in `size` bytes at current offset from the opened `file` into `buffer`. Returns the number of bytes actually read.

### Writing to opened files

```c
uint64_t axfs_write(axfs_file_t *file, void *buffer, uint64_t size);
```

Writes `size` bytes in `buffer` to current offset at the opened `file`. Returns the number of bytes actually written.

### Closing opened files

```c
int axfs_close(axfs_file_t *file);
```

Closes an opened file. Returns 1 on success, 0 on error.

### Traversing directory

```c
int axfs_readdir(axfs_file_t *file, axfs_dirent_t *ent);
```

Reads in the next directory entry into `ent` and returns 1. On error or when the end of the directory reached, returns 0.
Note, there's no opendir() / closedir(), this function uses `axfs_open` and `axfs_close` just like the regular files. Seeking
to the beginning of the opened file does exactly the same what rewinddir() would do.

### Truncate file size

```c
int axfs_truncate(axfs_file_t *file, uint64_t offs);
```

Sets the inode's data stream's size to `offs`, dropping everything after that.

### Set permissions

```c
int axfs_chowner(axfs_file_t *file, axfs_access_t *acl);
```

Set the inode's owner and their access bits.

```c
int axfs_chgroup(axfs_file_t *file, axfs_access_t *acl);
```

Set the inode's group and their access bits.

```c
int axfs_chother(axfs_file_t *file, axfs_access_t *acl);
```

Set the inode's other groups and their access bits.

### Basic attributes

```c
int axfs_setattr(axfs_file_t *file, uint8_t type, const char *mime, const char *icon);
```

Sets a file inode's file type to `type` (see the `AXFS_FT_*` defines). Works only for regular files and directories. For files,
arguments `mime` and `icon` can be NULL, and only supported when extended attributes are enabled on the volume.

For directories, `mime` should point to a 8 bytes long uint8_t array, the GUI attributes. This works even when extended
attributes are disabled. The `icon` argument can be NULL, and only supported when extended attributes are enabled.

Returns 1 on success, 0 on error.

```c
int axfs_getattr(axfs_file_t *file, axfs_stat_t *st, uint32_t outlen);
```

Fills in `st` with the inode's basic attributes and returns 1, or 0 on error. If extended attributes are enabled and `outlen` is
bigger than the stat structure's size then might return 3 zero terminated UTF-8 strings right after the struct (pointed by the
fields in the struct):
```c
uint64_t        st_ino;     /* fid, inode number */
uint64_t        st_size;    /* apparent file size on 80 bits */
uint16_t        st_size_hi;
uint16_t        st_type;    /* st_mode most significant bits, sort of AXFS_FT_x */
uint32_t        st_blksize; /* block size in bytes */
uint64_t        st_blocks;  /* allocated blocks for this file, in st_blksize */
uint64_t        st_nlink;   /* number of hard links */
uint64_t        st_btime;   /* birth (creation) time, microseconds since EPOCH */
uint64_t        st_ctime;   /* change time */
uint64_t        st_mtime;   /* modification time */
uint8_t         st_gui[8];  /* directory GUI attributes */
char            *st_mime;   /* points right after the struct, might be NULL */
char            *st_icon;   /* points right after the struct, might be NULL */
char            *st_path;   /* points right after the struct, might be NULL */
axfs_access_t   st_owner;   /* st_mode + st_uid */
axfs_access_t   st_acl[10]; /* st_mode + multiple st_gids */
/* mime type string */
/* icon file string */
/* path string */
```

### Extended attributes

```c
int axfs_setxattr(axfs_file_t *file, const char *key, const char *value);
```

Set `value` for `key`. If `value` is NULL or empty string, then it removes the extended attribute.

```c
int axfs_getxattr(axfs_file_t *file, const char *key, char *outval, uint32_t outlen);
```

Get the value in `outval` buffer of `outlen` size for `key`. Returns the length of the value on success, 0 otherwise.

```c
int axfs_listxattr(axfs_file_t *file, char *outval, uint32_t outlen);
```

List all extended attribute keys of an inode into `outval` buffer. Each key will be zero terminated. Returns the total length
of keys on success, 0 otherwise.

