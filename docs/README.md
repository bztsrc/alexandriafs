The AleXandria File System Specification
========================================

[[_TOC_]]

Preface
-------

AleXandriaFS got its name after the famous ancient library, which also had to store huge amount of data in a well-organized way;
also a gentle reminder that you absolutely have to keep a backup copy of your most precious files. This file system was designed
for modern storage devices without any moving parts (SSDs, SD cards, USB sticks, etc.) with the goal of being flexible, support
enourmous capacity, and also be error-tolerant. The second most important goal of AXFS is to use as small space for its meta data
as possible, leaving the most storage for meaningful data.

This can't be done without compromises, so to achieve its goal, AXFS made some limitations, its data structures and algorithms are
a bit complex (but still much simpler than the competition's). The biggest limitation is, that for improved indexing, the size of a
directory is limited in 4 billion bytes (still tens of millions entries, in practice no other file systems can handle several 100
thousands of entries efficiently), and a file name can't be longer than 246 bytes (a little bit smaller than the competition's, but
trust me, more than enough).

Even with these limitations AXFS is way ahead of its competition, it provides all of their features (or even more), while using
far less space for its meta data and relying on faster and simpler algorithms. In every day life AXFS seems truly unlimited and
scales very well.

| Property                      | Biggest allowed                                                                                |
|-------------------------------|------------------------------------------------------------------------------------------------|
| logical block size            | 2^264 (but that's not practical. The default is 4k, 64k being a more likely limit)             |
| one volume size               | 2^64 blocks (with 4k blocks it's 2^76 bytes, 64 Zetta, or ten thousands of million Terrabytes) |
| one file                      | 2^80 bytes (also depends on block size and the actual volume capacity)                         |
| longest file name             | 246 bytes (less in characters, because some UTF-8 characters are multi-byte)                   |
| O(log2) access directories    | 2^32 bytes (ca. 16 million entries, if all file names are 246 bytes, more if they are shorter) |
| entries in a directory        | 2^32 (less than 4 billion, because unique names force file names to grow)                      |
| blocks in one extent          | 2^64 - 1                                                                                       |
| number of attributes          | 2^32 in total, one compressed block (with 4k blocks about 500 attributes) per inode            |

File system specifications talk about maximum sizes, but they often neglect to mention minimum sizes, however those are just as
equally important. To understand what "AXFS is using far less space for meta data" actually means, here's a comparision (smaller
the better):

| Description                             | AXFS | FAT32 | NTFS | ext4   | btrfs  | XFS    | Minix3 |
|-----------------------------------------|-----:|------:|-----:|-------:|-------:|-------:|-------:|
| Minimum volume size (KiB)               |   32 | 33792 | 7168 |    128 | 111616 | 307200 |     32 |
| Meta data size on smallest volume (KiB) |    4 |   257 | 2500 |     31 |  82944 |  85840 |      8 |
| Meta data size on a 64 MiB volume (KiB) |    4 |  1025 | 2504 |  14131 |      - |      - |   1380 |
| Meta data size on a 1 GiB volume (KiB)  |    4 |  2072 | 5724 | 120888 | 122240 | 117464 |   8339 |

Measurements were performed on a Linux box, creating an empty volume on a loop back device, doubling its size until the
corresponding mkfs with default options did not fail (except when the mkfs tool reported the required minimum size). Then `df`
was used and its "Available" coloumn substracted from the image file's size to determine the space used for meta data on an empty
volume.

Note that AXFS is the only file system which did not increase the size of its meta data as the volume size grew. These numbers
are without a journal and for the default 4096 bytes block and 512 bytes inode size, however you could configure it with 512 bytes
block and 256 bytes inode size too. In this case the [smallest AXFS volume](#smallest-volume-example) is 4 KiB requiring 1 KiB of
meta data space.

On Disk Layout
--------------

Global considerations:

- All numbers in AXFS structures are little-endian.
- All structure fields are aligned properly according to their sizes, except in a compressed block run list.
- The cyclic checksum uses the ANSI method, CCITT32 CRC with 0xedb88320 polynom (same as for GPT or gzip).
- Timestamps are stored on 64 bits, in *microseconds* (millionth of a second) passed since UNIX EPOCH (Jan 1. 1970 mignight UTC).

```
  0        1                                                         last
  +--------+--------+--------+--------+--------+--------+ ... +-------+
  |   sb   |               ...meta and data blocks...                 |
  +--------+--------+--------+--------+--------+--------+ ... +-------+
```
The AleXandriaFS organizes the disk in blocks, so each unit above is a block. This is independent to the underlying storage's
actual sector size, it's a logical block. As you can see, AXFS is extremely flexible, the superblock must reside in the first
block otherwise there are no structures at fixed positions nor fixed sized tables at all, not even the number of files is limited
like with other file systems. The volume can be resized and expanded even on-the-fly.

With all that in mind, the most likely layout going to be:
```
  0        1        2        3                                       last
  +--------+--------+--------+--------+--------+--------+ ... +-------+
  |   sb   | inoblk | root   |        ...meta and data blocks...      |
  +--------+--------+--------+--------+--------+--------+ ... +-------+
```
Second is an inode block, and the third is the root directory. When a disk (or partition) is formatted, only these three blocks
are written, no other operations are necessary, so it is fast (quick format).

When the file system is located on a GPT partition then the partition's type could be anything, and should reflect what that
partition is used for (for storing applications, user data, etc.). In case this isn't known then the PartitionTypeGUID field can
be set to the jolly joker `58454C41-0100-5346-0000000000000000` UUID to indicate that the partition is formatted with AXFS.
On the other hand the partition's UniquePartitionGUID *must match* with the UUID in the superblock.

Superblock
----------

| Offset  | Size  | Description                                                    |
|--------:|------:|----------------------------------------------------------------|
|       0 |     4 | not used, reserved for the boot loader                         |
|       4 |     4 | `s_magic` magic bytes to identify, `"AXFS"`                    |
|       8 |     1 | `s_major` file system major version, `1`                       |
|       9 |     1 | `s_minor` file system minor version, `0`                       |
|      10 |     1 | `s_blksize` logical block's size                               |
|      11 |     1 | `s_inoshift` number of inode shift bits (0 to 12)              |
|      12 |     2 | `s_flags` optional file system feature flag bits               |
|      14 |     1 | `s_nresblks` number of reserved blocks for the boot loader     |
|      15 |     1 | `s_enctype` encryption algorithm                               |
|      16 |    28 | `s_encrypt` encryption mask                                    |
|      44 |     4 | `s_enchash` encryption password CRC                            |
|      48 |    16 | `s_uuid` volume unique identifier (must match with GPT)        |
|      64 |     8 | `s_nblks` total number of blocks on this volume                |
|      72 |     8 | `s_volume_fid` address of volume inode                         |
|      80 |     8 | `s_root_fid` address of root directory inode                   |
|      88 |     8 | `s_freeblk_fid` address of free blocks inode                   |
|      96 |     8 | `s_journal_fid` address of journal inode                       |
|     104 |     8 | `s_attridx_fid` address of attribute index inode               |
|     112 |     8 | `s_uquota_fid` address of user quota inode                     |
|     120 |   120 | `s_reserved` reserved, must be zeros                           |
|     240 |     8 | `s_badblk_fid` address of bad blocks inode                     |
|     248 |     4 | `s_chksum` CRC checksum of bytes 4 to 248                      |
|     252 |     4 | `s_magic2` magic bytes to identify, `"AXFS"`                   |
|     256 |     x | optionally boot loader program continues                       |

The superblock always starts at the byte offset 0 of the disk and it is 256 bytes long (independently to the block size).

Some firmware expects a boot loader at the beginning of the disk, for those the loaders must be written in a way to include the
superblock. To resolve conflicts with these, the AXFS superblock starts with a 4 byte `s_loader` field, which isn't used at all.
When needed, a boot loader can place a jump instruction here to jump over the superblock and continue execution after the data.
The number of blocks reserved for the boot loader is stored in `s_nresblks`, and the first usable block comes after the boot loader.
It worth noting that most disks are partitioned, meaning that those boot loaders must share their code with the partitioning table
and not with the superblock of a partition.

Since it's typical for modern storages that they have limited write cycles and the position of the superblock is fixed, therefore
this superblock was designed in a way that no writes should happen during normal operation, only reads. This is unlike other file
systems, which write the superblock constantly and all the time.

### The block addressing (LBN)

Even though disks store bytes, it would be very inefficient to address each byte individually. Therefore the smallest unit on
a file system is a logical block and a volume has `s_nblks` of those.

The size of this is stored in the `s_blksize` field, in power of two minus 9. So to get the block size in bytes, we need
`block_size = 2 ^ (s_blksize + 9)`, or in C syntax `1 << (s_blksize + 9)`. This means that the smallest unit you can use is
512 bytes (s_blksize = 0). This logical block number is totally independent to the physical sector size, however for performance
reasons it should be a multiple of that. For this reason the most common logical block size is 4096 bytes (s_blksize = 3), but
on servers with huge files 65536 bytes (s_blksize = 7) or 1 megabyte (s_blksize = 11) also likely. In practice the block size
is limited by the file system driver implementations (maybe at 65536 bytes), and not by the file system's design.

Modern storage devices have no moving heads, meaning there's no time penalty for seeking. Accessing the Nth sector and moving to
the next one takes the same time as accessing the first sector and moving to the very last (read as: the difference is within a
reasonable error margin, therefore insignificant and negligable). For file system design, this means there's no need for cylinders,
segments, nor for block groups. This simplifies the overall structure greatly, allowing the use of a flat addressing model in
general without concering about data locality.

In a flat model each block has a continuous block number, the Logical Block Number (LBN in short), so it's easy to convert into
byte offset with the `LBN * block_size` expression. All block numbers are 64 bits wide, so the total capacity of a volume in bytes
is `2 ^ (64 + 9 + s_blksize)`.

### The inode addressing (fid)

Most UNIX based file systems have a fixed sized table (or more fixed tables) at a fixed position with the inodes, and they're
indexing this by the element index. In contrast in AXFS it is crucial not to have any fixed positions and not to store meta data
in fixed size tables, therefore it uses a totally different approach.

The inode addresses are 64 bits, but they are splitted into two parts. The least significant `s_inoshift` bits store an index,
and the most significant bits above that store an LBN.

```
             63                 s_inoshift   0          bits
              +----------------------+-------+
              |          LBN         | index |          fid (64 bit)
              +----------------------+-------+
                            :           :
                            v           :
+========+    +========+    +========+  : +========+
| block  |    | block  |    | inode  |  : | block  |
|        |    |        |    |--------|  : |        |
|        |    |        |    | inode  |<-' |        |    logical
|        |    |        |    |--------|    |        |    blocks
|        |    |        |    | inode  |    |        |
|        |    |        |    |--------|    |        |
|        |    |        |    | inode  |    |        |
+========+    +========+    +========+    +========+
```

The AXFS fields that store such inode addresses are called file identifiers (or fid in short). To get the pointed inode, first
we need the block which stores that inode. For this we need to shift the fid to the right: `LBN = fid >> s_inoshift`. To get
the index inside the block, we need to mask using the biggest index possible: `index = fid & ((1 << s_inoshift) - 1)`. Multiplying
this index by the size of an inode gives the byte offset into that block where our inode structure is located.

The `s_inoshift` specifies the biggest index number, and by that, implicitly the inode's size as well. If `s_inoshift` is zero,
then the inode occupies a whole block, so inode size equals to block size. For example if `s_inoshift` is 2 bits, then we can
have four different indeces (from 0 to 3), so here inode's size is one quater of a block's size. Generally speaking,
`inode_size = block_size / 2 ^ s_inoshift`, or in C syntax, `block_size >> s_inoshift`.

The value of `s_inoshift` is determined dynamically during formatting depending on block's size, in a way that a block should
have preferably 4 to 128 inodes, but inode size shouldn't be larger than 4096 bytes, and it also cannot be smaller than 256
bytes. The result is most commonly 512 or 1024 bytes.

This addressing mode is very efficient, but it has a "drawback": all inodes must reside at the beginning of the volume. In practice
this is not a problem, because for example with 4k block size and 4 bits of s_inoshift, this is 2 ^ (64 + 12 - 4), so 2 ^ 72,
meaning inodes must be located in the first 4 Zettabytes, which is a lot more than what an actual storage capacity can provide.
If by any chance we might cross this limit, then an arbitrary data block below the limit must be moved above the limit to make
space for the inode.

### Special inodes

Most inode using file systems reserve the first few inodes for special purposes (not only ext4, NTFS does this as well, although it
calls these MFT records). This makes those file systems very fragile, because the place of the table is fixed. The AXFS also
reserves some inodes for special purposes, but since there's no table here, not from the beginning of a table, rather these can be
anywhere, even more, they can be moved on-the-fly.

Three of these are mandatory, and four are optional.

The `s_volume_fid` is the address of the [volume inode](#volume-inode). Because the superblock itself should be considered
read-only, all variable volume information are stored in this inode. Its data stream covers the inode blocks that have partially
used and partially free inodes.

The `s_root_fid` points to a [directory inode](#inodes-of-regular-files-and-directories). There's nothing special in this inode
(except it's sub-type code), this is simply the root of the [directory hierarchy](#directories).

The `s_freeblk_fid` stores the address of a [block list inode](#free-bad-blocklists-attribute-index-and-quota-inode), with a data
stream that covers all the unused blocks on the volume. When a block is needed, it is taken from the beginning of this data stream
descriptor, and when a file is deleted, its data stram descriptor is simply appended to this inode's descriptor, freeing those
blocks. Both cases are extremely fast, a memcpy limited to a few bytes only, so O(1) asymptotically.

The optional `s_badblk_fid` is very similar to this, but its [pointed inode](#free-bad-blocklists-attribute-index-and-quota-inode)
covers the bad blocks on the volume. When there's no bad blocks at all, this inode isn't allocated and `s_badblk_fid` is zero.

The likewise optional `s_journal_fid` isn't zero when the journaling feature is enabled on the volume. For more information,
see the section [dataconsistency](#dataconsistency).

The also optional `s_attridx_fid` isn't zero when extended attributes are enabled on the volume. For more information, see the
section [attribute indexing](#attribute-indexing).

Another optional `s_uquota_fid` isn't zero when quotas are enabled on the volume. For more information, see the section
[user quotas](#user-quotas).

### Remaining fields

To identify an AXFS superblock, the `s_magic` and `s_magic2` fields store the unique `A`,`X`,`F`,`S` characters. The file system's
version is stored in `s_major` and `s_minor` fields, these are 1 and 0 in this order. To make sure of it that the superblock is
valid, the `s_checksum` field stores the CRC checksum of bytes from 4 to 248.

The `s_flags` store feature flags, letting you know what optional features are used which doesn't have their own special inodes.
If the least significant bit (`AXFS_SB_F_NOCRC`) is set, then that indicates the lack of checksums in various structures (including
the `s_chksum` field). Only under special circumstances should you ever set this bit and by that turn off the checksums.

For encrypted volumes, the parameters are stored in `s_enchash`, `s_enctype` and `s_encrypt` fields. For more information, see
the section [encryption](#encryption).

The `s_uuid` field contains a random number, unique to each and every volume. This identifies the volume, mostly when this is
the only volume on the entire disk, or when the disk is partitioned with legacy MBR partitioning scheme. For GPT partitioning
scheme, the PartitionUniqueGUID field of the partition must always match this field.

The inode
---------

The inode is the most important building block of the file system. Every single file, every single directory and every single
special structure must have its own unique inode. Inodes are referenced by 64 bit fids, see [above](#the-inode-addressing-fid).
Within a volume, all inodes have the same size, `block_size >> s_inoshift` bytes.

Each inode consist of three field groups: a fixed size (32 bytes) common header, a variable sized header depending on the inode's
type, and a data stream descriptor as well.

When the inode's size is 256 bytes then the size of the type dependent header is 96 bytes. For any other size, that's 224 bytes.
So the whole header is either 128 bytes (for 256 bytes inodes) or 256 bytes (for every other size). In special inodes the inode
header size is independent to the inode size and usually smaller. The data stream descriptor always starts directly after the
inode header, and it occupies all the remaining space of the inode.

### Common inode header

| Offset  | Size  | Description                                                            |
|--------:|------:|------------------------------------------------------------------------|
|       0 |     4 | `i_magic` magic bytes to identify, `"AXIN"`                            |
|       4 |     4 | `i_chksum` CRC checksum of bytes 8 to inode size                       |
|       8 |     8 | `i_nblks` total number of allocated blocks for this data stream        |
|      16 |     8 | `i_size` low bits of data stream's size in bytes (0 - 63)              |
|      24 |     2 | `i_size_hi` high bits of data stream's size in bytes (64 - 79)         |
|      26 |     1 | `i_flags` inode feature flags (inlined data stream)                    |
|      27 |     1 | `i_type` the type of the file represented by this inode                |
|      28 |     4 | `i_mime` mime type of the file (0 if `s_attridx_fid` is also 0)        |

The `i_nblks` field stores the number of blocks actually allocated for the data stream, including all the blocks used with indirect
mapping as well, but not including the sizes of block runs with LBN 0 (gaps, sparse files).

The `i_size`, `i_size_hi` (file's apparent size), and the `i_flags` fields are playing role in describing the data stream,
for more information see the section [data stream mapping](#data-stream-mapping).

The `i_type` field indicates the inode's main type, the rest of the inode must be parsed depending on this. Possible values:

| Value   | Define           | Description                                                   |
|--------:|------------------|---------------------------------------------------------------|
|       0 | `AXFS_FT_DIR`    | special, directory                                            |
|       1 | `AXFS_FT_SDR`    | special, live directory (search results)                      |
|       2 | `AXFS_FT_UNI`    | special, directory union                                      |
|       3 | `AXFS_FT_LNK`    | special, symbolic link (symlink, reparse point)               |
|       4 | `AXFS_FT_DEV`    | special, device file                                          |
|       5 | `AXFS_FT_PIPE`   | special, named pipe                                           |
|       6 | `AXFS_FT_SOCK`   | special, socket                                               |
|       7 | `AXFS_FT_BOOT`   | regular file, same as 8, but defragged and not relocateable   |
|       8 | `AXFS_FT_APPL`   | regular file, application (binary bitchunk)                   |
|       9 | `AXFS_FT_AUDIO`  | regular file, sound file or music file                        |
|      10 | `AXFS_FT_FONT`   | regular file, font typeface                                   |
|      11 | `AXFS_FT_IMAGE`  | regular file, 2D picture                                      |
|      12 | `AXFS_FT_MESSAGE`| regular file, message (saved email for exampe)                |
|      13 | `AXFS_FT_MODEL`  | regular file, 3D model                                        |
|      14 | `AXFS_FT_MULTI`  | regular file, multipart                                       |
|      15 | `AXFS_FT_TEXT`   | regular file, ASCII or UTF-8 text                             |
|      16 | `AXFS_FT_VIDEO`  | regular file, video or movie                                  |
|      17 | `AXFS_FT_DBASE`  | regular file, database file                                   |
|      18 | `AXFS_FT_DOC`    | regular file, document, manual                                |
| 128-255 | `AXFS_FT_INT_x`  | AXFS internal                                                 |

The special types (less than 8) are defined by AXFS, while other regular types (between 8 and 127) come from the IANA registry.
AXFS adds two more categories to IANA, database files and documents. Unfortunately IANA messed up the multipart assignment, and
almost every archive file (zip, 7z, targz, cpio, pkg, etc.) was categorized as an application. Despite that, in AXFS all archive
files must be stored with the correct multipart type, code 14 or `AXFS_FT_MULTI`. IANA also defines yet another category,
"example", which is not used by AXFS.

When the optional `s_attridx_fid` field isn't zero and pointing to a valid inode, then `i_mime` is a byte offset into that
inode's data stream, pointing to a zero terminated UTF-8 string, like `image/jpeg`, `text/html`, `application/pdf`, etc. The full
list can be found on the [IANA registered media types](https://www.iana.org/assignments/media-types/media-types.txt) page.
This data stream does not store all the available types listed there, only those that actually occur on the volume. If any of the
`s_attridx_fid` or `i_mime` fields are zero, then the inode's mime type isn't known (`AXFS_MT_UNKN`), which translates as
`application/octet-stream`.

When `i_type` is between 0 and 7 or above 128, then `i_mime` is always zero, with one notable exception. The inode pointed by the
`s_root_fid` field has `i_type` 0 (directory), and its `i_mime` is 0xFFFFFFFF. This allows the reconstruction of the superblock
should there be any data failure.

### Type dependent inode header

As said earlier, the further fields of the inode depend on the value of the `i_type` field in the common inode header.

#### Inodes of regular files and directories

The `i_type` value is below 128.

| Offset  | Size   | Description                                                            |
|--------:|-------:|------------------------------------------------------------------------|
|      32 |      8 | `i_nlinks` how many fid references this inode                          |
|      40 |      8 | `i_btime` inode creation (birth) time                                  |
|      48 |      8 | `i_ctime` inode last change time                                       |
|      56 |      8 | `i_mtime` last modification time of the data stream                    |
|      64 |     16 | `i_attr` block run of the ext. attributes (if `s_attridx_fid` isn't 0) |
|      80 |     16 | `i_owner` file owner's permissions (control ACE)                       |
|      96 | 32/160 | `i_acl` the access control list with 2 or 10 entries (ACL)             |
| 128/256 |      x | `i_d` data stream descriptor, till the end of the inode                |

Depending on the inode size, there could be two different headers. These only differ in the number of entries in ACL, either
2 or 10. About ACLs, see section [permissions](#permissions).

The `i_nlinks` counter keeps track of all the fids that reference this inode. When this reaches 0, then the file's data stream as
well as this inode slot will be freed.

The `i_btime` and the `i_ctime` fields store the inode's creation and last change time, respectively. On the other hand `i_mtime`
stores the last modification time of the data stream. Timestamps are given in microseconds passed since Jan. 1 1970 midnight UTC
(one millionth of a second). Important that it's UTC, so time zones and daytime saving *not* included.

The `i_attr` block run points to a data stream describing the extended attributes of the inode, for more information see the
section [attribute indexing](#attribute-indexing).

The `i_d` field contains the data stream descriptor, for more information see [data stream mapping](#data-stream-mapping).

Directories are stored just like regular files, the only difference is, their data stream has a special structure, for more
information see the section [directories](#directories).

#### Inodes of directory union and symbolic links

The `i_type` is 1, 2 or 3.

The header is exactly the same as with regular files. Their `i_d` data stream contains the target of the union or link, or a
search phrase, a zero terminated UTF-8 string. For unions, there are multiple strings, so the whole target is closed by an empty
string (with other words, two zeros, for example "/bin `0` /usr/bin `0` `0`").

#### Inodes of device files, pipes, sockets

The `i_type` is 4, 5 or 6.

The header is exactly the same as with regular files. The difference is, that their `i_d` data stream is always inlined (so
`i_flags` = 1), and the content isn't general, rather operating system specific.

#### Free, bad blocklists, attribute index and quota inode

The `s_freeblk_fid`, `s_badblk_fid`, `s_attridx_fid` and `s_uquota_fid` fields point to such inodes, `i_type` is either
0xFF (free blocks list), 0xFE (bad blocks list), 0xFB (attribute index) or 0xFA (quota).

With these, all the rest of the inode (from the 32th byte to the end) contains a block run list in `i_d`.

For `i_type` 0xFF (or 0xFE), the data stream covers free (or bad) blocks.

For `i_type` 0xFB (attribute index), see section [attibute indexing](#attibute-indexing).

For `i_type` 0xFA (quota), see section [user quotas](#user-quotas).

#### Volume inode

The `s_volume_fid` points to this inode, `i_type` is 0xFD.

| Offset  | Size   | Description                                                            |
|--------:|-------:|------------------------------------------------------------------------|
|      24 |      4 | `i_state` free to use by file system check (0 means everything's ok)   |
|      32 |      8 | `i_ninode` total number of inodes on this volume                       |
|      40 |      8 | `i_lmtime` last mount time                                             |
|      48 |      8 | `i_lutime` last umount time                                            |
|      56 |      4 | `i_nwrite` maximum number of inode writes (0 if feature not used)      |
|      60 |      4 | `i_cwrite` remaining number of inode writes (counts down)              |
|      64 |      x | `i_d` data stream descriptor, till the end of the inode                |

The `i_mime` field is repurposed as `i_state`, and can be used by the file system checker (fsck) as it pleases. The only
restriction is, 0 must mean everything is all right with the volume, and the lowest bit means there's a need for fsck.

The `i_ninode` field stores the total number of currently used inodes on the volume (so how many files and directories there are).

When the volume is mounted, `i_lmtime` is set to the current timestamp and `i_lutime` is cleared. On umount, `i_lutime` is set
to the current timestamp. This allows detection if the volume is "dirty" (if `i_lutime` is already 0 on read), so if there's a
need to run file system checks (indicated by `i_state` being set to 1). When there's a journaling file, this is not needed. For
more information, see the section [dataconsistency](#dataconsistency).

The `i_cwrite` is a counter which counts down and contains the remaining number of inode writes. When it reaches zero, it's
reset to `i_nwrite`, and the special inodes will be relocated. This is needed to significantly expand the lifespan of devices with
limited write cycles. When `i_nwrite` is 0, then this feature is not used.

With this inode, `i_flags` must be 0, and the data stream descriptor starts at offset 64 regardless to the block size. This
`i_d` data stream then covers all the inode blocks which have partially used and partially free inode blocks. Those with only
used inodes or with only free inode slots are not in the list.

#### Journaling inode

The `s_journal_fid` points to this inode, `i_type` is 0xFC.

| Offset  | Size   | Description                                                            |
|--------:|-------:|------------------------------------------------------------------------|
|      32 |      8 | `i_blk` journalbuffer start LBN                                        |
|      40 |      8 | `i_len` journalbuffer length in blocks                                 |
|      48 |      8 | `i_head` journal head                                                  |
|      56 |      8 | `i_tail` journal tail                                                  |
|      64 |      x | `i_d` data stream descriptor, till the end of the inode                |

For more information see [dataconsistency](#dataconsistency).

Permissions
-----------

AXFS uses Access Control Lists (ACL). Here each Access Control Entry (ACE) is a 16 bytes long UUID, but the last byte is used
to store the access bits. The first element of the list, `i_owner` is special, as that's the file's owner and also the one who
can modify the other ACE in `i_acl` (Control ACE). Besides the usual access bits `rwx` (read, write, execute), further bits are
stored. `a` append (no write, but may append), `d` (can delete file even without write permission on its parent directory),
`!` not (negate the ACE, for example they can't write this). The most significant last two bits are the usual setuid (inherit
`i_owner`) and setgid (inherit `i_acl`).

Just like with other file systems where uid and gid is unspecified, AXFS does not define UUID either, assigning those is a job
for the operating system. However there are two special cases: the full zeros UUID, `00000000-0000-0000-0000-0000000000xx` means
that the ACE isn't used (in this case the last byte doesn't matter). The full one bits UUID, `FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFxx`
means catch-all, it matches any UUID no matter. Both values also mark the end of the ACL list.

For simulating the UNIX permissions, uid is placed in `i_owner.Data1`, the other Data2, Data3, Data4 fields are zero and only the
least significant 3 bits and the most significant 2 bits are used in `i_owner.access` which store owner rights. The gid goes in
`i_acl[0].Data1`, the other Data2, Data3, Data4 fields are zero and only the least significant 3 bits used in `i_acl[0].access`
which store group rights. For `i_acl[1]` all bytes are 0xFF, except the last one, only the least significant 3 bits used in
`i_acl[1].access` which store rights for others.

Data stream mapping
-------------------

The most important job of an inode (besides storing the file's attributes) is to map the data stream to allocated blocks. This is
done in the `i_d` data stream descriptor, which always follows tightly the inode header. For 256 bytes inodes, it starts at offset
128, for every other inode size at offset 256 (except for special inodes, which have a fixed sized header so the descriptor starts
at the same position, no matter the inode size).

What this `i_d` actually stores, depends on the `i_flags` field. When set, then it stores inlined data, otherwise a block run list
which describes as many blocks as many can cover `(i_size_hi << 64) + i_size` bytes. This is independent to the actual number of
allocated blocks, which is stored in `i_nblks`.

### Block runs

AXFS does not store the block numbers alone, rather in an `axfs_extent_t` structure, so called block runs.

| Offset  | Size  | Description                                                            |
|--------:|------:|------------------------------------------------------------------------|
|       0 |     8 | `e_blk` the pointed data or another block run list's starting LBN      |
|       8 |     8 | `e_num` number of blocks covered (aggregated sum if indirect)          |
|      16 |     4 | `e_len` allocated length in blocks if indirect                         |

But this is only when in memory; on disk these block runs are stored as compressed packets, using the following format:

```
   0        1       x       y       z        bytes
   +--------+-------+-------+-------+
   | packet | e_blk | e_num | e_len |        block run, packed extent
   +--------+-------+-------+-------+
```
The first `packet` byte is the header. Each bits in it:

| Bit   | Size  | Description                                                            |
|------:|------:|------------------------------------------------------------------------|
| 0 - 2 |     3 | size of `e_blk` in power of two bits                                   |
| 3 - 5 |     3 | size of `e_num` in power of two bits                                   |
| 6 - 7 |     2 | size of `e_len` in power of two bits                                   |

So here 0 means no more bytes stored, 1 means an uint8_t, 2 means an uint16_t, 3 means uint32_t, and 4 means uint64_t. Because
the size for `e_len` has only two bits, it can only be uint32_t at most. If this whole byte is zero (so no bytes follow the header)
then that means end of the block run list. Because there's no such thing as a block run with no blocks and also because having
a block run of size 1 block is pretty common, therefore `e_num` with value of 1 is saved without further bytes, and in the `packet`
its size is going to be zero. This also means that packets without e_num bytes must be decoded as `e_num = 1`. To distinguish a
one block long emptiness from the block run list terminator, the former is always stored with an explicit 1 value on a byte
for `e_num`, so as 0x08 0x01.

Example: let's assume packet has a header of 0x14. Extracting the bits we get 4, 2 and 0. So this header byte is going to be
followed by an uint64_t with the value of `e_blk`, followed by an uint16_t with the value of `e_num`. Because the most significant
bits in the header are zero, so no bytes are stored for `e_len`, that is read as value of 0.

### File Offset Address Space

It is very important that unlike classic UNIX file systems, where the first few entries store a direct block address, then comes
one with indirect address, and one with double indirect address and yet another one with triple indirect, in AXFS it could be that
the first entry is direct, the second is quadriple indirect, and the third is direct again.

```
  FAT       .  UNIX      .  AXFS
      a     .         a  .
      |     .         |  .
      b     .       a b  .     a   a
      |     .       | |  .      \ /
      b     .     a b b  .   a a b
      |     .     | | |  .    \|/
      b     . a a b b b  .  a  b a
      |     .  \ \|/ /   .   \ | |
  direntry  .   inode    .   inode
```

AXFS stores the file offset mappings in a very flexible tree, with a guaranteed maximum depth of four levels. Here on each level
you can find a block run list. This might contain another branches or leafs, even mixed on the same level.

- A leaf is when `e_len` is 0. This time `e_blk` points directly to the start block of the data, which has `e_num`  blocks, and
  therefore covers `e_num * block_size` bytes from the file offset's address space.

- A new branch is when `e_len` isn't 0. Now `e_blk` points to the first block of `e_len` continuous blocks, which store another
  block run list. Here `e_num` is the aggregated sum of all the `e_num`s in this pointed block run list, so it still stands that
  `e_num` covers `e_num * block_size` bytes from the file offset's address space.

You can search in this tree with a recursive algorithm, which runs as many times as the deepest level of the tree for a certain
file offset. Recursive means that you use exactly the same algorithm on every level of the tree. This algorithm has a variable,
`current`, which contains a byte file offset. On all levels, the block run list starts describing from `current`, and they
describe as big address space in blocks as the previous level's `e_num` value. At start `current` is zero (beginning of the address
space) and here the imaginary parent's `e_num` is `(i_size + block_size - 1) / block_size`, so the topmost level covers the entire
file's address space.

1. get the next block run from the list
2. `offset >= current` and `offset < current + e_num * block_size`?
3. if not then let's add `e_num * block_size` to `current` and go to step 1.
5. in the block run which we have found, is `e_len` zero?
6. if not, then repeat recursion starting at `current` with `e_num` blocks of address space on the block run list pointed by `e_blk`
7. otherwise the LBN of the block for the desired file offset is `e_blk + (offset - current) / block_size`

Furthermore in the description below we refer to the descriptor as an array of `axfs_extent_t`, however these are not stored as-is
on the disk, in reality they are stored in a compressed form.

### Inlined

When the least significant bit of `i_flags` is 1.

```
+-----inode-----+
| i_flags = 1   |
| other fields  |
|. . . . . . . .|
| data          |
+---------------+
```

Here `i_d` stores the file's content directly. This is only possible when `i_size_hi` is zero and `i_size` is smaller or equal
than the space in the inode after header. In this case `i_nblks` is always 0, because there are no allocated blocks for inode.

### Direct

When `i_flags` is 0 and the block run's `e_len` is 0 too.

```
+-----inode-----+     +-------+
| i_flags = 0   | .-->| data  |
| other fields  | :   | block |
|. . . . . . . .| :   +-------+
| axfs_extent_t-+-'   +-------+-------+-------+
| axfs_extent_t-+---->| more data             |
| axfs_extent_t |     | blocks                |
| axfs_extent_t |     +-------+-------+-------+
| axfs_extent_t |
+---------------+
  ^ e_len = 0
```

With `i_flags` being 0, there's always a block run list in the inode. In this `axfs_extent_t` array the `e_len` field is zero, and
the `e_blk` fields are pointing to the data blocks directly. If one of the block run has `e_blk` as 0, then that's not stored,
instead it is considered to be a data full of zeros as long as the block run describes. For example if every `e_blk` is 0, then
the file has `i_size` zeros as content, and of course `i_nblks` is also zero because no blocks are allocated for this inode.

### Indirect

When `i_flags` is 0 but the block run's `e_len` is not 0, however the pointed block run's `e_len` is 0.

```
+-----inode-----+    +---------------+   +-------+-------+-------+
| i_flags = 0   | .->| axfs_extent_t-+-->| more data             |
| other fields  | :  | axfs_extent_t |   | blocks                |
|. . . . . . . .| :  +---------------+   +-------+-------+-------+
| axfs_extent_t-+-'  +---------------+
| axfs_extent_t-+--->| axfs_extent_t |   +-------+
| axfs_extent_t |    | axfs_extent_t-+-->| data  |
| axfs_extent_t |    | axfs_extent_t |   | block |
| axfs_extent_t |    | axfs_extent_t |   +-------+
+---------------+    +---------------+
  ^ e_len != 0         ^ e_len = 0
```

In case the `axfs_extent_t` extent's `e_len` field in the inode isn't zero, then the `e_blk` points to a block with further
`axfs_extent_t` entries. In these the `e_len` is 0, and the sum of all their `e_num` fields is stored in the inode's extent's
`e_num` field.

### Double indirect

When `i_flags` is 0 but the block run's `e_len` is not 0, neither the pointed block run's `e_len`.

```
+-----inode-----+    +---------------+   +---------------+     +-------+
| i_flags = 0   | .->| axfs_extent_t-+-->| axfs_extent_t-+---->| data  |
| other fields  | :  | axfs_extent_t |   | axfs_extent_t |     | block |
|. . . . . . . .| :  +---------------+   | axfs_extent_t |     +-------+
| axfs_extent_t-+-'  +---------------+   | axfs_extent_t |
| axfs_extent_t-+--->| axfs_extent_t |   +---------------+
| axfs_extent_t |    | axfs_extent_t |   +---------------+     +-------+
| axfs_extent_t |    | axfs_extent_t-+-->| axfs_extent_t-+---->| data  |
| axfs_extent_t |    | axfs_extent_t |   | axfs_extent_t |     | block |
+---------------+    +---------------+   +---------------+     +-------+
  ^ e_len != 0         ^ e_len != 0        ^ e_len = 0
```

Similar to indirect mapping, but here the `axfs_extent_t` entries are pointing to `axfs_extent_t` entries that also has `e_len` set.

Under normal circumstances you'll never encounter double indirect mappings. It is important to stress again that here we're talking
about compressed block runs, and not about block address tables, like in classic UNIX. Despite that, AXFS supports not only double
and triple indirection, but you can go up to 4 levels. Here in each level `e_len` isn't zero, except for the last one, where
the `axfs_extent_t` entries are pointing data blocks directly, so they have `e_len` as 0. The `e_num` fields are always the
aggregated sum of the `e_num` fields below them.

Directories
-----------

The directories are actually regular files, it's just that their data stream store some structured content. A directory might
contain entries to files or to other directories, so it is possible to create a directory tree hierarchy. The top level of this
tree, called the root directory, is stored in `s_root_fid`. This is just like with any other file systems.

But most file systems don't care about providing fast access to directory entries (eg. FAT, ext2/3, Minix), or they use far too
overcomplicated hash trees or binary trees for that (ext4, NTFS, BeFS), that wastes space unnecessarily and makes everything else
besides fast access rather quite complicated.

For AXFS the goal was to use as small footprint for directories as possible, but also provide O(log2) access for each entry.

### Directory header

A directory always starts with a header, followed by tightly packed directory entries, but there's an indextable at the end as
well.

| Offset  | Size  | Description                                                            |
|--------:|------:|------------------------------------------------------------------------|
|       0 |     4 | `d_magic` magic bytes to identify, `"AXDR"`                            |
|       4 |     4 | `d_chksum` checksum of the indextable                                  |
|       8 |     4 | `d_size` size of the header and the directory entries in bytes         |
|      12 |     4 | `d_nument` number of directory entries                                 |
|      16 |     8 | `d_self_fid` back reference to the inode (current directory)           |
|      24 |     8 | `d_gui_attr` attributes for the user interface                         |

The `d_chksum` field contains the checksum of the indextable for the directory entries. It is zero if the directory isn't indexed.

The `d_size` field includes the size of the header as well as the directory entries in bytes. This is needed because `i_size`
stores the entire file's size, also including the size of the indextable. When this `d_size` is smaller than 65536, then the
indextable has 2 bytes long uint16_t entries, otherwise 4 bytes long uint32_t entries.

The `d_nument` is the number of the directory entries (not including the deleted entries). Since entries are packed tightly and
they might contain deleted entries as well, it's not easy to figure this number out. Also gives the size of the indextable, which
has this many entries, and each entry has the same size.

The `d_self_fid` field is a fid that points back to this directory's inode, should the reconstruction of the file system needed.
The AXFS does not store separate "." current and ".." parent entries, but you might consider this as if there were a "." current
directory entry. Because multiple references can be made to a directory (via hard links, symlinks, unions), so multiple parents
might exist, therefore ambiguous. Only parsing the path string can give you the proper parent directory for that path, so AXFS
doesn't even bother trying to store the parents at all. You can still store a ".." entry if you want, it's just not mandatory.

The `d_gui_attr` is to store the attributes of the user interface (for example sorting order, icon size etc.). This was put here
for two reasons: first, quick and easy access, and second, this should work even when `s_attrid_fid` is zero, and extended
attributes aren't enabled on the volume. It worth noting that sorting order in these is just for the interface, does not influence
the indextable, which is always ascending lexicographically ordered on disk.

### Directory entries

This 32 bytes long header is followed by the directory entries, on `d_size` - 32 bytes, `d_nument` records in total, terminated
with a zero byte (a block run list terminator, this latter byte not included in `d_size`).

| Offset  | Size  | Description                                                            |
|--------:|------:|------------------------------------------------------------------------|
|       0 |     8 | `d_fid` the pointed inode's fid                                        |
|       8 |     1 | `d_nlen` the length of d_name                                          |
|       9 |   247 | `d_name` file name, plus a terminating zero                            |

But this is only in memory, on disk the `d_fid` is stored as a block run, in which `e_num` and `e_len` has 0 length, and the
`e_blk` field stores the fid. This is followed by `d_name`, the zero terminated UTF-8 file name. The `d_nlen` field is not stored
on disk, only in memory.

All entries within the same directory must have a different `d_name`, but might have different entries with the same `e_blk` value.

```
   0        1               x                 bytes
   +--------+---------------+--------+---+
   | packet | e_blk = d_fid | d_name | 0 |    directory entry
   +--------+---------------+--------+---+
```

Because packet header needs 1 byte, fid could be 8 bytes long, file name maximum length is 246, and there's a terminating zero, so
the upper limit to any compressed directory entry record is 256 bytes.

```
   0        1               x                 bytes
   +--------+---------------+--------+---+
   | packet | e_num = d_fid | d_name | 0 |    deleted directory entry
   +--------+---------------+--------+---+
```

When an entry is deleted, then the length of `e_blk` is set to zero, and the original fid is stored in `e_num`.

### Directory indextable

If `i_size` is bigger than `d_size` + 1, then the directory entries are followed by an indextable as well. This starts at a 4 bytes
aligned offset and is a plain simple uint16_t or uint32_t array (depending on `d_size`), where each entry stores the byte offset
to directory entries in `d_name`'s lexicographical ascending order (so not a byte compare, rather each UTF-8 multibyte is extracted
and compared as UNICODE codepoints).

Note that using the indextable isn't mandatory: if an implementation stops reading at `d_size`, then it will see a classic UNIX
directory, just like in any other, and it doesn't have to know about the indextable. On the other hand if it does read the
indextable in, then it can use that for fast O(log2) access easily.

Dataconsistency
---------------

Imagine that we're saving a very important file when the cleaning lady (sister, wife, mum, collegue, whatever) comes hovering
and accidentally strips out the computer's plug. What happens to file system consistency then?

Two things could happen: internal inconsistency and external inconsistency.

### Internal inconsistency

The controller of storage devices only guarantees that a sector is written in its entirety on disk, so this is one atomic operation.
If the meta data size is less than the physical sector size, then we shouldn't worry, because either the entire meta data is written
out or it isn't. On the other hand if the meta data is bigger, then it might happen that it is only partially written to disk. In
order to detect this scenario, AXFS uses checksums in meta data, which are placed in a way to be surely included in the very first
physical sector no matter the size. This way when a meta data is read and the checksum is calculed, the driver can tell if the meta
data was successfully written out in its entirety or not.

### External inconsistency

The other case is the result of how file operations work, you must modify different blocks all around the volume. For example
creating a file needs to write a block with the new inode, it must update the free blocks list and the directory with the file
name, as well as the directory's indextable and inode. So external inconsistency is always something to be reckoned with.

### Journaling

As a solution, AXFS offers an optional feature, which is enabled when the `s_journal_fid` field isn't zero. The pointed inode's
type dependent header goes like: `i_blk` and `i_len`, which work just in a block run: `i_blk` contains the start LBN of a buffer,
and `i_len` is the length of the allocated buffer in blocks. These are determined during formatting, and this buffer isn't used
by the file system for any other purpose (but should there be a need, you can always relocate or resize the buffer even on-the-fly,
provided it is empty). The size of the journalbuffer is typically rather small, as many blocks as many meta block modifications
might occur during a single file operation multiplied by two to handle cases when meta data crosses block boundary and two blocks
needs to be written at once. Using larger buffer than this might only be worth it on certain devices, because bigger buffer
decreases the number of write cycles per block within the buffer.

There are two more pointers in that inode header, the `i_head` and the `i_tail`. When these two are the same, then the buffer
is empty. If `(i_tail + 1) % i_len` equals to `i_head`, then the buffer is full. Both are increasing counters that overflow at
`i_len` and become 0 again.

### Journal transaction

When the journal exists then the block writes are not happening in order. This time the data blocks are written out to their
final place as usual, however the meta data blocks are written to the next block pointed by `i_blk + i_tail`, and simultaneously
a block run is added to the journal file's data stream descriptor with the original (final) LBN address. Then `i_tail` is increased
by one and it overflows at `i_len`. This repeats as many times as many meta data writes there are in a file operation. When the
file operation has finished and there are no more blocks left to be written, then the journal's inode with the new `i_tail` value
and updated data stream descriptor is also written to the disk (in case the data stream isn't inlined, then the indirect block must
be written first, and then the journal inode). With this, a transaction is recorded.

What happens next, is also executed every time a volume is mounted.

### Journal replay

The file system driver checks if the journal is empty (`i_head` != `i_tail`). If it's not empty, then it starts reading block
runs from its data stream descriptor. It reads as many blocks from `i_blk + i_head` from the journal buffer as many blocks are
indicated in `e_num`, and it copies those to the LBN indicated by `e_blk`. In that loop it also increases `i_tail` by one, and it
overflows at `i_len`. By the time the data stream descriptor's end is reached, `i_head` should match `i_tail`. After this the
journal's inode, with empty data stream desriptor and the updated `i_head` (which now is the same as `i_tail`) is written out to
the disk. With this, a transaction is committed.

How does this solve the problem? Well, if the power is cut off before the journal's inode is written, then the file system
hasn't changed yet, so it is in a consistent state. If it happens after that, then it is true that the file system is left in an
unknown state, however when the machine is rebooted and the volume is mounted again, then the file system driver sees that the
journal isn't empty, and it replays its content before doing anything else. This way the file system becomes consistent again.

Of course this only guarantees that the file system is in a consistent state, the contents of the file might be lost anyway (if
the power is cut out before the transaction was recorded successfully). You must have an UPS to get protection against that,
because writing the data blocks into the journal won't work. If all the data blocks were also written to the journal, then that
would cut the storage's performance in half, and also you would have to increase the size of the journalbuffer considerably
by the number of blocks in the largest write there could be, taking away the space from meaningful data.

Attribute indexing
------------------

On a first glance extended attributes are simple: you can attach "key=value" pairs to files. And yet, this is the most complex
part of AXFS. The difficulty lies in the fact that the goal of AXFS is to minimize the amount of meta data, and also that you must
get these attributes back efficiently.

The AXFS defines only 4 of the attribute keys, the users are free to choose all the others.

- `size` refers to the inode's `i_size` field
- `mime` refers to the inode's `i_mime` field
- `path` refers to the path string constructed from inode's `i_attr.e_num` values
- `icon` a path pointing to an image file with the file's icon

### Simulated data streams

First, the inode pointed by `s_attridx_fid` must be able to handle multiple and varying number of data streams. For this, the
file offset address space is splitted into multiple 4 GiB chunks, and each chunk corresponds to one simulated data stream,
addressed by an offset relative to the start of that chunk. So the lower 32 bits of the file offset describe the simulated
data stream's file offset, and the most significant bits store the index of the simulated data stream. Note that AXFS handles
sparse files, so if you write a byte at file offset 0, another at 4 GiB, and yet another at 8 GiB, then it would look like the
file is 8 GiB in size, but actually only 3 blocks will be allocated as the gaps between the data aren't stored.

### Attribute header

Extended attributes are stored in each inode and in each simulated data streams. Regardless the location, they always have a common
header, which differs in the magic bytes only.

| Offset  | Size  | Description                                                            |
|--------:|------:|------------------------------------------------------------------------|
|       0 |     4 | `a_magic` magic bytes to identify, `"AXEA"` / `"AXIK"` / `"AXIV"`      |
|       4 |     4 | `a_chksum` CRC checksum                                                |
|       8 |     4 | `a_size` size of the attributes in bytes                               |
|      12 |     4 | `a_nument` number of attributes                                        |
|      16 |     8 | `a_self_fid` back reference to the inode                               |

### Inode extended attributes

In every inode the `i_mime` field stores a byte offset to the attribute index' data stream. This byte offset aligns with the first
simulated data stream's byte offset, so it will point to an `AXIK` record (to be precise, to the `d_name` field of the record).

```
+-----inode-----+ .---------------------------.  +---s_attridx_fid--+
| i_mime--------+-'                           |  | +---AXIK---+     |
| i_attr.e_blk--+----> +-------AXEA--------+  `--+-+-> dirent |     |
| i_attr.e_num--+-.    | ext. attributes---+-----+-+-> dirent |     |
|               | |    | ext. attributes---+-----+-+-> dirent |     |
|               | |    +-------------------+     | |   dirent |     |
| other fields  | `--> +-------inode-------+     | +----------+     |
|               |      | parent directory  |     |                  |
+---------------+      +-------------------+     +------------------+
```

For each inode the extended attributes (or xattr) are stored in the `i_attr` field as a block run. In this block run `e_len` must
be always zero, `e_blk` stores an LBN pointing to an `AXEA` block, and `e_num` stores the inode's parent directory's fid. This is
needed so that the search results can be converted into paths. Here one block for `AXEA` should be enough, because one inode
doesn't have too many attributes at all, and also it does not store the strings here, just their compressed offsets (with 4k
block size, this means several hundred, roughly ca. 500 attributes per inode).

This data block (stored in each and every inode, and NOT in the attribute index inode) also starts with the attribute header, where
`a_magic` is `AXEA`, `a_chksum` is the CRC from bytes 8 to `a_size`. The `a_self_fid` points back to the inode which references
this extended attribute data block, should file system reconstruction needed.

This header is followed by a block run list, on `a_size` - 24 bytes. Here in each record `e_blk` is an offset to the attribute's
key, just like `i_mime`, and `e_num` is an offset to the attribute value, also an offset to the attribute index data stream. They
are both pointing to `d_name` fields of an `AXIK` record, see below.

### Index keys

The first simulated data stream has the same header too, with `a_magic` being `AXIK`. Its `a_self_fid` has the same value as
`s_attridx_fid`. Otherwise it looks like and works like exactly as a directory and it also has an indextable too. Therefore its
`a_chksum` is the checksum of this indextable, just like in a directory. The size of one indextable entry depends on `a_size`
being smaller than 65536.

This index key data stream stores both attribute keys and attribute values, using the same format described in section
[directory entries](#directory-entries). The only difference is, here some packet bytes will be zero, while this isn't allowed
in a directory.

```
   0        1            x                          bytes
   +--------+------------+-------------------+---+
   | packet | e_blk = ds | d_name = attr key | 0 |  attribute key
   +--------+------------+-------------------+---+
```

For attribute keys the `e_blk` field stores the index of a simulated data stream, which must be 1 at least.

```
   0            1                                   bytes
   +------------+---------------------+---+
   | packet = 0 | d_name = attr value | 0 |         attribute value
   +------------+---------------------+---+
```

On the other hand for attribute values the structure is the same, but here `e_blk` is always missing, so packet is always zero.

The `i_mime`, `AXEA.e_blk`, `AXEA.e_num`, as well as `AXIV.e_num` fields point to the first byte of `d_name` in these records.

### Index values

All the other simulated data streams start with the common header too, with `a_magic` being `AXIV`. In the least significant 32
bits of these `a_self_fid` fields the byte offset of the attribute key's `AXIK` record is stored, and the most significant bits
hold the simulated data stream's index, should file system reconstruction needed. It also has an indextable, which is sorted
lexicographically by attribute values.

But instead of normal directory entries, these tables have just a single block run in their records:

```
   0        1               x                             bytes
   +--------+---------------+-------------------------+
   | packet | e_blk = d_fid | e_num = attr val d_name |   key value index
   +--------+---------------+-------------------------+
```

Just like with directory entries, `e_blk` stores a fid of an inode, but there's no `d_name` here, instead `e_num` has a byte offset
to a `d_name` field of an attribute value record in `AXIK`. It also differs from a directory entry in that multiple occurances with
the same `e_num` allowed, but all `e_blk` fields must be different (for a usual directory `d_name` must be unique and `e_blk` values
might repeat).

### Search workflow

To look up which files have a certain attribute key, one should do an O(log2) search on the index key table. Then the pointed
simulated data stream has an index value table with fids to all the inodes with that attribute key.

If a certain key value pair lookup is needed, then a second O(log2) search is performed, but this time on the index value table.
The result must be adjusted if the previous record has the same `e_num` value as the result's, and all records returned with that
particular `e_num`. (Because the indextable is sorted, the same `e_num` values are always groupped together.)

Finally to convert inode fids in the search result to human readable file paths, the `i_attr` block run field's `e_num` values are
consulted and a path is constructed for each search result.

User Quotas
-----------

When a computer is used by several users, then it might be necessary to disallow users to eat up all the storage space from other
users. This feature is enabled when `s_uquota_fid` isn't zero.

The quota file is a regular file, but it has fixed sized records which are always sorted by ascending UUID order. So for the quota
file, `i_size` must be multiple of 32. Because these records are sorted, look up is O(log2).

| Offset  | Size  | Description                                                            |
|--------:|------:|------------------------------------------------------------------------|
|       0 |    16 | `q_uuid` user's id (the i_owner field in the inode)                    |
|      16 |     8 | `q_nblks` total number of blocks currently allocated by this user      |
|      24 |     8 | `q_limit` number of blocks that this user allowed to allocate          |

The last byte in each `q_uuid` must be zero. When a user who's UUID is listed in this file deletes, creates files or writes to the
disk, then the total number of used blocks counter in `q_nblks` is updated. If that field would exceed `q_limit`, then the user
will get a "no more space left on device" error message.

Encryption
----------

This feature is used when the `s_enchash` field isn't zero. It stores the CRC of the decrpytion password, regardless to the
cipher used, so that it could be determined apriori if the password is correct or not (if one or two character is mistyped in
the correct password then the checksum is going to be totally different, however there are infinite number of totally different
bad passwords that produce the same checksum which is bad news for attackers). For encrypted volumes, all blocks except for the
the superblock is encrypted (if the first block is shared with inodes, so `s_nresblks = 0`, then the first 256 bytes isn't
encrypted, but the rest is), and on formatting all unused blocks are filled with random bytes, up to the `s_nblks`th block (this
is time consuming, but a must for security).

When the password's checksum matches, then the encryption key `enckey` is calculated as follows:

1. `tmp = sha256(sha256(password) + uint32_t s_enchash)`
2. after that the first 28 bytes of `tmp` XOR'd with the bytes in the `s_encrypt` field and the first byte of password plus position
3. finally `enckey = sha256(first 28 bytes of tmp)`

This cumbersome method serves two purposes: first, increased security (sha256 3 times in a row makes the rainbow hashing attack very
difficult if not impossible), and second, by changing `s_encrypt` it allows replacing the password without the need of re-encrypting
the entire volume.

After that each block is processed separately, so any LBN independently to others can be encrypted or decrypted. For this, the
cipher is given in the `s_enctype` field.

When `s_enctype` is 0, then a symmetric cipher is used (same steps used to encode and decode as well), `AXFS_SB_E_SHA`:

1. for each block, start with `blkkey = sha256(32 bytes of enckey + uint64_t LBN)`
2. `blkkey = sha256(blkkey)`
3. the next 32 bytes of the block is XOR'd with the bytes in `blkkey`
4. if we are not at the end of the block yet, go to step 2.

When `s_enctype` is 1, then the asymetric AES256-CBC is used, `AXFS_SB_E_AES`.

1. the AES key is `enckey` as-is
2. the AES iv is choosen as 16 bytes from `enckey` from position `(enckey[0] + LBN) & 15`
3. to decrypt, you call `AES_decrypt` on the entire block, and to encrypt you call `AES_encrypt`

The first, the cyclic SHA is simple and fast, yet it provides very decent privacy. The second one is a lot slower, but in return
is a military grade encryption. If by any chance any of these methods become known as compromised, then in the future AXFS might
add new ciphers (not likely unless quantuum computers become commonplace).

Example data
------------

### Superblock example

Using the swiss army knife tool:

```
$ ./tool.axfs /dev/sda1 lbn 0
AleXandriaFS tool Copyright (c) 2023 bzt, GPLv3+

  Disk offset:      0000000000000000
  Inode entry:      0000: fid 0000000000000000, AleXandriaFS SuperBlock (valid)
  Inode entry:      0100: fid 0000000000000001, used, type fd 00000000 (AXFS_FT_INT_VOL)

$ ./tool.axfs /dev/sda1 sb
AleXandriaFS tool Copyright (c) 2023 bzt, GPLv3+

Superblock:
  Location:         LBN 0, index 0
  Disk offset:      0000000000000000
  Magic:            AXFS
  Version:          1.0
  Block size:       512 bytes (s_blksize 0)
  Inode size:       256 bytes (s_inoshift 1)
  Flags:            0000
  Enctype:          00 (AXFS_SB_E_SHA)
  Encyrpt:          00 00 00 00 ... 00 00 00 00
  Enchash:          0000 (no encryption)
  UUID:             525556B6-6D3E-428D-0ADAB7738A848921
  Total blocks:     2048 (1 MiB)
  Volume fid:       0000000000000001
  Root dir fid:     0000000000000002
  Free blocks fid:  0000000000000003
  Journal fid:      0000000000000000
  Attributes fid:   0000000000000000
  User quota fid:   0000000000000000
  Bad blocks fid:   0000000000000000
  Checksum:         8542b92b (valid)
```

Annotated hexdump:

```
00000000  00 00 00 00 41 58 46 53  01 00 00 01 00 00 00 00  |....AXFS........|
          └s_loader─┘ └─s_magic─┘        ├┘ └┤       └┴──s_nresblk=0
                                s_blksize=0  s_inoshift=1
00000010  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000020  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
          └────────────────no encryption─────────────────┘
00000030  b6 56 55 52 3e 6d 8d 42  0a da b7 73 8a 84 89 21  |.VUR>m.B...s...!|
          └────────────────────s_uuid────────────────────┘
00000040  00 08 00 00 00 00 00 00  01 00 00 00 00 00 00 00  |................|
          └────s_nblks=0x800────┘  └────s_volume_fid─────┘
00000050  02 00 00 00 00 00 00 00  03 00 00 00 00 00 00 00  |................|
          └────s_root_fid=2─────┘  └────s_freeblk_fid────┘
00000060  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
000000f0  00 00 00 00 00 00 00 00  2b b9 42 85 41 58 46 53  |........+.B.AXFS|
          └────s_badblk_fid─────┘  └s_chksum─┘ └s_magic2─┘
00000100  41 58 49 4e d2 d7 f0 c9  00 00 00 00 00 00 00 00  |AXIN............|
00000110  00 00 00 00 00 00 00 00  00 00 00 fd 00 00 00 00  |................|
00000120  19 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
```

The superblock starts at the beginning of the disk (or partition). There's no jump instruction, so no boot loader code. Block size
is `1 << (s_blksize=0 + 9)`, so 512 bytes. Inode size is shifted to the right `s_inoshift=1` bits, so one inode is 256 bytes long.

We can also see that since there are no reserved blocks for the boot loader (`s_nresblk=0`) and inode size is smaller than the
block size, so the first inode is at fid 1, which translates to LBN 0, index 1, therefore the first inode comes right after the
superblock (fid 0, which always translates to LBN 0, index 0 no matter the blocksize, always points to the superblock).

### Inode example

The root directory's fid is 2, and because `s_inoshift=1` this gives LBN 1, index 0. Since block size is 512 bytes and inode size
is 256 bytes, therefore this inode can be found on disk at byte address `1 * 512 + 0 * 256 = 0x200`.

Using the swiss army knife tool:

```
$ ./tool.axfs /dev/sda1 lbn 1
AleXandriaFS tool Copyright (c) 2023 bzt, GPLv3+

  Disk offset:      0000000000000200
  Inode entry:      0000: fid 0000000000000002, used, type 00 ffffffff (AXFS_FT_DIR, AXFS_MT_ROOT)
  Inode entry:      0100: fid 0000000000000003, used, type ff 00000000 (AXFS_FT_INT_FREE)

$ ./tool.axfs /dev/sda1 inode 2
AleXandriaFS tool Copyright (c) 2023 bzt, GPLv3+

  Location:         LBN 1, index 0
  Disk offset:      0000000000000200
  Magic:            AXIN
  Checksum:         5faea2f0 (valid)
  Allocated blocks: 0
  File size:        126 bytes +(0 << 64)
  Flags:            01 (inlined)
  File type:        00 (AXFS_FT_DIR)
  Mime type:        ffffffff (AXFS_MT_ROOT)
  Number of links:  1
  Birth time:       1705136192000000 (2024-01-13 09:56:32.000000)
  Changed time:     0 (1970-01-01 01:00:00.000000)
  Modified time:    0 (1970-01-01 01:00:00.000000)
  Extended attribs: (none)
  Owner:            746F6F72-0000-0000-00000000000000:rwx----
  Inlined data:     0000: 41 58 44 52 00 2d f9 8e 65 00 00 00 0b 00 00 00  AXDR.-..e.......
                      ...
```

Annotated hexdump:

```
00000200  41 58 49 4e f0 a2 ae 5f  00 00 00 00 00 00 00 00  |AXIN..._........|
          └─i_magic─┘ └i_chksum─┘  └───────i_nblks───────┘
00000210  7e 00 00 00 00 00 00 00  00 00 01 00 ff ff ff ff  |................|
          └─────────i_size=0x7e────────┘ ├┘ └┤ └─i_mime──┘
                                 i_flags=1   i_type=0
00000220  01 00 00 00 00 00 00 00  00 90 04 f5 cf 0e 06 00  |................|
          └───────i_nlinks──────┘  └───────i_btime───────┘
00000230  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
          └───────i_ctime───────┘  └───────i_mtime───────┘
00000240  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
          └────────────────────i_attr────────────────────┘
00000250  72 6f 6f 74 00 00 00 00  00 00 00 00 00 00 00 07  |root............|
          └───────────────────i_owner────────────────────┘
00000260  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
          └──────────────────i_acl[0]────────────────────┘
00000270  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
          └──────────────────i_acl[1]────────────────────┘
00000280  41 58 44 52 00 2d f9 8e  65 00 00 00 0b 00 00 00  |AXDR.-..e.......|
          └───i_d, data stream descriptor─────────────────
```

Here we can see that the file type is 0, so this is a directory. It's mime type isn't 0, rather the special 0xffffffff which
indicates that this directory is not just any directory, it is the root directory.

The `i_d` data stream descriptor usually starts at offset 256, but since here the entire inode is 256 bytes long, in this case it
starts at 128. Since `d_flags` lowest bit is set, there are no block runs in this data stream descriptor rather it has inlined data.

### Directory example

Using the swiss army knife tool:

```
$ ./tool.axfs /dev/sda1 ls /
AleXandriaFS tool Copyright (c) 2023 bzt, GPLv3+

  Magic:            AXDR
  Checksum:         8ef92d00 (valid)
  Entries size:     101 bytes
  Num of entries:   11 record(s)

  0000000000000004 bin
  0000000000000005 boot
  0000000000000007 etc
  0000000000000008 home
  0000000000000009 lib
  000000000000000a mnt
  000000000000000b sys
  000000000000001d root
  0000000000000022 tmp
  0000000000000023 usr
  0000000000000024 var
```

Annotated hexdump:

```
00000280  41 58 44 52 00 2d f9 8e  65 00 00 00 0b 00 00 00  |AXDR.-..e.......|
          └─d_magic─┘ └d_chksum─┘  └─d_size──┘ └d_nument─┘
00000290  02 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
          └─────d_self_fid──────┘  └─────d_gui_attr──────┘
000002a0  01 04 62 69 6e 00 01 05  62 6f 6f 74 00 01 07 65  |..bin...boot...e|
          └──1st dirent───┘ └────2nd dirent─────┘
000002b0  74 63 00 01 08 68 6f 6d  65 00 01 09 6c 69 62 00  |tc...home...lib.|
000002c0  01 0a 6d 6e 74 00 01 0b  73 79 73 00 01 1d 72 6f  |..mnt...sys...ro|
000002d0  6f 74 00 01 22 74 6d 70  00 01 23 75 73 72 00 01  |ot.."tmp..#usr..|
000002e0  24 76 61 72 00 00 00 00  20 00 26 00 2d 00 33 00  |$var.... .&.-.3.|
          ─last dirent─┘ ├┘ └pad┘  └────indextable────────
                         end of list marker
000002f0  3a 00 40 00 4c 00 46 00  53 00 59 00 5f 00 00 00  |:.@.L.F.S.Y._...|
          ─────────────────────────────────────────┘
```

The `d_chksum` field isn't zero, so there's an indextable for this directory (also indicated by `i_size > d_size + 1`). The total
length of the directory entries is 0x65 bytes (including the directory header). There's another zero byte after that and rounded up
to 4 bytes gives `(0x65 + 1 + 3) & ~3 = 0x68`, this is where the indextable starts (header position 0x280 + file offset 0x68, that's
0x2e8 in absolute disk offset). Since `d_size` is smaller than 65536, one index entry is 2 bytes long, and the first element in the
indextable is 0x20.

This means that the first directory entry starts at file offset 0x20 (header position 0x280 + file offset 0x20, that's 0x2a0 in
absolute disk offset). Here the first byte is a packet, 1 means the `d_fid` is stored as uint8_t, and it's in the next byte, 4.
This is followed by the `d_name`, being "bin" and a zero byte string terminator.

Then the second value in the indextable is 0x26. So the second directory entry is at file offset 0x26, starts with a packet byte,
again indicating that `d_fid` is stored as uint8_t, which is 5, and `d_name` is "boot".

The indextable has `d_nument=11` elements, the last pointing to offset 0x5f, and the list ends at file offset 0x65, which contains
a zero fid length packet byte.

### Smallest volume example

```
$ ./mkfs.axfs -c 4K -b 512 -i 256 -j 0 smallest.bin
AleXandriaFS format Copyright (c) 2023 bzt, GPLv3+

AXFS with 8 blocks created successfully.
```

```
$ ./tool.axfs smallest.bin sb
AleXandriaFS tool Copyright (c) 2023 bzt, GPLv3+

Superblock:
  Location:         LBN 0, index 0
  Disk offset:      0000000000000000
  Magic:            AXFS
  Version:          1.0
  Block size:       512 bytes (s_blksize 0)
  Inode size:       256 bytes (s_inoshift 1)
  Flags:            0000
  Enctype:          00 (AXFS_SB_E_SHA)
  Encyrpt:          00 00 00 00 ... 00 00 00 00
  Enchash:          0000 (no encryption)
  UUID:             470F7900-0E95-4D50-913B7E586EE9124A
  Total blocks:     8 (4 KiB)
  Volume fid:       0000000000000001
  Root dir fid:     0000000000000002
  Free blocks fid:  0000000000000003
  Journal fid:      0000000000000000
  Attributes fid:   0000000000000000
  User quota fid:   0000000000000000
  Bad blocks fid:   0000000000000000
  Checksum:         a07311ed (valid)

Volume inode:
  Location:         LBN 0, index 1
  Disk offset:      0000000000000100
  Magic:            AXIN
  Checksum:         18da8c39 (valid)
  Allocated blocks: 0
  File size:        0 bytes +(0 << 64)
  Flags:            00
  File type:        fd (AXFS_FT_INT_VOL)
  State (fsck):     00000000
  Number of inodes: 3
  Last mount time:  0 (1970-01-01 01:00:00.000000)
  Last umount time: 0 (1970-01-01 01:00:00.000000)
  Inode writes:     max 0 countdown 0
  Data stream:      (end of list)

Root directory inode:
  Location:         LBN 1, index 0
  Disk offset:      0000000000000200
  Magic:            AXIN
  Checksum:         edcc3ad9 (valid)
  Allocated blocks: 0
  File size:        33 bytes +(0 << 64)
  Flags:            01 (inlined)
  File type:        00 (AXFS_FT_DIR)
  Mime type:        ffffffff (AXFS_MT_ROOT)
  Number of links:  1
  Birth time:       1705210963000000 (2024-01-14 06:42:43.000000)
  Changed time:     0 (1970-01-01 01:00:00.000000)
  Modified time:    0 (1970-01-01 01:00:00.000000)
  Extended attribs: (none)
  Owner:            746F6F72-0000-0000-00000000000000:rwx----
  Inlined data:     0000: 41 58 44 52 00 00 00 00 20 00 00 00 00 00 00 00  AXDR.... .......
                    0010: 02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................
                    0020: 00                                               .

Free blocks inode:
  Location:         LBN 1, index 1
  Disk offset:      0000000000000300
  Magic:            AXIN
  Checksum:         0c37df44 (valid)
  Allocated blocks: 0
  File size:        3072 bytes +(0 << 64)
  Flags:            00
  File type:        ff (AXFS_FT_INT_FREE)
  Mime type:        00000000
  Data stream:      LBN 2, num 6, len 0
                    (end of list)
```

```
$ hexdump -C smallest.bin
00000000  00 00 00 00 41 58 46 53  01 00 00 01 00 00 00 00  |....AXFS........|
00000010  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000030  00 79 0f 47 95 0e 50 4d  91 3b 7e 58 6e e9 12 4a  |.y.G..PM.;~Xn..J|
00000040  08 00 00 00 00 00 00 00  01 00 00 00 00 00 00 00  |................|
00000050  02 00 00 00 00 00 00 00  03 00 00 00 00 00 00 00  |................|
00000060  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
000000f0  00 00 00 00 00 00 00 00  ed 11 73 a0 41 58 46 53  |..........s.AXFS|
00000100  41 58 49 4e 39 8c da 18  00 00 00 00 00 00 00 00  |AXIN9...........|
00000110  00 00 00 00 00 00 00 00  00 00 00 fd 00 00 00 00  |................|
00000120  03 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000130  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000200  41 58 49 4e d9 3a cc ed  00 00 00 00 00 00 00 00  |AXIN.:..........|
00000210  21 00 00 00 00 00 00 00  00 00 01 00 ff ff ff ff  |!...............|
00000220  01 00 00 00 00 00 00 00  c0 7a b7 5d e1 0e 06 00  |.........z.]....|
00000230  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000250  72 6f 6f 74 00 00 00 00  00 00 00 00 00 00 00 07  |root............|
00000260  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000280  41 58 44 52 00 00 00 00  20 00 00 00 00 00 00 00  |AXDR.... .......|
00000290  02 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
000002a0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000300  41 58 49 4e 44 df 37 0c  00 00 00 00 00 00 00 00  |AXIND.7.........|
00000310  00 0c 00 00 00 00 00 00  00 00 00 ff 00 00 00 00  |................|
00000320  09 02 06 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000330  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00001000
```
